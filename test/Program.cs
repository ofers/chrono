﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {
            Debug.Assert(args.Length <= 1);
            string arg = "";
            if (args.Length == 1) arg = args[0];            
            //Directory.SetCurrentDirectory(@"C:\Users\Ofer\Dropbox\Algorithms in logic\easy_cnf_instances");
            Directory.SetCurrentDirectory(@"C:\temp\muc_test\marques-silva\fpga-routing");
            bool check_correctness = false;
            int conflicts = 0, decisions = 0;
            double time = 0;
            foreach (string file in Directory.GetFiles(Directory.GetCurrentDirectory(),"*.cnf"))
            {
                Console.WriteLine(file);
                Process p = new Process();
                ProcessStartInfo si = new ProcessStartInfo();
                si.FileName = "cmd";
                si.Arguments = @"/c C:\Users\Ofer\source\repos\chrono\Release\chrono.exe " +  arg + " " +  Path.GetFileName(file) + " > tmp";
                si.UseShellExecute = false;
                p.StartInfo = si;
                p.Start();
                p.WaitForExit();

                // now read output
                bool sat = file.Contains("yes");
                string[] lines = File.ReadAllLines("tmp");
                foreach (string line in lines)
                {
                    if (line.Contains("conflicts"))
                    {
                        string[] cols = line.Split(new char[] { ' ' },StringSplitOptions.RemoveEmptyEntries);
                        conflicts += Int32.Parse(cols[3]);
                    }
                    else if (line.Contains("decisions"))
                    {
                        string[] cols = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        decisions += Int32.Parse(cols[3]);
                    }
                    else if (line.Contains("CPU time"))
                    {
                        string[] cols = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        time += Double.Parse(cols[4]);
                    }
                    else if (check_correctness && line.Contains("s SATISFIABLE"))
                    {
                        Debug.Assert(sat);
                    }
                    else if (check_correctness && line.Contains("UNSATISFIABLE"))
                    {
                        Debug.Assert(!sat);
                    }

                }
            }
            Console.WriteLine("Arguments = " + arg);
            Console.WriteLine("------------------");
            Console.WriteLine("conflicts = " + conflicts);
            Console.WriteLine("decisions = " + decisions);
            Console.WriteLine("time = " + time);
        }
    }
}
