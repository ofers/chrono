/****************************************************************************************[Solver.h]
MiniSat -- Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
           Copyright (c) 2007-2010, Niklas Sorensson

Chanseok Oh's MiniSat Patch Series -- Copyright (c) 2015, Chanseok Oh
 
Maple_LCM, Based on MapleCOMSPS_DRUP -- Copyright (c) 2017, Mao Luo, Chu-Min LI, Fan Xiao: implementing a learnt clause minimisation approach
Reference: M. Luo, C.-M. Li, F. Xiao, F. Manya, and Z. L. , “An effective learnt clause minimization approach for cdcl sat solvers,” in IJCAI-2017, 2017, pp. to–appear.
 
Maple_LCM_Dist, Based on Maple_LCM -- Copyright (c) 2017, Fan Xiao, Chu-Min LI, Mao Luo: using a new branching heuristic called Distance at the beginning of search
 
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#ifndef Minisat_Solver_h
#define Minisat_Solver_h
#define PRINT_OUT
#define ANTI_EXPLORATION
//#define BIN_DRUP

#define GLUCOSE23
//#define INT_QUEUE_AVG
//#define LOOSE_PROP_STAT

#ifdef GLUCOSE23
#define INT_QUEUE_AVG
#define LOOSE_PROP_STAT
#endif

#include "mtl/Vec.h"
#include "mtl/Heap.h"
#include "mtl/Alg.h"
#include "utils/Options.h"
#include "core/SolverTypes.h"
#include "utils/System.h"
#include <set>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
// Don't change the actual numbers.
#define LOCAL 0
#define TIER2 2
#define CORE  3


namespace Minisat {

//=================================================================================================
// Solver -- the main class:



class Solver {
private:
    
    template<typename T>
    class MyQueue {
        int max_sz, q_sz;
        int ptr;
        int64_t sum;
        vec<T> q;
    public:
        MyQueue(int sz) : max_sz(sz), q_sz(0), ptr(0), sum(0) { assert(sz > 0); q.growTo(sz); }
        inline bool   full () const { return q_sz == max_sz; }
#ifdef INT_QUEUE_AVG
        inline T      avg  () const { assert(full()); return sum / max_sz; }
#else
        inline double avg  () const { assert(full()); return sum / (double) max_sz; }
#endif
        inline void   clear()       { sum = 0; q_sz = 0; ptr = 0; }
        void push(T e) {
            if (q_sz < max_sz) q_sz++;
            else sum -= q[ptr];
            sum += e;
            q[ptr++] = e;
            if (ptr == max_sz) ptr = 0;
        }
    };

	bool		reorder_now = false, reorder_build = false; // os:

    std::vector<bool> mAssignment; // for testing
public:

    // Constructor/Destructor:
    //
    Solver();
    virtual ~Solver();
    
    void read_assignment(const std::string& filename); // for testing
  
    bool clause_ok(const std::vector<Lit>& eclause); // for testing

    bool clause_ok(const vec<Lit>& eclause);
    
    
    // Problem specification:
    //
    Var     newVar    (bool polarity = true, bool dvar = true); // Add a new variable with parameters specifying variable mode.
    int get_gcd(int a, int b);
    bool    addClause_(      vec<Lit>& ps
#if Extra
        , bool issymetric = true
#endif    
    );                     // Add a clause to the solver without making superflous internal copy. Will


    // Solving:
    //
    bool    simplify     ();                        // Removes already satisfied clauses.
    bool    solve        (const vec<Lit>& assumps); // Search for a model that respects a given set of assumptions.
    lbool   solveLimited (const vec<Lit>& assumps); // Search for a model that respects a given set of assumptions (With resource constraints).
    bool    solve        ();                        // Search without assumptions.
    bool    solve        (Lit p);                   // Search for a model that respects a single assumption.
    bool    solve        (Lit p, Lit q);            // Search for a model that respects two assumptions.
    bool    solve        (Lit p, Lit q, Lit r);     // Search for a model that respects three assumptions.
    bool    okay         () const;                  // FALSE means solver is in a conflicting state

    void    toDimacs     (FILE* f, const vec<Lit>& assumps);            // Write CNF to file in DIMACS-format.
    void    toDimacs     (const char *file, const vec<Lit>& assumps);
    void    toDimacs     (FILE* f, Clause& c, vec<Var>& map, Var& max);
#if Waerden
    void Waerden_createPallets();
    void Waerden_generate(vec<Lit>& lits, std::vector<std::vector<Lit>>& ExtraClauses, int min_Distance_z, int min_Distance_n, std::vector<int>& WaerdenDistance);
    void Waerden_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses, int min_Distance_z, int min_Distance_n, std::vector<int>& WaerdenDistance);
#endif
#if Pyth
    void pyth_generate(std::vector<Lit>& base, int c_gcd, int c_maxvar, std::vector<std::vector<Lit>>& ExtraClauses, std::vector<short>& GcdOfEClauses, std::vector<short>& MaxvarOfEClauses);
    void  pyth_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses, std::vector<short>& GcdOfEClauses, std::vector<short>&  MaxvarOfEClauses);
    std::vector<short> GcdOfEClauses;
    std::vector<short> MaxvarOfEClauses;

#endif
#if Ramsey
    void ramsey_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses);
#endif
// data related to extra_from_generators:
    typedef std::vector<Lit> t_orbit; // e.g., (1 2 3)
    typedef std::vector<t_orbit> t_generator; // a list of orbits, e.g., (1 2 3)(4 -5)(-4 5))
    std::vector<t_generator> generators; 
    typedef std::pair<int, int> t_location; // location = <orbit,offset> 
    typedef std::map<int, t_location> t_lit2location; // lit |=> location in which it resides.
    std::vector<t_lit2location> generator_lit2location; // for each generator
    //std::set<std::vector<Lit>> eclauses_set; // just for statistics, to see how much we add. 
    void read_generators(std::string filename);
    void generators_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses);
    void generators_generate(vec<Lit>& lits, std::vector<std::vector<Lit>>& ExtraClauses);

    // Convenience versions of 'toDimacs()':
    void    toDimacs     (const char* file);
    void    toDimacs     (const char* file, Lit p);
    void    toDimacs     (const char* file, Lit p, Lit q);
    void    toDimacs     (const char* file, Lit p, Lit q, Lit r);
    
    // Variable mode:
    //
    void    setPolarity    (Var v, bool b); // Declare which polarity the decision heuristic should use for a variable. Requires mode 'polarity_user'.
    void    setDecisionVar (Var v, bool b); // Declare if a variable should be eligible for selection in the decision heuristic.

    // Read state:
    //
    lbool   value      (Var x) const;       // The current value of a variable.
    lbool   value      (Lit p) const;       // The current value of a literal.
    lbool   modelValue (Var x) const;       // The value of a variable in the last model. The last call to solve must have been satisfiable.
    lbool   modelValue (Lit p) const;       // The value of a literal in the last model. The last call to solve must have been satisfiable.
    int     nAssigns   ()      const;       // The current number of assigned literals.
    int     nClauses   ()      const;       // The current number of original clauses.
    int     nLearnts   ()      const;       // The current number of learnt clauses.
    int     nVars      ()      const;       // The current number of variables.
    int     nFreeVars  ()      const;

    // Resource constraints:
    //
    void    setConfBudget(int64_t x);
    void    setPropBudget(int64_t x);
    void    budgetOff();
    void    interrupt();          // Trigger a (potentially asynchronous) interruption of the solver.
    void    clearInterrupt();     // Clear interrupt indicator flag.
    void set_cpu_lim(double time) { cpu_lim = time; } // os:

    // Memory managment:
    //
    virtual void garbageCollect();
    void    checkGarbage(double gf);
    void    checkGarbage();

    // Extra results: (read-only member variable)
    //
    vec<lbool> model;             // If problem is satisfiable, this vector contains the model (if any).
    vec<Lit>   conflict;          // If problem is unsatisfiable (possibly under assumptions),
    // this vector represent the final conflict clause expressed in the assumptions.

    // Mode of operation:
    //
    FILE*     drup_file;
    int       verbosity;
    double    step_size;
    double    step_size_dec;
    double    min_step_size;
    int       timer;
    double    var_decay;
    double    clause_decay;
    double    random_var_freq;
    double    random_seed;
    bool      VSIDS;
    int       ccmin_mode;         // Controls conflict clause minimization (0=none, 1=basic, 2=deep).
    int       phase_saving;       // Controls the level of phase saving (0=none, 1=limited, 2=full).
    bool      rnd_pol;            // Use random polarities for branching heuristics.
    bool      rnd_init_act;       // Initialize variable activities with a small random value.
    double    garbage_frac;       // The fraction of wasted memory allowed before a garbage collection is triggered.
	bool      add_rcnf;	

    int       restart_first;      // The initial restart limit.                                                                (default 100)
    double    restart_inc;        // The factor with which the restart limit is multiplied in each restart.                    (default 1.5)
    double    learntsize_factor;  // The intitial limit for learnt clauses is a factor of the original clauses.                (default 1 / 3)
    double    learntsize_inc;     // The limit for learnt clauses is multiplied with this factor each restart.                 (default 1.1)

    int       learntsize_adjust_start_confl;
    double    learntsize_adjust_inc;

    // Statistics: (read-only member variable)
    //
    uint64_t solves, starts, decisions, reorder_decisions, rnd_decisions, propagations, reorder_propagations, conflicts, conflicts_VSIDS;
    uint64_t dec_vars, clauses_literals, learnts_literals, max_literals, tot_literals;
    uint64_t chrono_backtrack, non_chrono_backtrack;
    //! os:
    // These only accumulate info for propagation in the main search. 
    int                 stat_prop_called;
    int                 stat_propogations; 
    int                 stat_conflicts;
    int                 stat_binary_prop;
    int                 stat_bumped;
    int                 stat_size_sat;
    int                 stat_size_unsat;
    float               stat_smoothed_implications_begin;
    float               stat_smoothed_edges;    
    int                 stat_reduced;
    uint64_t            stat_prop_binary, stat_prop_watched, stat_prop_searchNewWatch;
    float               stat_exp_learnt_size; // moving exponent
    const float         stat_exp_alpha = 0.001;
    long long           stat_learnt_size = 0;    // avg. learnt size from beginning. 
    long long           stat_lbd_size = 0;  // avg. lbd size from beginning (only of non-e-clauses). 
    
    int                 stat_rcnf_clauses; 
    void                inc_rcnf_clause() { stat_rcnf_clauses++; }
    int                 get_stat_rcnf_clauses() {
        return stat_rcnf_clauses;
    }
#if Extra
    // keeps the waerden/ramsey/pyth clauses from creation time until the next restart where they are allocated
    // and attached: 
    std::vector<std::vector<Lit>> ExtraClauses;
    // same, for the generators' clauses
    std::vector<std::vector<Lit>> ExtraClauses_gen;
#endif
#if Waerden
    // Like any other clause, the new waerden clauses have distance from 0 and n. 
    // We maintain both of these values consecutively, in tandom with the WaerdenClauses list. 
    std::vector<int> WaerdenDistance;
    typedef struct { short dist_z, dist_n; } Varbounds; // see varbounds below. 
    std::map < Var, Varbounds > varbounds; // var |=> (distance from zero, distance from n) only for variables with value at level 0.
    
#endif // Waerden
    bool useSymmetry;
#if Extra
    int                 stat_non_sym = 0;
    int                 stat_learned_active; // non-extra
    long long int       stat_learned_active_total;
    int                 stat_extra_learned;
    int                 stat_extra_gen_learned;
    int                 stat_extra_unit_learned;
    int                 stat_extra_derived;
    int                 stat_extra_active;    
    long long int       stat_extra_active_total;
    int                 stat_extra_core;
    double              stat_extra_time_begin;
    int                 stat_redundant = 0;
    double              stat_w_time = 0.0;
#endif // Waerden   
#if Pyth
    //!! std::unordered_map<Var, short> gcd_unaries;
#endif
#if Extra    
    bool                extra_restart_now;    
    std::set<Var>       nonsym_unaries; // we cannot rely on the the reason clause of unaries, 
    // because 'analyze' removes literals that are false at level 0, without looking at their reason. 
    
#endif
    int                 ramsey_n;    
    int                 waerden_n; // sequence length. '0' aborts. 
    int                 waerden_c; // # of colors
    std::vector<int>    waerden_col; // color bounds, e.g. for w_3_4_4_30.cnf this will be {3,4,4}
    std::vector<std::vector<int>> waerden_color_permutations;  // for generating more clauses based on color 
                                                                // permutations (see Waerden_createPallets)  
    int                 pyth_n;
	int					extra_plbd;
    int                 extra_nonfalselit;
    bool                extra_restartonconf;
	int					extra_maxsize;
	int					extra_reducelist;   
    int                 extra_learnbound;
    int                 ramsey_searchbound;
    bool                extra_from_generators;
    bool                extra_pyth;
    bool                extra_waerden;
    bool                extra_ramsey;

    //! 

    vec<uint32_t> picked;
    vec<uint32_t> conflicted;
    vec<uint32_t> almost_conflicted;
#ifdef ANTI_EXPLORATION
    vec<uint32_t> canceled;
#endif
	double reorder_cpu_time = 0.0, reorder_start = 0.0; // os:
	int stat_backtrack_size = 0, stat_backtrack_recommendad_size = 0, stat_backtrack_count = 0;

protected:

    // Helper structures:
    //

    struct VarData { CRef reason; int level; };
    static inline VarData mkVarData(CRef cr, int l){ VarData d = {cr, l}; return d; }

    struct Watcher {
        CRef cref;
        Lit  blocker;
        Watcher(CRef cr, Lit p) : cref(cr), blocker(p) {}
        bool operator==(const Watcher& w) const { return cref == w.cref; }
        bool operator!=(const Watcher& w) const { return cref != w.cref; }
    };

    struct WatcherDeleted
    {
        const ClauseAllocator& ca;
        WatcherDeleted(const ClauseAllocator& _ca) : ca(_ca) {}
        bool operator()(const Watcher& w) const { return ca[w.cref].mark() == 1; }
    };

    struct VarOrderLt {
        const vec<double>&  activity;
        bool operator () (Var x, Var y) const { 
            assert(x != y);            
            return activity[x] > activity[y];
        }
        VarOrderLt(const vec<double>&  act) : activity(act) { }
    };

    struct ConflictData
	{
		ConflictData() :
			nHighestLevel(-1),
			bOnlyOneLitFromHighest(false)
		{}

		int nHighestLevel;
		bool bOnlyOneLitFromHighest;
	};

    typedef std::pair<Watcher*, Watcher*> watcher_pair_t;
    // Solver state:
    //
    bool                ok;               // If FALSE, the constraints are already unsatisfiable. No part of the solver state may be used!
    vec<CRef>           clauses;          // List of problem clauses.
    vec<CRef>           learnts_core,     // List of learnt clauses.
    learnts_tier2,
    learnts_local;                        // os: clauses with low quality (high lbd, and others that were not very active).
    double              cla_inc;          // Amount to bump next clause with.

    vec<double>         activity_CHB,     // A heuristic measurement of the activity of a variable.
    activity_VSIDS,activity_distance;
    double              var_inc;          // Amount to bump next variable with.
    OccLists<Lit, vec<Watcher>, WatcherDeleted>
    watches_bin,      // Watches for binary clauses only.
    watches;          // 'watches[lit]' is a list of constraints watching 'lit' (will go there if literal becomes true).
    vec<lbool>          assigns;          // The current assignments.
    vec<char>           polarity;         // The preferred polarity of each variable.
    vec<bool>           decision;        //os: changed to bool, from char // Declares if a variable is eligible for selection in the decision heuristic.
    vec<Lit>            trail;            // Assignment stack; stores all assigments made in the order they were made.
	
    vec<int>            trail_lim;        // Separator indices for different decision levels in 'trail'.
	vec<double>			MaxScoreInLevel;  // os: max score of literals at the decision level
    vec<VarData>        vardata;          // Stores reason and level for each variable.
    int                 qhead;            // Head of queue (as index into the trail -- no more explicit propagation queue in MiniSat).
    int                 simpDB_assigns;   // Number of top-level assignments since last execution of 'simplify()'.
    int64_t             simpDB_props;     // Remaining number of propagations that must be made before next execution of 'simplify()'.
    vec<Lit>            assumptions;      // Current set of assumptions provided to solve by the user.
    Heap<VarOrderLt>    order_heap_CHB,   // A priority queue of variables ordered with respect to the variable activity.
						order_heap_VSIDS,
						order_heap_distance;
	vec<Var>			reorder_sorted;	  // when reordering the trail, this will hold the trail sorted by its activity.
	int					reorder_index = 0;// index into reorder_sorted
	    
    bool                remove_satisfied; // Indicates whether possibly inefficient linear scan for satisfied clauses should be performed in 'simplify'.

    int                 core_lbd_cut;
    float               global_lbd_sum;
    MyQueue<int>        lbd_queue;  // For computing moving averages of recent LBD values.

    uint64_t            next_T2_reduce,
    next_L_reduce;

    ClauseAllocator     ca;
    
    int 				confl_to_chrono;
    int 				chrono;
	
	// os:
	bool				ncb_opt;
	bool				restarts;
	bool				init_no_restarts;
	bool				reorder;
    float               BCPReduceWeight;
    float               reduceratio;
    int                 bcpreduce_strategy;
	bool				reorder_reverse;
	int					reorder_freq;
	int					reorder_filter;
	float				reorder_sort_filter;
    double              cpu_lim;
    bool                act_reduce;

    // Temporaries (to reduce allocation overhead). Each variable is prefixed by the method in which it is
    // used, except 'seen' which is used in several places.
    //
    vec<char>           seen;
    vec<Lit>            analyze_stack;
    vec<Lit>            analyze_toclear;
    vec<Lit>            add_tmp;
    vec<Lit>            add_oc;

    vec<uint64_t>       seen2;    // Mostly for efficient LBD computation. 'seen2[i]' will indicate if decision level or variable 'i' has been seen.
    uint64_t            counter;  // Simple counter for marking purpose with 'seen2'.

    double              max_learnts;
    double              learntsize_adjust_confl;
    int                 learntsize_adjust_cnt;

    // Resource contraints:
    //
    int64_t             conflict_budget;    // -1 means no budget.
    int64_t             propagation_budget; // -1 means no budget.
    bool                asynch_interrupt;

    // Main internal methods:
    //
    void     insertVarOrder   (Var x);                                                 // Insert a variable in the decision order priority queue.
    Lit      pickBranchLit    (bool *reorder_now);                                                      // Return the next decision variable.
    void     newDecisionLevel ();                                                      // Begins a new decision level.
    void     uncheckedEnqueue (Lit p, 
//#if Pyth
//        short gcd = 1, // being conservative. Can be improved by checking all the locations in which this function is called.
//#endif
//#if Extra
//        bool issymetric = false, // being conservative. Can be improved by checking all the locations in which this function is called. 
//#endif
        int level = 0, CRef from = CRef_Undef);                         // Enqueue a literal. Assumes value of literal is undefined.
    void threadFunction(int id);
    CRef spawnThreads(int workers);
    bool prop_body(bool first, Watcher *& i, Watcher *& j, Watcher*& end, Lit& p, int currentlevel, std::vector<std::pair<Watcher*, Watcher*>>& locked, CRef& confl, bool& success);
    //CRef propagate_main_thread();
    CRef propagate_thread(std::vector<Lit>& tr_trail);
    
    bool     enqueue          (Lit p, CRef from = CRef_Undef);                         // Test if fact 'p' contradicts current state, enqueue otherwise.
    CRef     propagate        (bool graph = false);                                                      // Perform unit propagation. Returns possibly conflicting clause.
    void     cancelUntil      (int level);											   // Backtrack until a certain level.
	double   sortedness(vec<Lit>& A);	
	double   sortedness(vec<Var>& A);
	void     analyze          (CRef confl, vec<Lit>& out_learnt, int& out_btlevel, int& out_lbd
#if Waerden
        , short& min_Distance_z
        , short& min_Distance_n
#endif
#if Pyth
        , short& gcd
        , short& maxvar
#endif
#if Extra
        , bool& is_symmetric_clause
        , bool& is_e_derived
#endif
    );    // (bt = backtrack)
    void     analyzeFinal     (Lit p, vec<Lit>& out_conflict);                         // COULD THIS BE IMPLEMENTED BY THE ORDINARIY "analyze" BY SOME REASONABLE GENERALIZATION?
    bool     litRedundant     (Lit p,
 uint32_t abstract_levels
#if Waerden
        , short & min_dist_z, short & min_dist_n
#endif    
#if Pyth
        , short& gcd
        , short& maxvar
#endif
#if Extra
        , bool& issym
#endif
    );                       // (helper method for 'analyze()')
    lbool    search           (int& nof_conflicts);                                    // Search for a given number of conflicts.
    lbool    solve_           ();                                                      // Main solve method (assumptions given in 'assumptions').
    void     reduceDB         ();                                                      // Reduce the set of learnt clauses.
    void     reduceDB_Tier2   ();
    void     removeSatisfied  (vec<CRef>& cs);                                         // Shrink 'cs' to contain only non-satisfied clauses.
    void     safeRemoveSatisfied(vec<CRef>& cs, unsigned valid_mark);
    void     rebuildOrderHeap ();
    bool     binResMinimize   (vec<Lit>& out_learnt
#if Waerden
        , short& min_distance_z, short& min_distance_n
#endif
#if Pyth
        , short& gcd
        , short& maxvar
#endif
#if Extra
        , bool& is_symmetric_clause
        , bool& is_extra_derived
#endif
    );                                  // Further learnt clause minimization by binary resolution.

    // Maintaining Variable/Clause activity:
    //
    void     varDecayActivity ();                      // Decay all variables with the specified factor. Implemented by increasing the 'bump' value instead.
    void     varBumpActivity  (Var v, double mult);    // Increase a variable with the current 'bump' value.
    void     claDecayActivity ();                      // Decay all clauses with the specified factor. Implemented by increasing the 'bump' value instead.
    void     claBumpActivity  (Clause& c, float weight = 1.0);             // Increase a clause with the current 'bump' value.

    

	vec<double> maxScore; // os: maxScore[i] = max_{v \in vars}{score(v) | dl(v) > i}
	
    // Operations on clauses:
    //
    void     attachClause     (CRef cr);               // Attach a clause to watcher lists.
    void     detachClause     (CRef cr, bool strict = false); // Detach a clause to watcher lists.
    void     removeClause     (CRef cr);               // Detach and free a clause.
    bool     locked           (const Clause& c) const; // Returns TRUE if a clause is a reason for some implication in the current state.
    bool     satisfied        (const Clause& c) const; // Returns TRUE if a clause is satisfied in the current state.

    void     relocAll         (ClauseAllocator& to);

    // Misc:
    //
    int      decisionLevel    ()      const; // Gives the current decisionlevel.
    uint32_t abstractLevel    (Var x) const; // Used to represent an abstraction of sets of decision levels.
    CRef     reason           (Var x) const;
    
    ConflictData FindConflictLevel(CRef cind);

    // symmetry_sel

private:
    vec<SymGenerator*> generators_sel;
    int qhead_gen; // Head of queue (as index into the trail) - used for generating new sel clauses.
    vec<SymGenerator*> generatorsOfVar; // vector of relevant generators for each variable (only the one's in which the variable permutes)
    vec<int> generatorsOfVarIndices; // start and end point in generatorsOfVar for each variable
    int watchidx; // Index in list of watching symmetry generators (generatorsOfVar)

    int qhead_sel; // Head of queue (as index into the trail) - used for checking possible propagations in existing sel clauses.
    vec<Lit> selClauses; // contiguous list of (shortened) symmetrical explanation clauses (Theta from the paper). Grows/shrinks as current assignment increases/decreases.
    vec<int> selClausesIndices; // start and endpoint of each selClause. selClausesIndices[i] is the start point of clause i, selClausesIndices[i+1] is the end point.
    vec<int> selClausesOrigProp; // original propagated variable for selClause
    vec<SymGenerator*> selClausesGen; // original generator for selClause
    vec<vec<int>* > selClausesOfWatcher; // maps each literal to the indices of all selClauses it watches

    void minimizeClause(vec<Lit>& c); // minimize clause through self-subsumption
    void prepareWatches(vec<Lit>& c); // prepares watches of a (new) clause
    CRef addClauseFromSymmetry(vec<Lit>& symmetrical); // add a symmetric unit or conflicting clause to the learned clause store (Delta). CRef return value is the conflicting clause, or CRef_Undef if the symmetrical clause is not conflicting.

    // returns 0: conflict clause: should be added to learned clause store (Delta)
    // returns 1: unit clause:     should be added to learned clause store (Delta)
    // returns 2: satisfied:       ignored
    // returns 3: unknown:         added to symmetric clause store (Theta)
    int addSelClause(SymGenerator* g, Lit l);

public:
    vec<char> seenSel;
    uint64_t symgenprops;
    uint64_t symgenconfls;
    uint64_t symselprops;
    uint64_t symselconfls;
    uint64_t symseladded;
    void addGenerator(SymGenerator* g);
    void initiateGenWatches();


    int nGenerators() { return generators_sel.size(); }

    void printClause(vec<Lit>& cl) {
        /* for(int64_t i=0; i<cl.size(); ++i){
             char val = 'a';
             if(value(cl[i])==l_Undef){
                 val = 'u';
             }else if(value(cl[i])==l_True){
                 val = 't';
             }else if(value(cl[i])==l_False){
                 val = 'f';
             }else{
                 val = 'x';
             }
             printf("%d|%c:%d ",toInt(cl[i]),val,level(var(cl[i])));
         }*/
        printf("\n");
    }
    
public:
    int      level            (Var x) const;
    
    int computePartialLBD(const std::vector<Lit> lits) {
        int lbd = 0;

        counter++;
        for (unsigned int i = 0; i < lits.size(); i++) {
            Lit lit = lits[i];
            if (value(lit) == l_Undef) continue;
            int l = level(var(lit));
            if (l != 0 && seen2[l] != counter) {
                seen2[l] = counter;
                lbd++;
            }
        }

        return lbd;
    }

protected:
    bool     withinTimelimit()  const;
    bool     withinBudget()     const;

    template<class V> int computeLBD(const V& c) {
        int lbd = 0;

        counter++;
        for (int i = 0; i < c.size(); i++){
            int l = level(var(c[i]));
            if (l != 0 && seen2[l] != counter){
                seen2[l] = counter;
                lbd++; } }

        return lbd;
    }
#if Extra    
    int computePartialLBD(const Clause& c) {
		int lbd = 0;

		counter++;
		for (int i = 0; i < c.size(); i++) {
			Lit lit = c[i];
			if (value(lit) == l_Undef) continue;          
			int l = level(var(lit));
			if (l != 0 && seen2[l] != counter) {
				seen2[l] = counter;
				lbd++;
			}
		}

		return lbd;
	}
#endif

#ifdef BIN_DRUP
    static int buf_len;
    static unsigned char drup_buf[];
    static unsigned char* buf_ptr;

    static inline void byteDRUP(Lit l){
        unsigned int u = 2 * (var(l) + 1) + sign(l);
        do{
            *buf_ptr++ = u & 0x7f | 0x80; buf_len++;
            u = u >> 7;
        }while (u);
        *(buf_ptr - 1) &= 0x7f; // End marker of this unsigned number.
    }

    template<class V>
    static inline void binDRUP(unsigned char op, const V& c, FILE* drup_file){
        assert(op == 'a' || op == 'd');
        *buf_ptr++ = op; buf_len++;
        for (int i = 0; i < c.size(); i++) byteDRUP(c[i]);
        *buf_ptr++ = 0; buf_len++;
        if (buf_len > 1048576) binDRUP_flush(drup_file);
    }

    static inline void binDRUP_strengthen(const Clause& c, Lit l, FILE* drup_file){
        *buf_ptr++ = 'a'; buf_len++;
        for (int i = 0; i < c.size(); i++)
            if (c[i] != l) byteDRUP(c[i]);
        *buf_ptr++ = 0; buf_len++;
        if (buf_len > 1048576) binDRUP_flush(drup_file);
    }

    static inline void binDRUP_flush(FILE* drup_file){
        fwrite(drup_buf, sizeof(unsigned char), buf_len, drup_file);
        buf_ptr = drup_buf; buf_len = 0;
    }
#endif

    // Static helpers:
    //

    // Returns a random float 0 <= x < 1. Seed must never be 0.
    static inline double drand(double& seed) {
        seed *= 1389796;
        int q = (int)(seed / 2147483647);
        seed -= (double)q * 2147483647;
        return seed / 2147483647; }

    // Returns a random integer 0 <= x < size. Seed must never be 0.
    static inline int irand(double& seed, int size) {
        return (int)(drand(seed) * size); }


    // simplify
    //
public:
    bool	simplifyAll();
    void	simplifyLearnt(Clause& c);
    bool	simplifyLearnt_x(vec<CRef>& learnts_x);
    bool	simplifyLearnt_core();
    bool	simplifyLearnt_tier2();
    int		trailRecord;    
    void	cancelUntilTrailRecord();
    void	simpleUncheckEnqueue(Lit p, CRef from = CRef_Undef);
    CRef    simplePropagate();
    uint64_t nbSimplifyAll;
    uint64_t simplified_length_record, original_length_record;
    uint64_t s_propagations;

    vec<Lit> simp_learnt_clause;
    vec<CRef> simp_reason_clause;
    void	simpleAnalyze(CRef confl, vec<Lit>& out_learnt, vec<CRef>& reason_clause, bool True_confl
#if Waerden
        ,
        short& min_Distance_z,
        short& min_Distance_n
#endif
#if Pyth
        , short& gcd
        , short& maxvar
#endif
#if Extra  
        , bool& is_symmetric_clause
#endif
    );

    // in redundant
    bool removed(CRef cr);
    // adjust simplifyAll occasion
    long curSimplify;
    int nbconfbeforesimplify;
    int incSimplify;

    bool collectFirstUIP(CRef confl);
    vec<double> var_iLevel,var_iLevel_tmp;
    uint64_t nbcollectfirstuip, nblearntclause, nbDoubleConflicts, nbTripleConflicts;
    int uip1, uip2;
    vec<int> pathCs;
    //CRef propagateLits(vec<Lit>& lits);
    uint64_t previousStarts;
    double var_iLevel_inc;
    vec<Lit> involved_lits;
    double    my_var_decay;
    bool   DISTANCE;
};


//=================================================================================================
// Implementation of inline methods:

inline CRef Solver::reason(Var x) const { return vardata[x].reason; }
inline int  Solver::level (Var x) const { return vardata[x].level; }

inline void Solver::insertVarOrder(Var x) {
    //    Heap<VarOrderLt>& order_heap = VSIDS ? order_heap_VSIDS : order_heap_CHB;
    Heap<VarOrderLt>& order_heap = DISTANCE ? order_heap_distance : ((!VSIDS)? order_heap_CHB:order_heap_VSIDS);
    if (!order_heap.inHeap(x) && decision[x]) order_heap.insert(x); 
	if (reorder_build) {
		//static int counter = 0;
		//counter++;
		if (decision[x]) {
#if _DEBUG
			if (find(reorder_sorted, x))
			{
				printf("Repeating variable ? %d", x);
			}
#endif
			//if (counter < 1000) std::cout << x << ":";

			reorder_sorted.push(x);  
		}
	}
}


inline void Solver::varDecayActivity() {
    var_inc *= (1 / var_decay); }

inline void Solver::varBumpActivity(Var v, double mult) {
    if ( (activity_VSIDS[v] += var_inc * mult) > 1e100 ) {
        // Rescale:
        for (int i = 0; i < nVars(); i++)
            activity_VSIDS[i] *= 1e-100;
        var_inc *= 1e-100; }

    // Update order_heap with respect to new activity:
	if (order_heap_VSIDS.inHeap(v)) order_heap_VSIDS.decrease(v);
}

inline void Solver::claDecayActivity() { 
    if (act_reduce)
        cla_inc += (1 / clause_decay);
    else cla_inc *= (1 / clause_decay); 
    
}

inline void Solver::claBumpActivity (Clause& c, float weight /* = 1*/) {
    stat_bumped++;
    if ( (c.activity() += cla_inc * weight) > 1e20f ) {
        // Rescale:
        for (int i = 0; i < learnts_local.size(); i++)
            ca[learnts_local[i]].activity() *= 1e-20f;
        cla_inc *= 1e-20; } }


inline void Solver::checkGarbage(void){ return checkGarbage(garbage_frac); }
inline void Solver::checkGarbage(double gf){
    if (ca.wasted() > ca.size() * gf)
        garbageCollect(); }

// NOTE: enqueue does not set the ok flag! (only public methods do)
inline bool     Solver::enqueue         (Lit p, CRef from)      { return value(p) != l_Undef ? value(p) != l_False : 
    (uncheckedEnqueue(p, 
//#if Pyth
//    1, 
//#endif
//#if Extra
//        true, 
//#endif
        decisionLevel(), from), true); }
inline bool     Solver::locked          (const Clause& c) const {
    int i = c.size() != 2 ? 0 : (value(c[0]) == l_True ? 0 : 1);
    return value(c[i]) == l_True && reason(var(c[i])) != CRef_Undef && ca.lea(reason(var(c[i]))) == &c;
}
inline void     Solver::newDecisionLevel()                      { trail_lim.push(trail.size()); }

inline int      Solver::decisionLevel ()      const   { return trail_lim.size(); }
inline uint32_t Solver::abstractLevel (Var x) const   { return 1 << (level(x) & 31); }
inline lbool    Solver::value         (Var x) const   { return assigns[x]; }
inline lbool    Solver::value         (Lit p) const   { return assigns[var(p)] ^ sign(p); }
inline lbool    Solver::modelValue    (Var x) const   { return model[x]; }
inline lbool    Solver::modelValue    (Lit p) const   { return model[var(p)] ^ sign(p); }
inline int      Solver::nAssigns      ()      const   { return trail.size(); }
inline int      Solver::nClauses      ()      const   { return clauses.size(); }
inline int      Solver::nLearnts      ()      const   { return learnts_core.size() + learnts_tier2.size() + learnts_local.size(); }
inline int      Solver::nVars         ()      const   { return vardata.size(); }
inline int      Solver::nFreeVars     ()      const   { return (int)dec_vars - (trail_lim.size() == 0 ? trail.size() : trail_lim[0]); }
inline void     Solver::setPolarity   (Var v, bool b) { polarity[v] = b; }
inline void     Solver::setDecisionVar(Var v, bool b) 
{ 
    if      ( b && !decision[v]) dec_vars++;
    else if (!b &&  decision[v]) dec_vars--;

    decision[v] = b;
    if (b && !order_heap_CHB.inHeap(v)){
        order_heap_CHB.insert(v);
        order_heap_VSIDS.insert(v);
        order_heap_distance.insert(v);}
}
inline void     Solver::setConfBudget(int64_t x){ conflict_budget    = conflicts    + x; }
inline void     Solver::setPropBudget(int64_t x){ propagation_budget = propagations + x; }
inline void     Solver::interrupt(){ asynch_interrupt = true; }
inline void     Solver::clearInterrupt(){ asynch_interrupt = false; }
inline void     Solver::budgetOff(){ conflict_budget = propagation_budget = -1; }
inline bool     Solver::withinTimelimit() const { return cpu_lim == INT32_MAX || Minisat::cpuTime() < cpu_lim; }
inline bool     Solver::withinBudget() const {
    return !asynch_interrupt &&
            (conflict_budget    < 0 || conflicts < (uint64_t)conflict_budget) &&
            (propagation_budget < 0 || propagations < (uint64_t)propagation_budget); }

// FIXME: after the introduction of asynchronous interrruptions the solve-versions that return a
// pure bool do not give a safe interface. Either interrupts must be possible to turn off here, or
// all calls to solve must return an 'lbool'. I'm not yet sure which I prefer.
inline bool     Solver::solve         ()                    { budgetOff(); assumptions.clear(); return solve_() == l_True; }
inline bool     Solver::solve         (Lit p)               { budgetOff(); assumptions.clear(); assumptions.push(p); return solve_() == l_True; }
inline bool     Solver::solve         (Lit p, Lit q)        { budgetOff(); assumptions.clear(); assumptions.push(p); assumptions.push(q); return solve_() == l_True; }
inline bool     Solver::solve         (Lit p, Lit q, Lit r) { budgetOff(); assumptions.clear(); assumptions.push(p); assumptions.push(q); assumptions.push(r); return solve_() == l_True; }
inline bool     Solver::solve         (const vec<Lit>& assumps){ budgetOff(); assumps.copyTo(assumptions); return solve_() == l_True; }
inline lbool    Solver::solveLimited  (const vec<Lit>& assumps){ assumps.copyTo(assumptions); return solve_(); }
inline bool     Solver::okay          ()      const   { return ok; }

inline void     Solver::toDimacs     (const char* file){ vec<Lit> as; toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p){ vec<Lit> as; as.push(p); toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p, Lit q){ vec<Lit> as; as.push(p); as.push(q); toDimacs(file, as); }
inline void     Solver::toDimacs     (const char* file, Lit p, Lit q, Lit r){ vec<Lit> as; as.push(p); as.push(q); as.push(r); toDimacs(file, as); }

}

#endif
