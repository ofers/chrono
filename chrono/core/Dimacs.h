/****************************************************************************************[Dimacs.h]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#ifndef Minisat_Dimacs_h
#define Minisat_Dimacs_h

#include <stdio.h>

#include "utils/ParseUtils.h"
#include "core/SolverTypes.h"
#include "mtl/Matrix.h"

namespace Minisat {

//=================================================================================================
// DIMACS Parser:

template<class B, class Solver>
static void readClause(B& in, Solver& S, vec<Lit>& lits) {
    int     parsed_lit, var;
    lits.clear();
    for (;;){
        parsed_lit = parseInt(in);
        if (parsed_lit == 0) break;
        var = abs(parsed_lit)-1;
        while (var >= S.nVars()) S.newVar();
        lits.push( (parsed_lit > 0) ? mkLit(var) : ~mkLit(var) );
    }
}

template<class B, class Solver>
static void parse_DIMACS_main(B& in, Solver& S, bool isCnf) {
    vec<Lit> lits;
    int vars    = 0;
    int clauses = 0;
    int cnt     = 0;
	for (;;) {
		skipWhitespace(in);
		if (*in == EOF) {
			break;
		}
		else if (*in == 'p') {
			if (eagerMatch(in, "p cnf")) {
				vars = parseInt(in);
				clauses = parseInt(in);
			}
			else {
				printf("PARSE ERROR! Unexpected char: %c\n", *in), exit(3);
			}
		}
		else if (*in == 'c' || *in == 'p')
			skipLine(in);
		else {
			cnt++;
			readClause(in, S, lits);
		
            // This is a tailored solution for symmetry-breaking constraints. 
            
			S.addClause_(lits
#if Extra
                , isCnf
#endif
            ); 
            // When using the code below for rcnf, the fact that we add a *learnt* clause at this 
            // stage creates some memory problem and hence nondet behavior. 
				
            if (!isCnf) S.inc_rcnf_clause(); // stat only.
		}
	}
    if (vars != S.nVars())
        fprintf(stderr, "WARNING! DIMACS header mismatch: wrong number of variables.\n");
    if (cnt  != clauses)
        fprintf(stderr, "WARNING! DIMACS header mismatch: wrong number of clauses.\n");
}


// symmetry_sel
// a function for parsing .sym files
template<class B, class Solver>
static void parse_SYMMETRY_main(B& in, Solver& S, bool linear_sym_gens) {
    vec<Lit> from;
    vec<Lit> to;
    vec<Lit> cycle;
    for (;;) {
        skipWhitespace(in);
        if (*in == EOF) {
            break;
        }
        else if (*in == '(') {
            // clear from/to to start a new symmetry
            std::cout << "starting generator" << std::endl;
            from.clear(); to.clear();
            while (*in != '\n' && *in != EOF) { // parse one symmetry                
                cycle.clear();
                ++in; // skipping '('
                
                ++in; // skipping ' '
                
                while (*in != ')') { // parse one cycle
                    int parsed_lit = parseInt(in);
                    std::cout << parsed_lit << " ";
                    int var = abs(parsed_lit) - 1;
                    Lit l = (parsed_lit > 0) ? mkLit(var) : ~mkLit(var);
                    cycle.push(l);
                    ++in; // skipping ' '
                }
                // add cycle to from/to
                for (int i = 0; i < cycle.size() - 1; ++i) {
                    from.push(cycle[i]);
                    to.push(cycle[i + 1]);
                }
                if (cycle.size() > 0) {
                    from.push(cycle.last());
                    to.push(cycle[0]);
                }
                ++in; // skipping ')'
                
                ++in; // skipping ' '
                
                //while ((*in >= 9 && *in < 13) || *in == 32) ++in;
            }
            ++in; // skipping '/n'
            std::cout << "adding gnenerator" << std::endl;
            S.addGenerator(new SymGenerator(from, to));
        }
        else if (*in == 'r') {
            // interchangeability matrix
            ++in; ++in; ++in; ++in; // skipping "rows"
            int nbRows = parseInt(in);
            ++in; ++in; ++in; ++in; ++in; ++in; ++in; ++in; // skipping "columns"
            int nbColumns = parseInt(in);

            // parse the matrix
            matrix<Lit> symmat(nbRows, nbColumns);
            for (int i = 0; i < nbRows; ++i) {
                for (int j = 0; j < nbColumns; ++j) {
                    int parsed_lit = parseInt(in);
                    int var = abs(parsed_lit) - 1;
                    Lit l = (parsed_lit > 0) ? mkLit(var) : ~mkLit(var);
                    symmat.set(i, j, l);
                }
            }

            // use a linear number of symmetry generators
            if (linear_sym_gens) {
                for (int i = 0; i < symmat.nr; ++i) {
                    int j = (i + 1) % symmat.nr;
                    from.clear(); to.clear();
                    for (int k = 0; k < symmat.nc; ++k) {
                        from.push(symmat.get(i, k)); from.push(symmat.get(j, k));
                        to.push(symmat.get(j, k));   to.push(symmat.get(i, k));
                    }
                    S.addGenerator(new SymGenerator(from, to));
                }
            }

            // use a quadradic number of symmetry generators
            else {
                for (int i = 0; i < symmat.nr; ++i) {
                    for (int j = i + 1; j < symmat.nr; ++j) {
                        from.clear(); to.clear();
                        for (int k = 0; k < symmat.nc; ++k) {
                            from.push(symmat.get(i, k)); from.push(symmat.get(j, k));
                            to.push(symmat.get(j, k));   to.push(symmat.get(i, k));
                        }
                        S.addGenerator(new SymGenerator(from, to));
                    }
                }
            }
            ++in; // skipping "\n"
        }
        else {
            skipLine(in);
        }
    }
    S.initiateGenWatches();
}

// Inserts problem into solver.
//
template<class Solver>
#if defined(__linux__)
static void parse_DIMACS(gzFile input_stream, Solver& S, bool isCnf) {
#else
static void parse_DIMACS(FILE *input_stream, Solver& S, bool isCnf) {
#endif
    StreamBuffer in(input_stream);
    parse_DIMACS_main(in, S, isCnf); }

// symmetry_sel

template<class Solver>
#if defined(__linux__)
static void parse_SYMMETRY(gzFile input_stream, Solver& S, bool linear_sym_gens) {
#else
static void parse_SYMMETRY(FILE * input_stream, Solver & S, bool linear_sym_gens) {
#endif
    StreamBuffer in(input_stream);
    parse_SYMMETRY_main(in, S, linear_sym_gens);
}
//=================================================================================================

template<class B, class Solver>
static void simple_readClause(B& in, Solver& S, vec<Lit>& lits) {
    int     parsed_lit, var;
    lits.clear();
    for (;;){
        parsed_lit = parseInt(in);
        if (parsed_lit == 0) break;
        var = abs(parsed_lit)-1;
        lits.push( (parsed_lit > 0) ? mkLit(var) : ~mkLit(var) );
    }
}

template<class B, class Solver>
static void check_solution_DIMACS_main(B& in, Solver& S) {
    vec<Lit> lits;
    int vars    = 0;
    int clauses = 0;
    int cnt     = 0;
    bool ok=true;
    for (;;){
        skipWhitespace(in);
        if (*in == EOF) break;
        else if (*in == 'p'){
            if (eagerMatch(in, "p cnf")){
                vars    = parseInt(in);
                clauses = parseInt(in);
                // SATRACE'06 hack
                // if (clauses > 4000000)
                //     S.eliminate(true);
            }else{
                printf("c PARSE ERROR! Unexpected char: %c\n", *in), exit(3);
            }
        } else if (*in == 'c' || *in == 'p')
            skipLine(in);
        else{
            cnt++;
            int parsed_lit, var;
            bool ok=false;
            for(;;) {
                parsed_lit = parseInt(in);
                if (parsed_lit == 0) break; //{printf("\n"); break;}
                var = abs(parsed_lit)-1;
                // printf("%d ", parsed_lit);
                if ((parsed_lit>0 && S.model[var]==l_True) ||
                        (parsed_lit<0 && S.model[var]==l_False))
                    ok=true;
            }
            if (!ok) {
                printf("c clause %d is not satisfied\n", cnt);
                ok=false;
                // break;
            }
        }
    }
    if (cnt  != clauses)
        printf("c WARNING! DIMACS header mismatch: wrong number of clauses.%d %d\n", cnt, clauses);
    else if (ok)
        printf("c solution checked against the original DIMACS file\n");
}

template<class Solver>
static void check_solution_DIMACS(FILE *input_stream, Solver& S) {
    StreamBuffer in(input_stream);
    check_solution_DIMACS_main(in, S); }

//=================================================================================================
}

#endif

