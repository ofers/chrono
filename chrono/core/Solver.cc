/***************************************************************************************[Solver.cc]
MiniSat -- Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
           Copyright (c) 2007-2010, Niklas Sorensson
 
Chanseok Oh's MiniSat Patch Series -- Copyright (c) 2015, Chanseok Oh
 
Maple_LCM, Based on MapleCOMSPS_DRUP -- Copyright (c) 2017, Mao Luo, Chu-Min LI, Fan Xiao: implementing a learnt clause minimisation approach
Reference: M. Luo, C.-M. Li, F. Xiao, F. Manya, and Z. L. , “An effective learnt clause minimization approach for cdcl sat solvers,” in IJCAI-2017, 2017, pp. to–appear.

Maple_LCM_Dist, Based on Maple_LCM -- Copyright (c) 2017, Fan Xiao, Chu-Min LI, Mao Luo: using a new branching heuristic called Distance at the beginning of search
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#include <math.h>
#include <algorithm>
#include <signal.h>
#ifndef _MSC_VER
#include <unistd.h>
#endif



#include "mtl/Sort.h"
#include "core/Solver.h"
#include <stack> // os:
#include "utils/System.h" // os:
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <unordered_set>
#include <set>
#include <sstream>    
#include <mutex>
#include <thread>
#include <random>

using namespace Minisat;
extern void printStats(Solver& solver); //! os:


std::mutex mtx; // Mutex for locking
bool terminate_threads = false;
std::mt19937 g(10);
CRef thread_conf;
int tcounter = 0;

#ifdef BIN_DRUP
int Solver::buf_len = 0;
unsigned char Solver::drup_buf[2 * 1024 * 1024];
unsigned char* Solver::buf_ptr = drup_buf;
#endif


//=================================================================================================
// Options:


static const char* _cat = "CORE";

static DoubleOption  opt_step_size         (_cat, "step-size",   "Initial step size",                             0.40,     DoubleRange(0, false, 1, false));
static DoubleOption  opt_step_size_dec     (_cat, "step-size-dec","Step size decrement",                          0.000001, DoubleRange(0, false, 1, false));
static DoubleOption  opt_min_step_size     (_cat, "min-step-size","Minimal step size",                            0.06,     DoubleRange(0, false, 1, false));
static DoubleOption  opt_var_decay         (_cat, "var-decay",   "The variable activity decay factor",            0.80,     DoubleRange(0, false, 1, false));
static DoubleOption  opt_clause_decay      (_cat, "cla-decay",   "The clause activity decay factor",              0.999,    DoubleRange(0, false, 1, false));
static DoubleOption  opt_random_var_freq   (_cat, "rnd-freq",    "The frequency with which the decision heuristic tries to choose a random variable", 0, DoubleRange(0, true, 1, true));
static DoubleOption  opt_random_seed       (_cat, "rnd-seed",    "Used by the random variable selection",         91648253, DoubleRange(0, false, HUGE_VAL, false));
static IntOption     opt_ccmin_mode        (_cat, "ccmin-mode",  "Controls conflict clause minimization (0=none, 1=basic, 2=deep)", 2, IntRange(0, 2));
static IntOption     opt_phase_saving      (_cat, "phase-saving", "Controls the level of phase saving (0=none, 1=limited, 2=full)", 2, IntRange(0, 2));
static BoolOption    opt_rnd_init_act      (_cat, "rnd-init",    "Randomize the initial activity", false);
static IntOption     opt_restart_first     (_cat, "rfirst",      "The base restart interval", 100, IntRange(1, INT32_MAX));
static DoubleOption  opt_restart_inc       (_cat, "rinc",        "Restart interval increase factor", 2, DoubleRange(1, false, HUGE_VAL, false));
static DoubleOption  opt_garbage_frac      (_cat, "gc-frac",     "The fraction of wasted memory allowed before a garbage collection is triggered",  0.20, DoubleRange(0, false, HUGE_VAL, false));
static IntOption     opt_chrono            (_cat, "chrono",  "-1 = chronological backtracking turned off; otherwise sets the lower-bound_extra_learned on the non-chronological backtrack size to perform chrono backtracking", 100, IntRange(-1, INT32_MAX)); // os:
static IntOption     opt_conf_to_chrono    (_cat, "confl-to-chrono",  "Above this number of conflicts, chrono backtracking begins", 4000, IntRange(-1, INT32_MAX)); // os:
static BoolOption	 opt_ncb	           (_cat, "ncb-opt", "when making non-chrono jump, jump to highest i in [backtrack-level...decisionLevel()] such that backtrackLevel..i-1 won't change anyway due to score (unless there would have been a conflict)", false);
static BoolOption    opt_reorder		   (_cat, "reorder", "reorder trail according to reorder_dir", false);
static BoolOption    opt_reorder_reverse   (_cat, "reorder_reverse", "reorder trail with (default, true) higher or (false) lower score at higher decison levels", false);
static IntOption     opt_reorder_freq	   (_cat, "reorder_freq", "Frequency of trail reordering", 100, IntRange(2, INT32_MAX)); // os:
static IntOption     opt_reorder_filter	   (_cat, "reorder_filter", "Min decision level to activate reorder", 10, IntRange(2, INT32_MAX)); // os:
static DoubleOption  opt_reorder_sortedness_filter (_cat, "reorder_sort_filter","max sortedness of the trail to perform reorder (0 = no reorder, 1 - always reorder) ", 1, DoubleRange(0, true, 1, true));
static BoolOption    opt_restarts			(_cat, "restarts", "activate retarts", true);
static BoolOption    opt_init_no_restarts	(_cat, "init_no_restarts", "Initially run without retarts for 10000 clauses", true);
static DoubleOption  opt_bcpreduce          (_cat, "bcp-reduce", "bcp-based reduceDB heuristic (0.0..3.0), where 0 is normal activity, and the higher the number, the more weight is given to bcp-activity", 0.0, DoubleRange(0.0,true, 10.0, true));
static DoubleOption  opt_reduceratio        (_cat, "reduceratio", "ratio of clauses to reduce (1.0..4.0)", 2.0, DoubleRange(1.0, true, 10.0, true));
static IntOption     opt_bcpreduce_strategy (_cat, "bcpreduce_strategy", "0 - all implications, 1 - implications in unsat\n", 0, IntRange(0, 1)); 
static BoolOption    opt_act_reduce         (_cat, "act_reduce", "reduce a fixed pecentage of total activity, rather than of # of local clauses", false);
static BoolOption    opt_add_rcnf           (_cat, "include-rcnf", "Search rcnf file", false);

#if Extra
static IntOption     opt_extra_plbd         (_cat, "extra_plbd", "threshold (partial-) lbd for keeping extra (waerden/ramsey) clauses", 6, IntRange(-1, 1000));
static IntOption     opt_extra_nonfalselit  (_cat, "extra_nonfalselit", "Screening of eclauses: add only those that have up to this many non-false litearls (-1 = de-activate)", -1, IntRange(-1, 5));
static BoolOption    opt_extra_restartonconf(_cat, "extra_resonconf", "Force restart once the eclause is in conflict", false);
static IntOption     opt_extra_size		    (_cat, "extra_size", "threshold size for keeping waerden/ramsey clauses", 10, IntRange(-1, 1000));
static IntOption     opt_extra_reducelist   (_cat, "extra_reducelist", "reduce list for the extra (waerden/ramsey) clauses:  0: local, 1: Tier2", 0, IntRange(0, 1));
static IntOption     opt_extra_learnbound   (_cat, "extra_learnbound", "bound on the number of extra clauses added per learned clause", 100, IntRange(1, 1000));
static BoolOption    opt_generators         (_cat, "extra_generators", "add e-clauses from breakid generators (looks for a .sym file with generators)", false);
static BoolOption    opt_pyth               (_cat, "extra_pyth", "Pythagorian triples formulas: add e-clauses", false);
static BoolOption    opt_waerden            (_cat, "extra_waerden", "waerden formulas: add e-clauses", false);
static BoolOption    opt_ramsey             (_cat, "extra_ramsey", "ramsey formulas: add e-clauses", false);


#endif
static BoolOption    opt_gen_omer(_cat, "gen_omer", "symmetry through generators, as in the original article", false);
#if Ramsey

static IntOption     opt_ramsey_searchbound (_cat, "ramsey_searchbound", "bound on the search for good (low lbd) extra clauses to add", 4000, IntRange(1, 10000));
#endif
//=================================================================================================
// Constructor/Destructor:


Solver::Solver() :

    // Parameters (user settable):
    //
    drup_file        (NULL)
  , verbosity        (0)
  , step_size        (opt_step_size)
  , step_size_dec    (opt_step_size_dec)
  , min_step_size    (opt_min_step_size)
  , timer            (5000)
  , var_decay        (opt_var_decay)
  , clause_decay     (opt_clause_decay)
  , random_var_freq  (opt_random_var_freq)
  , random_seed      (opt_random_seed)
  , VSIDS            (false)
  , ccmin_mode       (opt_ccmin_mode)
  , phase_saving     (opt_phase_saving)
  , rnd_pol          (false)
  , rnd_init_act     (opt_rnd_init_act)
  , garbage_frac     (opt_garbage_frac)
  , restart_first    (opt_restart_first)
  , restart_inc      (opt_restart_inc)
  , add_rcnf         (opt_add_rcnf)
    // symmetry_sel
    , qhead_gen(0)
    , watchidx(0)
    , qhead_sel(0)
    , symgenprops(0)
    , symgenconfls(0)
    , symselprops(0)
    , symselconfls(0)
    , symseladded(0)
    , useSymmetry(opt_gen_omer)    
    , extra_waerden(false)
    , extra_ramsey(false)
    , extra_from_generators(false)
#if Extra  
  , extra_plbd	     (opt_extra_plbd)
  , extra_nonfalselit (opt_extra_nonfalselit)
  , extra_restartonconf (opt_extra_restartonconf)
  , extra_maxsize	 (opt_extra_size)
  , extra_reducelist (opt_extra_reducelist)
  , extra_learnbound (opt_extra_learnbound)
  , extra_from_generators (opt_generators)
  , extra_pyth       (opt_pyth)
  , extra_waerden    (opt_waerden)
  , extra_ramsey     (opt_ramsey)
#endif
#if Ramsey  
  , ramsey_searchbound(opt_ramsey_searchbound)
#endif
  // Parameters (the rest):
  //
  , learntsize_factor((double)1/(double)3), learntsize_inc(1.1)

  // Parameters (experimental):
  //
  , learntsize_adjust_start_confl (100)
  , learntsize_adjust_inc         (1.5)

  // Statistics: (formerly in 'SolverStats')
  //
  , solves(0), starts(0), decisions(0), reorder_decisions(0), rnd_decisions(0), propagations(0)
  , reorder_propagations(0), conflicts(0), conflicts_VSIDS(0)
  , dec_vars(0), clauses_literals(0), learnts_literals(0), max_literals(0), tot_literals(0)
  , chrono_backtrack(0), non_chrono_backtrack(0)

    //! os:
  , stat_prop_called(0)
  , stat_propogations(0)
  , stat_conflicts(0)
  , stat_binary_prop(0)
  , stat_bumped(0)
  , stat_size_sat(0)
  , stat_size_unsat(0)
  , stat_smoothed_edges(0)
  , stat_reduced(0)
	, stat_prop_binary(0)
	, stat_prop_watched(0)
	, stat_prop_searchNewWatch(0) 
    , stat_rcnf_clauses(0)
#if Extra
    , stat_exp_learnt_size(5) // 5 is arbitrary - just to get the stat going.      
    , stat_extra_learned(0)
    , stat_extra_gen_learned(0)
    , stat_extra_unit_learned(0)
    , stat_extra_derived(0)
    , stat_learned_active(0)
    , stat_learned_active_total(0)
    , stat_extra_active(0)
    , stat_extra_active_total(0)
    , stat_extra_core(0)
#endif // Waerden  
 

  , ok                 (true)
  , cla_inc            (1)
  , var_inc            (1)
  , watches_bin        (WatcherDeleted(ca))
  , watches            (WatcherDeleted(ca))
  , qhead              (0)
  , simpDB_assigns     (-1)
  , simpDB_props       (0)
  , order_heap_CHB     (VarOrderLt(activity_CHB))
  , order_heap_VSIDS   (VarOrderLt(activity_VSIDS))      
  , remove_satisfied   (true)
  , core_lbd_cut       (3)
  , global_lbd_sum     (0)
  , lbd_queue          (50)
  , next_T2_reduce     (10000) // 10000
  , next_L_reduce      (15000) // 15000
  , confl_to_chrono    (opt_conf_to_chrono)
  , chrono			   (opt_chrono)
  , ncb_opt			   (opt_ncb)
  , reorder			   (opt_reorder)
  , reorder_reverse (opt_reorder_reverse)
  ,	restarts(opt_restarts)
  , init_no_restarts (opt_init_no_restarts)
  ,	reorder_freq (opt_reorder_freq)
  , reorder_filter (opt_reorder_filter)
  ,	reorder_sort_filter (opt_reorder_sortedness_filter)
  , counter            (0) 
  , BCPReduceWeight (opt_bcpreduce)
  , reduceratio (opt_reduceratio)
   , bcpreduce_strategy(opt_bcpreduce_strategy)
        , act_reduce(opt_act_reduce)
  // Resource constraints:
  //
  , conflict_budget    (-1)
  , propagation_budget (-1)
  , asynch_interrupt   (false)

  // simplfiy
  , nbSimplifyAll(0)
  , s_propagations(0)

  // simplifyAll adjust occasion
  , curSimplify(1)
  , nbconfbeforesimplify(1000)
  , incSimplify(1000)

  , my_var_decay       (0.6)
  , DISTANCE           (true) 
  , var_iLevel_inc     (1)
  , order_heap_distance(VarOrderLt(activity_distance))  
	
  {
      // symmetry_sel
      selClausesIndices.push(0);
      generatorsOfVarIndices.push(0);
  }


Solver::~Solver()
{
}

static bool switch_mode = false;
static void SIGALRM_switch(int signum) { switch_mode = true; }
// simplify All
// called from simplifyLEarnt. This propagation will be undone at the end of simplifyLearnt. 
CRef Solver::simplePropagate()
{
    CRef    confl = CRef_Undef;
    int     num_props = 0;
    watches.cleanAll();
    watches_bin.cleanAll();
    std::set<Var> tmp_nonsym_unaries;    // test. does not seem to solve the problem. in theory 
    // we should send ths info to the caller, but it doesn't seem to ever be populated and 
    // hence relevant to fixing the problem. 
    while (qhead < trail.size())
    {
        Lit            p = trail[qhead++];     // 'p' is enqueued fact to propagate.
        vec<Watcher>&  ws = watches[p];
        Watcher        *i, *j, *end;
        num_props++;

        // First, Propagate binary clauses
        vec<Watcher>&  wbin = watches_bin[p];

        for (int k = 0; k<wbin.size(); k++)
        {
            Lit imp = wbin[k].blocker;
            
            if (value(imp) == l_False)
            {                
#if Extra
                if (tmp_nonsym_unaries.count(var(p)) == 1 || nonsym_unaries.count(var(p)) == 1)
                { 
                    tmp_nonsym_unaries.insert(var(imp));
                }
#endif
                return wbin[k].cref;
            }

            if (value(imp) == l_Undef)
            {             
#if Extra
                if (tmp_nonsym_unaries.count(var(p)) == 1 || nonsym_unaries.count(var(p)) == 1)
                    tmp_nonsym_unaries.insert(var(imp));
#endif
                simpleUncheckEnqueue(imp, wbin[k].cref);
            }
        }
        for (i = j = (Watcher*)ws, end = i + ws.size(); i != end;)
        {
            // Try to avoid inspecting the clause:
            Lit blocker = i->blocker;
            if (value(blocker) == l_True)
            {
                *j++ = *i++; continue;
            }

            // Make sure the false literal is data[1]:
            CRef     cr = i->cref;
            Clause&  c = ca[cr];
            Lit      false_lit = ~p;
            if (c[0] == false_lit)
                c[0] = c[1], c[1] = false_lit;
            assert(c[1] == false_lit);
            //  i++;

            // If 0th watch is true, then clause is already satisfied.
            // However, 0th watch is not the blocker, make it blocker using a new watcher w
            // why not simply do i->blocker=first in this case?
            Lit     first = c[0];
            if (first != blocker && value(first) == l_True)
            {
                i->blocker = first;
                *j++ = *i++; continue;
            }
            else
            {
                for (int k = 2; k < c.size(); k++)
                {

                    if (value(c[k]) != l_False)
                    {
                        // watcher i is abandonned using i++, because cr watches now ~c[k] instead of p
                        // the blocker is first in the watcher. However,
                        // the blocker in the corresponding watcher in ~first is not c[1]
                        Watcher w = Watcher(cr, first); i++;
                        c[1] = c[k]; c[k] = false_lit;
                        watches[~c[1]].push(w);
                        goto NextClause;
                    }
                }
            }

            // Did not find watch -- clause is unit under assignment:
            i->blocker = first;
            *j++ = *i++;
            if (value(first) == l_False)
            {
                confl = cr;
                qhead = trail.size();
                // Copy the remaining watches:
                while (i < end)
                    *j++ = *i++;
            }
            else
            {
                simpleUncheckEnqueue(first, cr);
#if Extra
                if (tmp_nonsym_unaries.count(var(p)) == 1 || nonsym_unaries.count(var(p)) == 1)
                    tmp_nonsym_unaries.insert(var(first));
#endif
            }
NextClause:;
        }
        ws.shrink(i - j);
    }
    
    s_propagations += num_props;

    return confl;
}

void Solver::simpleUncheckEnqueue(Lit p, CRef from){
    assert(value(p) == l_Undef);    
    assigns[var(p)] = lbool(!sign(p)); // this makes a lbool object whose value is sign(p)
    vardata[var(p)].reason = from;    
    trail.push_(p);
}

void Solver::cancelUntilTrailRecord()
{
    for (int c = trail.size() - 1; c >= trailRecord; c--)
    {
        Var x = var(trail[c]);
        assigns[x] = l_Undef;

    }
    qhead = trailRecord;
    trail.shrink(trail.size() - trailRecord);

}


bool Solver::removed(CRef cr) {
    return ca[cr].mark() == 1;
}

void Solver::simpleAnalyze(CRef confl, vec<Lit>& out_learnt, vec<CRef>& reason_clause, bool True_confl
#if Waerden
    ,
    short& min_Distance_z,
    short& min_Distance_n
#endif
#if Pyth
    , short& gcd
    , short& maxvar
#endif
#if Extra     
    , bool& is_symmetric_clause
#endif
)
{
    int pathC = 0;
    Lit p = lit_Undef;
    int index = trail.size() - 1;

    do{
        if (confl != CRef_Undef){
            reason_clause.push(confl);
            Clause& c = ca[confl];            
#if Waerden
            // calculating the min distance among premise clauses: 
            min_Distance_z = std::min(min_Distance_z, c.Distance_z());
            min_Distance_n = std::min(min_Distance_n, c.Distance_n());
#endif
#if Pyth
            if (is_symmetric_clause && (c.gcd() % gcd > 0)) {
                gcd = get_gcd(gcd, c.gcd());
                maxvar = std::max(maxvar, c.maxvar());
            }
#endif
#if Extra
            is_symmetric_clause = is_symmetric_clause && c.is_symmetric_clause();
#endif
            // Special case for binary clauses
            // The first one has to be SAT
            if (p != lit_Undef && c.size() == 2 && value(c[0]) == l_False) {

                assert(value(c[1]) == l_True);
                Lit tmp = c[0];
                c[0] = c[1], c[1] = tmp;
            }
            
            // os: If we are looking at the first clause (i.e., when p == lit_Undef), then the 0 literal 
            // matters. Otherwise it is a clause that resolves with the previous one, and the first 
            // literal is the pivot so it was already handled. 
            // An exception is True_clause: in that case even in the first iteration there is no 
            // need to look at the first literal because its reason was already considered in the 
            // beginning. 
            for (int j = (p == lit_Undef && True_confl == false) ? 0 : 1; j < c.size(); j++){
                Lit q = c[j];
                if (!seen[var(q)]){
                    seen[var(q)] = 1;
                    pathC++;
                }
            }
        }
        else {
            assert(confl == CRef_Undef);
#if Extra
            if (nonsym_unaries.count(var(p)) == 1) 
                is_symmetric_clause = false;
#endif
            out_learnt.push(~p);            
        }
        // if not break, while() will come to the index of trail below 0, and fatal error occur;
        if (pathC == 0) break;
        // Select next clause to look at:
        while (!seen[var(trail[index--])]);
        // if the reason cr from the 0-level assigned var, we must break avoid move forth further;
        // but attention that maybe seen[x]=1 and never be clear. However makes no matter;
        if (trailRecord > index + 1) break;
        p = trail[index + 1];
        confl = reason(var(p));
        seen[var(p)] = 0;
        pathC--;

    } while (pathC >= 0);
}

// os: temporarily assigns the literals in c false. Stops if it conflicts the rest of the formula via bcp. 
// It then invokes simpleanalyze which detects which of the literals that we assigned in c 
// participated in the conflict so it can shrink even more. Undoes the assignment at the end (via cancelUntilTrailRecord). 
// Example: c = (1 2 3 4). Assigning -1, -2, -3 leads to a conflict. With simpleanalyze we detect that
// -1, -3 are sufficient for the conflict. Hence \varphi |= (1 3), so we shrink c to (1 3).
// It can also be that assigning -1 -2 implies 3. So \varphi |= (1 2 3) which subsumes (1 2 3 4). 
// This is handled here via 'True_confl'. 
void Solver::simplifyLearnt(Clause& c)
{    
    
    return; //  just because it leads to some bug in Extra
    original_length_record += c.size();

    trailRecord = trail.size();// record the start pointer

    //vec<Lit> falseLit; // os: removed, was unused.
    //falseLit.clear();

    //sort(&c[0], c.size(), VarOrderLevelLt(vardata));

    bool True_confl = false;
    int i, j;
    CRef confl;    
    for (i = 0, j = 0; i < c.size(); i++){
        if (value(c[i]) == l_Undef){            
            simpleUncheckEnqueue(~c[i]); 
            c[j++] = c[i];
            confl = simplePropagate();
            if (confl != CRef_Undef){                
                break;
            }
        }
        else{
            if (value(c[i]) == l_True){                
                c[j++] = c[i];
                True_confl = true;
                confl = reason(var(c[i]));                                                
                break;
            }
            //else{                // os: removed, was unused. 
            //    falseLit.push(c[i]);
            //}
        }
    }
    
    c.shrink(c.size() - j);    

    if (confl != CRef_Undef || True_confl == true){
        simp_learnt_clause.clear();
        simp_reason_clause.clear();
        if (True_confl == true){
            simp_learnt_clause.push(c.last());
        }
#if Extra
        bool issym = c.is_symmetric_clause();

#endif
#if Waerden
        short min_Distance_z = c.Distance_z(), min_Distance_n = c.Distance_n();
#endif
#if Pyth
        short gcd = c.gcd();
        short gcd_org = gcd;
        short maxvar = c.maxvar();
#endif
        simpleAnalyze(confl, simp_learnt_clause, simp_reason_clause, True_confl

#if Waerden
            , min_Distance_z, min_Distance_n
#endif
#if Pyth
            , gcd
            , maxvar
#endif
#if Extra
            , issym
#endif
        );

#if Waerden        
        c.Distance_z(min_Distance_z);
        c.Distance_n(min_Distance_n);
#endif
#if Pyth        
        c.gcd(gcd);
        c.maxvar(maxvar);
#endif

#if Extra        
        c.is_symmetric_clause(issym);
        
        //c.is_symmetric_clause(false); // test: solves the problem
        
#endif

        if (
            simp_learnt_clause.size() < c.size()){
            for (i = 0; i < simp_learnt_clause.size(); i++){
                c[i] = simp_learnt_clause[i];
            }
            c.shrink(c.size() - i);
        }
    }

    cancelUntilTrailRecord();

    simplified_length_record += c.size();

}

bool Solver::simplifyLearnt_core()
{    
    int ci, cj, li, lj;
    bool sat, false_lit;
    int nblevels;
  
    int nbSimplified = 0;
    int nbSimplifing = 0;
    //printf("skipping simplifyLearnt\n");
    for (ci = 0, cj = 0; ci < learnts_core.size(); ci++){
        CRef cr = learnts_core[ci];
        Clause& c = ca[cr];        

        if (removed(cr)) continue;
        else if (c.simplified()){
            learnts_core[cj++] = learnts_core[ci];
            ////
            nbSimplified++;
        }
        else{
            int saved_size=c.size();         
            nbSimplifing++;
            sat = false_lit = false;
            for (int i = 0; i < c.size(); i++){
                if (value(c[i]) == l_True){
                    sat = true;
                    break;
                }
                else if (value(c[i]) == l_False){
                    false_lit = true;
                }
            }
            if (sat){
                removeClause(cr);
            }
            else{
                detachClause(cr, true);

                if (false_lit){
                    for (li = lj = 0; li < c.size(); li++){
                        if (value(c[li]) != l_False){
                            c[lj++] = c[li];
                        }
                    }
                    c.shrink(li - lj);
                }
                                
                assert(c.size() > 1);
                // simplify a learnt clause c
                simplifyLearnt(c); 
                
                assert(c.size() > 0);
                                
                if(drup_file && saved_size !=c.size()){
#ifdef BIN_DRUP
                    binDRUP('a', c , drup_file);
#else
                    for (int i = 0; i < c.size(); i++)
                        fprintf(drup_file, "%i ", (var(c[i]) + 1) * (-2 * sign(c[i]) + 1));
                    fprintf(drup_file, "0\n");
#endif
                }

                if (c.size() == 1){
                    // when a unit clause occurs, enqueue and propagate
                    uncheckedEnqueue(c[0]);
#if Extra
                    if (!c.is_symmetric_clause()) nonsym_unaries.insert(var(c[0]));
#endif
#if Waerden
                    else {
                        varbounds[var(c[0])].dist_z = c.Distance_z();
                        varbounds[var(c[0])].dist_n = c.Distance_n();
                    }
#endif
                    if (propagate() != CRef_Undef){
                        ok = false;
                        return false;
                    }
                    // delete the clause memory in logic
                    c.mark(1);
                    ca.free(cr);
                }
                else{
                    attachClause(cr);
                    learnts_core[cj++] = learnts_core[ci];

                    nblevels = computeLBD(c);
                    if (nblevels < c.lbd()){
                        //printf("lbd-before: %d, lbd-after: %d\n", c.lbd(), nblevels);
                        c.set_lbd(nblevels);
                    }

                    c.setSimplified(true);
                }
            }
        }
    }
    learnts_core.shrink(ci - cj);

    return true;

}

bool Solver::simplifyLearnt_tier2()
{
    //printf("skipping simplifyLearnt\n"); // look inside simplifyLearnt
    int ci, cj, li, lj;
    bool sat, false_lit;
    unsigned int nblevels;
  
    int nbSimplified = 0;
    int nbSimplifing = 0;

    for (ci = 0, cj = 0; ci < learnts_tier2.size(); ci++){
        CRef cr = learnts_tier2[ci];
        Clause& c = ca[cr];

        if (removed(cr)) continue;
        else if (c.simplified()){
            learnts_tier2[cj++] = learnts_tier2[ci];
            ////
            nbSimplified++;
        }
        else{
            int saved_size=c.size();     
            nbSimplifing++;
            sat = false_lit = false;
            for (int i = 0; i < c.size(); i++){
                if (value(c[i]) == l_True){
                    sat = true;
                    break;
                }
                else if (value(c[i]) == l_False){
                    false_lit = true;
                }
            }
            if (sat){
                removeClause(cr);
            }
            else{
                detachClause(cr, true);

                if (false_lit){
                    for (li = lj = 0; li < c.size(); li++){
                        if (value(c[li]) != l_False){
                            c[lj++] = c[li];
                        }
#if Extra
                        else {
                            if (nonsym_unaries.count(var(c[li]) == 1)) {
                                c.is_symmetric_clause(false);
                                // it actually happens: 
                                //printf("!!simlifylearnttier2-nonsym\n");
                            }
                        }
#endif
                    }
                    c.shrink(li - lj);
                }
                                
                assert(c.size() > 1);
                // simplify a learnt clause c
                simplifyLearnt(c); 
                assert(c.size() > 0);
                                
                if(drup_file && saved_size!=c.size()){

#ifdef BIN_DRUP
                    binDRUP('a', c , drup_file);
#else
                    for (int i = 0; i < c.size(); i++)
                        fprintf(drup_file, "%i ", (var(c[i]) + 1) * (-2 * sign(c[i]) + 1));
                    fprintf(drup_file, "0\n");
#endif
                }
                                
                if (c.size() == 1){
                    // when unit clause occur, enqueue and propagate
                    uncheckedEnqueue(c[0]);
#if Extra
                    if (!c.is_symmetric_clause()) nonsym_unaries.insert(var(c[0]));
#endif
#if Waerden
                    else {
                        varbounds[var(c[0])].dist_z = c.Distance_z();
                        varbounds[var(c[0])].dist_n = c.Distance_n();
                    }
#endif
                    if (propagate() != CRef_Undef){
                        ok = false;
                        return false;
                    }
                    // delete the clause memory in logic
                    c.mark(1);
                    ca.free(cr);
                }
                else{
                    attachClause(cr);
                    learnts_tier2[cj++] = learnts_tier2[ci];

                    nblevels = computeLBD(c);
                    if (nblevels < c.lbd()){
                        //printf("lbd-before: %d, lbd-after: %d\n", c.lbd(), nblevels);
                        c.set_lbd(nblevels);
                    }

                    if (c.lbd() <= core_lbd_cut){
                        cj--;
                        learnts_core.push(cr); 
#if Extra
                        if (c.is_e_clause()) {
                            stat_extra_core++;                               
                        }
#endif
                        c.mark(CORE);  
                    } 

                    c.setSimplified(true);
                }
            }
        }
    }
    learnts_tier2.shrink(ci - cj);

    return true;

}

// os: only called at decisionlevel 0
bool Solver::simplifyAll()
{	    
    simplified_length_record = original_length_record = 0;
    
    if (!ok || propagate() != CRef_Undef)
        return ok = false;
    
    if (!simplifyLearnt_core()) return ok = false;
    
    if (!simplifyLearnt_tier2()) return ok = false;
    
    //if (!simplifyLearnt_x(learnts_local)) return ok = false;

    checkGarbage();    
    return true;
}
//=================================================================================================
// Minor methods:


// Creates a new SAT variable in the solver. If 'decision' is cleared, variable will not be
// used as a decision variable (NOTE! This has effects on the meaning of a SATISFIABLE result).
//
Var Solver::newVar(bool sign, bool dvar)
{
    int v = nVars();
    watches_bin.init(mkLit(v, false));
    watches_bin.init(mkLit(v, true ));
    watches  .init(mkLit(v, false));
    watches  .init(mkLit(v, true ));
    assigns  .push(l_Undef);
    vardata  .push(mkVarData(CRef_Undef, 0));
    activity_CHB  .push(0); 
    activity_VSIDS.push(rnd_init_act ? drand(random_seed) * 0.00001 : 0); 

    picked.push(0);
    conflicted.push(0);
    almost_conflicted.push(0);
#ifdef ANTI_EXPLORATION
    canceled.push(0);
#endif

    seen     .push(0);
    seen2    .push(0);
    polarity .push(sign);
    decision .push();
    trail    .capacity(v+1);
    activity_distance.push(0.0); // this line should come before setDecisionVar. 

    setDecisionVar(v, dvar);

    // symmetry_sel
    selClausesOfWatcher.push(new vec<int>());
    selClausesOfWatcher.push(new vec<int>());
    generatorsOfVarIndices.push(generatorsOfVar.size());
    seenSel.push(0);

    var_iLevel.push(0);
    var_iLevel_tmp.push(0);
    pathCs.push(0);
    return v;
}

int Solver::get_gcd(int a, int b) {    
        if (a == 0)
            return b;
        return get_gcd(b % a, a);    
}

bool Solver::addClause_(vec<Lit>& ps
#if Extra
    , bool issymetric
#endif
)
{
    assert(decisionLevel() == 0);
    if (!ok) return false;

    // Check if clause is satisfied and remove false/duplicate literals:
    sort(ps);
    Lit p; int i, j;

    if (drup_file){
        add_oc.clear();
        for (int i = 0; i < ps.size(); i++) add_oc.push(ps[i]); }
#if Pyth
    int tmp_gcd = 0;
    int tmp_maxvar = 0;
#endif
    for (i = j = 0, p = lit_Undef; i < ps.size(); i++)
        if (value(ps[i]) == l_True || ps[i] == ~p)
            return true;
        else if (value(ps[i]) != l_False && ps[i] != p) {
#if Pyth
            if (issymetric) {                
                tmp_gcd = get_gcd(tmp_gcd, var(ps[i]) + 1);
                tmp_maxvar = std::max(tmp_maxvar, var(ps[i]) + 1);
            }
            assert(tmp_gcd > 0);
#endif
            ps[j++] = p = ps[i];
        }
    ps.shrink(i - j);    
        
    if (drup_file && i != j){
#ifdef BIN_DRUP
        binDRUP('a', ps, drup_file);
        binDRUP('d', add_oc, drup_file);
#else
        for (int i = 0; i < ps.size(); i++)
            fprintf(drup_file, "%i ", (var(ps[i]) + 1) * (-2 * sign(ps[i]) + 1));
        fprintf(drup_file, "0\n");

        fprintf(drup_file, "d ");
        for (int i = 0; i < add_oc.size(); i++)
            fprintf(drup_file, "%i ", (var(add_oc[i]) + 1) * (-2 * sign(add_oc[i]) + 1));
        fprintf(drup_file, "0\n");
#endif
    }

    if (ps.size() == 0)
        return ok = false;
    else if (ps.size() == 1){
        uncheckedEnqueue(ps[0]);
#if Extra
        if (!issymetric) nonsym_unaries.insert(var(ps[0]));
#endif
        return ok = (propagate() == CRef_Undef);
    }else{
        CRef cr = ca.alloc(ps, false);
        
        clauses.push(cr);
#if Extra
        ca[cr].is_symmetric_clause(issymetric);
        if (!issymetric) stat_non_sym++;
#endif
#if Pyth
        ca[cr].gcd(tmp_gcd); // it will be 0 if !issymetric.
        ca[cr].maxvar(tmp_maxvar);
#endif
        attachClause(cr);
    }

    return true;
}


void Solver::attachClause(CRef cr) {
    const Clause& c = ca[cr];
    assert(c.size() > 1);
    OccLists<Lit, vec<Watcher>, WatcherDeleted>& ws = c.size() == 2 ? watches_bin : watches;
    ws[~c[0]].push(Watcher(cr, c[1]));
    ws[~c[1]].push(Watcher(cr, c[0]));
    if (c.learnt()) learnts_literals += c.size();
    else            clauses_literals += c.size(); 
#if Extra
    if (c.is_e_clause()) {
        stat_extra_active++;
        if (c.mark() == CORE) {
            stat_extra_core++;
        }
    }
    else if (c.learnt()) stat_learned_active++;
#endif

}


void Solver::detachClause(CRef cr, bool strict) {
    const Clause& c = ca[cr];
    assert(c.size() > 1);
    OccLists<Lit, vec<Watcher>, WatcherDeleted>& ws = c.size() == 2 ? watches_bin : watches;
    
    if (strict){        
        remove(ws[~c[0]], Watcher(cr, c[1]));
        remove(ws[~c[1]], Watcher(cr, c[0]));
    } else {
        // Lazy detaching: (NOTE! Must clean all watcher lists before garbage collecting this clause)
        ws.smudge(~c[0]);
        ws.smudge(~c[1]);
    }

    if (c.learnt()) learnts_literals -= c.size();
    else            clauses_literals -= c.size(); 
#if Extra
    
    if (c.is_e_clause()) {
        stat_extra_active--;
        if (c.mark() == CORE) {
            stat_extra_core--;
        }
    }
    else if (c.learnt()) stat_learned_active--;
#endif

}


void Solver::removeClause(CRef cr) {
    Clause& c = ca[cr];

    if (drup_file){
        if (c.mark() != 1){
#ifdef BIN_DRUP
            binDRUP('d', c, drup_file);
#else
            fprintf(drup_file, "d ");
            for (int i = 0; i < c.size(); i++)
                fprintf(drup_file, "%i ", (var(c[i]) + 1) * (-2 * sign(c[i]) + 1));
            fprintf(drup_file, "0\n");
#endif
        }else
            printf("c Bug. I don't expect this to happen.\n");
    }

    detachClause(cr);
    // Don't leave pointers to free'd memory!
    if (locked(c)){
        Lit implied = c.size() != 2 ? c[0] : (value(c[0]) == l_True ? c[0] : c[1]);
        vardata[var(implied)].reason = CRef_Undef; }
    c.mark(1);
    ca.free(cr);
}


bool Solver::satisfied(const Clause& c) const {
    for (int i = 0; i < c.size(); i++)
        if (value(c[i]) == l_True)
            return true;
    return false; }


// Revert to the state at given level (keeping all assignment at 'level' but not beyond). 
// os: this comment is not in sync with the code(?) the code does eliminate 'level' (?) or perhaps trail_lim[bl] is the end of the level ? 
//
void Solver::cancelUntil(int bLevel) {
#ifdef PRINT_OUT			
	//std::cout << "canceluntil " << bLevel << std::endl;
#endif
    if (decisionLevel() > bLevel){
		add_tmp.clear();
        for (int c = trail.size()-1; c >= trail_lim[bLevel]; c--)
        {
            Var      x  = var(trail[c]);

			if (level(x) <= bLevel) // os: because of chrono jumping
			{
				add_tmp.push(trail[c]);
			}
			else
			{
				 if (!VSIDS){
					uint32_t age = conflicts - picked[x];
					if (age > 0){
						double adjusted_reward = ((double) (conflicted[x] + almost_conflicted[x])) / ((double) age);
						double old_activity = activity_CHB[x];
						activity_CHB[x] = step_size * adjusted_reward + ((1 - step_size) * old_activity);
						if (order_heap_CHB.inHeap(x)){
							if (activity_CHB[x] > old_activity)
								order_heap_CHB.decrease(x);
							else
								order_heap_CHB.increase(x);
						}
					}
#ifdef ANTI_EXPLORATION
					canceled[x] = conflicts;
#endif
				}
				
				assigns [x] = l_Undef;
	            if (phase_saving > 1 || (phase_saving == 1) && c > trail_lim.last())
					polarity[x] = sign(trail[c]);
				insertVarOrder(x);
			}
        }
        qhead = trail_lim[bLevel];

        // symmetry_sel
        qhead_gen = trail_lim[bLevel];
        qhead_sel = trail_lim[bLevel];
        watchidx = 0;

        trail.shrink(trail.size() - trail_lim[bLevel]);
		if (ncb_opt) MaxScoreInLevel.shrink(trail_lim.size() - bLevel); // os: 
		trail_lim.shrink(trail_lim.size() - bLevel);
		
        for (int nLitId = add_tmp.size() - 1; nLitId >= 0; --nLitId)
		{
			 trail.push_(add_tmp[nLitId]);
		} 
		 
		add_tmp.clear();
		//static int calls_counter = 0;

        // symmetry_sel
        // remove all the symmetrical clauses that were added due to propagations that are now deleted
        if (bLevel == 0) {
            for (int i = 0; i < selClausesOfWatcher.size(); ++i) {
                selClausesOfWatcher[i]->clear();
            }
            selClauses.clear();
            selClausesIndices.clear(); selClausesIndices.push(0);
            selClausesGen.clear();
            selClausesOrigProp.clear();
        }
        else {
            while (selClausesOrigProp.size() > 0 && level(selClausesOrigProp.last()) > bLevel) {
                selClausesOrigProp.pop();
            }
            selClausesGen.shrinkTo_(selClausesOrigProp.size());
            selClausesIndices.shrinkTo_(selClausesOrigProp.size() + 1);
            selClauses.shrinkTo_(selClausesIndices.last());
        }
	} }

double Solver::sortedness(vec<Lit>& A) {
	vec<Var> A_var;
	for (int i = trail_lim[0]; i < A.size(); ++i) {
		A_var.push(var(A[i]));
	}
	return sortedness(A_var);
}

// If it is already sorted it returns 1. Anything else will be smaller than 1.
double Solver::sortedness(vec<Var>& A) {
	vec<Var> sorted, sort_dec_level;
	A.copyTo(sorted);
	//A.copyTo(sort_dec_level);
	double sum = 0, sum_sorted = 0; // , sum_dec = 0, sum_dec_sorted = 0;
	vec<double>& activity = DISTANCE ? activity_distance : ((!VSIDS) ? activity_CHB : activity_VSIDS);
	sort(sorted, VarOrderLt(activity)); // order will be descending
	//sort(sort_dec_level, VarDecLevelOrderLt(vardata));
	int weight = sorted.size();
	for (int i = 0; i < sorted.size(); ++i) {
		sum_sorted += (weight - i ) * activity[sorted[i]]; // first element (the largest) will be multiplied by the heaviest weight. 
		sum += (weight - i) * activity[A[i]];	// in the trail the activity of the decisions is also descending
		
		//sum_dec_sorted += (weight - i) * level(sort_dec_level[i]);  // after sorting the level decrease
		//sum_dec += (i + 1) * level(A[i]); // in the trail the decision level is *increasing*, hence we multiply by the reverse index 
	}
	//printf("trail sortedness = %f, level sortedness = %f\n", sum / sum_sorted, sum_dec / sum_dec_sorted);
	return sum / sum_sorted; 
}




//=================================================================================================
// Major methods:


Lit Solver::pickBranchLit(bool *reorder_now)
{    
    Var next = var_Undef;
	if (*reorder_now) { // os:				
		if (reorder_reverse) {
			do
			{	
				if (reorder_index == 0) {
					*reorder_now = false;
					reorder_sorted.clear();
					next = var_Undef;
					reorder_cpu_time += cpuTime() - reorder_start;
					break;
				}
				next = reorder_sorted[reorder_index];
				reorder_index--;
			} while (value(next) != l_Undef || !decision[next]);
		}
		else {
			do
			{				
				if (reorder_index == reorder_sorted.size()) {
					*reorder_now = false;					
					reorder_sorted.clear();
					next = var_Undef;
					reorder_cpu_time += cpuTime() - reorder_start;
					break;
				}
				next = reorder_sorted[reorder_index];
				reorder_index++;
			} while (value(next) != l_Undef || !decision[next]);
		}
		if (next != var_Undef) {			
			Lit l = mkLit(next, polarity[next]);		
			return l;
		}
	}
	assert(!(*reorder_now));
    //    Heap<VarOrderLt>& order_heap = VSIDS ? order_heap_VSIDS : order_heap_CHB;
    Heap<VarOrderLt>& order_heap = DISTANCE ? order_heap_distance : ((!VSIDS)? order_heap_CHB:order_heap_VSIDS);
    
    // Random decision:
    /*if (drand(random_seed) < random_var_freq && !order_heap.empty()){
        next = order_heap[irand(random_seed,order_heap.size())];
        if (value(next) == l_Undef && decision[next])
            rnd_decisions++; }*/ 

    // Activity based decision:
    while (next == var_Undef || value(next) != l_Undef || !decision[next])
        if (order_heap.empty())
            return lit_Undef;
        else{
#ifdef ANTI_EXPLORATION			
            if (!VSIDS){
                Var v = order_heap_CHB[0];
                uint32_t age = conflicts - canceled[v]; 
                while (age > 0){
                    double decay = pow(0.95, age); // os: the bigger the age, the smaller number is the decay. 
                    activity_CHB[v] *= decay;
                    if (order_heap_CHB.inHeap(v))
                        order_heap_CHB.increase(v);
                    canceled[v] = conflicts;
                    v = order_heap_CHB[0];
                    age = conflicts - canceled[v];
                }
            }
#endif
            // os: it actually returns max. value, because of the way VarOrderLt '()' defines 
            // lt (in reverse, i.e. true when activity[x]> activity[y]).
            next = order_heap.removeMin(); 
        }

    return mkLit(next, polarity[next]);
}

inline Solver::ConflictData Solver::FindConflictLevel(CRef cind)
{
	ConflictData data;
	Clause& conflCls = ca[cind];
	data.nHighestLevel = level(var(conflCls[0]));
	if (data.nHighestLevel == decisionLevel() && level(var(conflCls[1])) == decisionLevel())
	{
		return data;
	}

	int highestId = 0;
    data.bOnlyOneLitFromHighest = true;
	// find the largest decision level in the clause
	for (int nLitId = 1; nLitId < conflCls.size(); ++nLitId)
	{
		int nLevel = level(var(conflCls[nLitId]));
		if (nLevel > data.nHighestLevel)
		{
			highestId = nLitId;
			data.nHighestLevel = nLevel;
			data.bOnlyOneLitFromHighest = true;
		}
		else if (nLevel == data.nHighestLevel && data.bOnlyOneLitFromHighest == true)
		{
			data.bOnlyOneLitFromHighest = false;
		}
	}

	if (highestId != 0)
	{
		std::swap(conflCls[0], conflCls[highestId]);
		if (highestId > 1)
		{
			OccLists<Lit, vec<Watcher>, WatcherDeleted>& ws = conflCls.size() == 2 ? watches_bin : watches;
			//ws.smudge(~conflCls[highestId]);
			remove(ws[~conflCls[highestId]], Watcher(cind, conflCls[1]));
			ws[~conflCls[0]].push(Watcher(cind, conflCls[1]));
		}
	}

	return data;
}


/*_________________________________________________________________________________________________
|
|  analyze : (confl : Clause*) (out_learnt : vec<Lit>&) (out_btlevel : int&)  ->  [void]
|  
|  Description:
|    Analyze conflict and produce a reason clause.
|  
|    Pre-conditions:
|      * 'out_learnt' is assumed to be cleared.
|      * Current decision level must be greater than root level.
|  
|    Post-conditions:
|      * 'out_learnt[0]' is the asserting literal at level 'out_btlevel'.
|      * If out_learnt.size() > 1 then 'out_learnt[1]' has the greatest decision level of the 
|        rest of the literals. There may be others from the same level though.
|  
|________________________________________________________________________________________________@*/
//#pragma optimize( "", off )
void Solver::analyze(CRef confl, vec<Lit>& out_learnt, int& out_btlevel, int& out_lbd
#if Waerden
    ,
    short& min_Distance_z,
    short& min_Distance_n
#endif
#if Pyth
    , short& gcd
    , short& maxvar
#endif
#if Extra
    ,
    bool& is_symmetric_clause,
    bool& is_e_derived
#endif
    )
{    
    int pathC = 0;
    Lit p     = lit_Undef;
    
    // Generate conflict clause:
    //
    
    out_learnt.push();      // (leave room for the asserting literal)
    int index   = trail.size() - 1;
    int nDecisionLevel = level(var(ca[confl][0]));  // os: chrono; the decision level in the conflicting clause is not necessarily decisionLevel() 
    assert(nDecisionLevel == level(var(ca[confl][0])));
#if Extra
    is_symmetric_clause = true; // is the learned clause derived only from r_clauses (i.e. symmetric)
    is_e_derived = false;
#endif
#if Waerden
    // These will record the min distance among all premise clauses:
    min_Distance_z = 511, min_Distance_n = 511;    
#endif
#if Pyth
    gcd = 0;
    maxvar = 0;
#endif
    do {
        assert(confl != CRef_Undef); // (otherwise should be UIP)
        Clause& c = ca[confl];        

        //c.printClause("used:");
#if Waerden
        // calculating the min distance among premise clauses: 
        min_Distance_z = std::min(min_Distance_z, c.Distance_z());
        min_Distance_n = std::min(min_Distance_n, c.Distance_n());  
#endif
#if Pyth
        if (is_symmetric_clause) {            
           gcd = get_gcd(gcd, c.gcd());
           maxvar = std::max(maxvar, c.maxvar());
           assert(maxvar % gcd == 0);
        }                
#endif
#if Extra
        is_symmetric_clause = is_symmetric_clause && c.is_symmetric_clause();
        is_e_derived = is_e_derived || c.is_e_clause();
#endif
        // For binary clauses, we don't rearrange literals in propagate(), so check and make sure the first is an implied lit.
        if (p != lit_Undef && c.size() == 2 && value(c[0]) == l_False){
            assert(value(c[1]) == l_True);
            Lit tmp = c[0];
            c[0] = c[1], c[1] = tmp; } 

        // Update LBD if improved.
        if (c.learnt() && c.mark() != CORE){
            int lbd = computeLBD(c);
            if (lbd < c.lbd()){
                if (c.lbd() <= 30) c.removable(false); // Protect once from reduction.
                c.set_lbd(lbd);
                if (lbd <= core_lbd_cut){
                    learnts_core.push(confl);
                    c.mark(CORE);
                }else if (lbd <= 6 && c.mark() == LOCAL){
                    // Bug: 'cr' may already be in 'learnts_tier2', e.g., if 'cr' was demoted from TIER2
                    // to LOCAL previously and if that 'cr' is not cleaned from 'learnts_tier2' yet.
                    learnts_tier2.push(confl);
                    c.mark(TIER2); }
            }

            if (c.mark() == TIER2)
                c.touched() = conflicts;
            else if (c.mark() == LOCAL)
                claBumpActivity(c);
        }
        
        for (int j = (p == lit_Undef) ? 0 : 1; j < c.size(); j++){
            Lit q = c[j];
            register Var var_q = var(q);
            if (!seen[var_q] && level(var_q) > 0){
                if (VSIDS){
                    varBumpActivity(var_q, .5);
					if (ncb_opt) { // os:
						int lv = level(var_q); 
						if (MaxScoreInLevel[lv - 1] < activity_VSIDS[var_q]) {
							MaxScoreInLevel[lv - 1] = activity_VSIDS[var_q]; 
							//std::cout << "Updated level " << lv << " to " << MaxScoreInLevel[lv - 1] << std::endl;
						}
					}
                    add_tmp.push(q);
                }else
                    conflicted[var_q]++;
                seen[var_q] = 1;
                if (level(var_q) >= nDecisionLevel){
                    pathC++;
                } else
                    out_learnt.push(q);
            }
#if Extra
			if (is_symmetric_clause && level(var_q) == 0 && nonsym_unaries.count(var_q) == 1) {
				is_symmetric_clause = false;				
            }
#endif
#if Waerden
            if (extra_waerden && is_symmetric_clause && level(var_q) == 0) { // q was not added to the clause, and we will not look at its reason. Hence we should consider its bounds. 
                assert(varbounds.count(var_q) == 1);
                min_Distance_z = std::min(min_Distance_z, varbounds[var_q].dist_z);
                min_Distance_n = std::min(min_Distance_n, varbounds[var_q].dist_n);
            }
#endif

        }
        
        // Select next clause to look at:
		do {
			while (!seen[var(trail[index--])]);
			p  = trail[index+1];
#if Pyth
            if (level(var(p)) == 0 && reason(var(p)) != CRef_Undef) {
                int tmp = ca[reason(var(p))].gcd();
                if (tmp % gcd > 0) {
                    gcd = get_gcd(gcd, tmp);
                    //printf("!!unary gcd update ");
                }
                maxvar = std::max(maxvar, ca[reason(var(p))].maxvar());
            }
#endif

		} while (level(var(p)) < nDecisionLevel); // os: this is chrono-specific. 
                    // we do not want to develop a literal if its level is 
					// smaller than nDecisionLevel, which is the highest level in the conflicting clause. 
					// in any case we do not bypasss the current dec. level.
        
        confl = reason(var(p));
        seen[var(p)] = 0;
        pathC--;

    } while (pathC > 0);
    out_learnt[0] = ~p;
    
    // Simplify conflict clause:
    //
    int i, j;
    out_learnt.copyTo(analyze_toclear);
    if (ccmin_mode == 2){
        uint32_t abstract_level = 0;
        for (i = 1; i < out_learnt.size(); i++)
            abstract_level |= abstractLevel(var(out_learnt[i])); // (maintain an abstraction of levels involved in conflict)
        
        for (i = j = 1; i < out_learnt.size(); i++)
            if (reason(var(out_learnt[i])) == CRef_Undef || !litRedundant(out_learnt[i], abstract_level
#if Waerden
                , min_Distance_z, min_Distance_n
#endif
#if Pyth
                , gcd
                , maxvar
#endif
#if Extra
                , is_symmetric_clause
#endif
            )
            )

            out_learnt[j++] = out_learnt[i];
        
    } else if (ccmin_mode == 1){
        for (i = j = 1; i < out_learnt.size(); i++){
            Var x = var(out_learnt[i]);

            if (reason(x) == CRef_Undef)
                out_learnt[j++] = out_learnt[i];
            else{
                // TDOO: for waerden this should be considered too like in litredundant.
                Clause& c = ca[reason(var(out_learnt[i]))];
                for (int k = c.size() == 2 ? 0 : 1; k < c.size(); k++)
                    if (!seen[var(c[k])] && level(var(c[k])) > 0){
                        out_learnt[j++] = out_learnt[i];
                        break; }
            }
        }
    }else
        i = j = out_learnt.size();
    
    max_literals += out_learnt.size();
    out_learnt.shrink(i - j);
    tot_literals += out_learnt.size();

    out_lbd = computeLBD(out_learnt);
    if (out_lbd <= 6 && out_learnt.size() <= 30) // Try further minimization?
        if (binResMinimize(out_learnt
#if Waerden
            , min_Distance_z, min_Distance_n
#endif
#if Pyth
            , gcd
            , maxvar
#endif
#if Extra
            , is_symmetric_clause
            , is_e_derived
#endif
        ))
            out_lbd = computeLBD(out_learnt); // Recompute LBD if minimized.
    
    // Find correct backtrack level:
    //
    if (out_learnt.size() == 1)
        out_btlevel = 0;
    else{
        int max_i = 1;
        // Find the first literal assigned at the next-highest level:
        for (int i = 2; i < out_learnt.size(); i++)
            if (level(var(out_learnt[i])) > level(var(out_learnt[max_i])))
                max_i = i;
        // Swap-in this literal at index 1:
        Lit p             = out_learnt[max_i];
        out_learnt[max_i] = out_learnt[1];
        out_learnt[1]     = p;
        out_btlevel       = level(var(p));
    }

    if (VSIDS){
        for (int i = 0; i < add_tmp.size(); i++){ // os: add_tmp will be the learned clause. 
            Var v = var(add_tmp[i]);
			int lv = level(v); // os:
			if (lv >= out_btlevel - 1)
			{
				varBumpActivity(v, 1);
				if (ncb_opt && (MaxScoreInLevel[lv-1] < activity_VSIDS[v])) {
					MaxScoreInLevel[lv-1] = activity_VSIDS[v]; 
					//std::cout << "Updated level " << lv << "to " << MaxScoreInLevel[lv-1] << std::endl;
				}				
			}
        }
        add_tmp.clear();
    }else{
        seen[var(p)] = true;
        for(int i = out_learnt.size() - 1; i >= 0; i--){
            Var v = var(out_learnt[i]);
            CRef rea = reason(v);
            if (rea != CRef_Undef){
                const Clause& reaC = ca[rea];
                for (int i = 0; i < reaC.size(); i++){
                    Lit l = reaC[i];
                    if (!seen[var(l)]){
                        seen[var(l)] = true;
                        almost_conflicted[var(l)]++;
                        analyze_toclear.push(l); } } } } }

    for (int j = 0; j < analyze_toclear.size(); j++) seen[var(analyze_toclear[j])] = 0;    // ('seen[]' is now cleared)
}


// Try further learnt clause minimization by means of binary clause resolution.
bool Solver::binResMinimize(vec<Lit>& out_learnt
#if Waerden
    , short& min_distance_z, short& min_distance_n
#endif
#if Pyth
    , short& gcd
    , short& maxvar
#endif
#if Extra
    , bool& is_symmetric_clause
    , bool& is_extra_derived
#endif
)
{ 
    // Preparation: remember which false variables we have in 'out_learnt'.
    counter++;
    for (int i = 1; i < out_learnt.size(); i++)
        seen2[var(out_learnt[i])] = counter;

    // Get the list of binary clauses containing 'out_learnt[0]'.
    const vec<Watcher>& ws = watches_bin[~out_learnt[0]];

    int to_remove = 0;
    for (int i = 0; i < ws.size(); i++){
        Lit the_other = ws[i].blocker;
        // Does 'the_other' appear negatively in 'out_learnt'?
        if (seen2[var(the_other)] == counter && value(the_other) == l_True){
#if Extra
            Clause& c = ca[ws[i].cref];

            is_symmetric_clause = is_symmetric_clause && c.is_symmetric_clause();
            is_extra_derived = is_extra_derived || c.is_e_clause();
#endif
#if Waerden
            min_distance_z = std::min(min_distance_z, c.Distance_z());
            min_distance_n = std::min(min_distance_n, c.Distance_n());
#endif
#if Pyth
            gcd = get_gcd(gcd, c.gcd());
            maxvar = std::max(maxvar, c.maxvar());
#endif

            to_remove++;
            seen2[var(the_other)] = counter - 1; // Remember to remove this variable.
        }
    }

    // Shrink.
    if (to_remove > 0) {
        int last = out_learnt.size() - 1;
        for (int i = 1; i < out_learnt.size() - to_remove; i++)
            if (seen2[var(out_learnt[i])] != counter)
                out_learnt[i--] = out_learnt[last--];         
        out_learnt.shrink(to_remove);
    }

    return to_remove != 0;
}


// Check if 'p' can be removed. 'abstract_levels' is used to abort early if the algorithm is
// visiting literals at levels that cannot be removed later.
// os: How it works: we follow the literal p back to its reason clause, recursively. If all 
// the way it uses literals that are on the learned clause anyway, it means that this literal
// can be resolved away. Example: the learned clause is (-13 12 14 15). reason(-13) = (13 14 15).
// hence if we resolve these two we get (12 14 15), i.e., -13 can be removed. 
// It could have been a longer chain.
// It uses the global 'seen', which is initially set by analyze, so for every variable v that is in 
// out_learnt, seen[v]=1. Also analyze_toclear contains out_learnt. 

bool Solver::litRedundant(Lit p,
 uint32_t abstract_levels
#if Waerden
    , short & min_dist_z
    , short & min_dist_n
#endif
#if Pyth
    , short & gcd
    , short & maxvar
#endif
#if Extra
, bool& issym
#endif
)
{
#if Extra
    bool tmp_issym = issym;
#endif
#if Waerden
    short tmp_min_dist_z = min_dist_z, tmp_min_dist_n = min_dist_n;
#endif
#if Pyth
    short tmp_gcd = gcd;
    short tmp_maxvar = maxvar;
#endif
    analyze_stack.clear(); analyze_stack.push(p);
    int top = analyze_toclear.size();
    while (analyze_stack.size() > 0){
        assert(reason(var(analyze_stack.last())) != CRef_Undef);
        Clause& c = ca[reason(var(analyze_stack.last()))]; analyze_stack.pop();
#if Waerden
        tmp_min_dist_z = std::min(tmp_min_dist_z, c.Distance_z());
        tmp_min_dist_n = std::min(tmp_min_dist_n, c.Distance_n());
#endif
#if Extra
        tmp_issym = tmp_issym && c.is_symmetric_clause();
#endif
#if Pyth        
        if (tmp_issym) {
            tmp_gcd = get_gcd(tmp_gcd, c.gcd());
            tmp_maxvar = std::max(tmp_maxvar, c.maxvar());
        }
#endif

/*        c.printClause("reason clause");
        for (int i = 0; i < seen.size(); ++i)
            printf("%d: %d,", i + 1, seen[i]);
        printf("\n");
        */
        // Special handling for binary clauses like in 'analyze()'.
        if (c.size() == 2 && value(c[0]) == l_False){
            assert(value(c[1]) == l_True);
            Lit tmp = c[0];
            c[0] = c[1], c[1] = tmp; }

        for (int i = 1; i < c.size(); i++){
            Lit p  = c[i];
            if (!seen[var(p)] && level(var(p)) > 0){
                if (reason(var(p)) != CRef_Undef && (abstractLevel(var(p)) & abstract_levels) != 0){
                    seen[var(p)] = 1;
                    analyze_stack.push(p);
                    analyze_toclear.push(p);
                }else{
                    for (int j = top; j < analyze_toclear.size(); j++)
                        seen[var(analyze_toclear[j])] = 0;
                    analyze_toclear.shrink(analyze_toclear.size() - top);
                    return false;
                }
            }
        }
    }
#if Extra
    issym = tmp_issym;
#endif
#if Waerden
    min_dist_z = tmp_min_dist_z;
    min_dist_n = tmp_min_dist_n;
#endif
#if Pyth
    gcd = tmp_gcd;
    maxvar = tmp_maxvar;
#endif

    return true;
}

//#pragma optimize( "", on )
/*_________________________________________________________________________________________________
|
|  analyzeFinal : (p : Lit)  ->  [void]
|  
|  Description:
|    Specialized analysis procedure to express the final conflict in terms of assumptions.
|    Calculates the (possibly empty) set of assumptions that led to the assignment of 'p', and
|    stores the result in 'out_conflict'.
|________________________________________________________________________________________________@*/
void Solver::analyzeFinal(Lit p, vec<Lit>& out_conflict)
{
    out_conflict.clear();
    out_conflict.push(p);

    if (decisionLevel() == 0)
        return;

    seen[var(p)] = 1;

    for (int i = trail.size()-1; i >= trail_lim[0]; i--){
        Var x = var(trail[i]);
        if (seen[x]){
            if (reason(x) == CRef_Undef){
                assert(level(x) > 0);
                out_conflict.push(~trail[i]);
            }else{
                Clause& c = ca[reason(x)];
                for (int j = c.size() == 2 ? 0 : 1; j < c.size(); j++)
                    if (level(var(c[j])) > 0)
                        seen[var(c[j])] = 1;
            }
            seen[x] = 0;
        }
    }

    seen[var(p)] = 0;
}

// os: here is where the assignment happens
void Solver::uncheckedEnqueue(Lit p, int level, CRef from)
{
    std::lock_guard<std::mutex> lock(mtx);
#if THREADS
    if (value(p) == l_False) {
        std::cout << "conflict !!" << std::endl; return;
    }
    if (!(value(p) == l_Undef)) return; // already assigned
#endif    
    
    assert(value(p) == l_Undef); 
    Var x = var(p);
	
    if (!VSIDS){
        picked[x] = conflicts;
        conflicted[x] = 0;
        almost_conflicted[x] = 0;
#ifdef ANTI_EXPLORATION
        uint32_t age = conflicts - canceled[var(p)];
        if (age > 0){
            double decay = pow(0.95, age);
            activity_CHB[var(p)] *= decay;
            if (order_heap_CHB.inHeap(var(p)))
                order_heap_CHB.increase(var(p));
        }
#endif
    }	
    assigns[x] = lbool(!sign(p));
    vardata[x] = mkVarData(from, level); // os: this updates 'reason' and level.
#if Pyth
    /*!!
    if (level == 0) { // This actually happens
        gcd_unaries[x] = gcd;
    }*/
#endif
    trail.push_(p);
}
void node(Lit l) {
    //std::cout << toInt(var(l)) << " [style=filled,fillcolor=grey];" << std::endl;
}

void edge(Lit from, Lit to, std::string col = "black") {
    //std::cout << toInt(var(from)) << " -> " << toInt(var(to)) << " [color = " << col << "];\n";
}

#if THREADS
void Solver::threadFunction(int id) {
//    std::lock_guard<std::mutex> lock(mtx); // Lock the mutex 
    //std::cout << id<< "," << std::this_thread::get_id() << " is running." << std::endl;
    std::vector<Lit> tr_trail;
    int x = qhead;
    while (x < trail.size()) {
        assert(trail[x].x >= 0);
        tr_trail.push_back(trail[x++]);
    }
    
    if (id != 0) std::shuffle(tr_trail.begin(), tr_trail.end(),g);    
    //std::cout << std::this_thread::get_id() << " after shuffle." << std::endl;
    //{
        //std::lock_guard<std::mutex> lock(mtx);
        //std::cout << std::this_thread::get_id() << " acquired lock" << std::endl;
    thread_conf = propagate_thread(tr_trail);    
        // lock_guard is unlocked automatically once it gets out of scope. 
    //}
    //std::cout << std::this_thread::get_id() << " finished." << std::endl;
    terminate_threads = true;
}

CRef Solver::spawnThreads(int workers) {
    std::vector<std::thread> threads(workers);
    
    for (int i = 0; i < workers && !terminate_threads; ++i) {
        threads[i] = std::thread(&Solver::threadFunction, this, i);
    }
    for (auto& t : threads) {
        if (t.joinable()) {
      //      std::cout << "counter = " << tcounter++ << "," << t.get_id() << std::endl;
            t.join();
        }
    }
    terminate_threads = false;
    return thread_conf;
}

// retuen value: true => continue the loop, false => terminate the loop. 
bool Solver::prop_body(bool first_call,
    Watcher*& i, Watcher*& j, Watcher*& end,
    Lit& p,
    int currLevel,
    std::vector<watcher_pair_t>& locked,
    CRef& confl, bool& success
) {
    if (terminate_threads) {
      //  std::cout << "killed" << std::this_thread::get_id() << std::endl;
        return false;
    }
    stat_prop_watched++;
    // Try to avoid inspecting the clause:
    Lit blocker = i->blocker;
    if (value(blocker) == l_True) {
        *j++ = *i++; return false;
    }

    // Make sure the false literal is data[1]:
    CRef    cr = i->cref;
    Clause& c = ca[cr];
    // if the clause is locked, save it for later    
    
    if (c.cmtx()) {
        // if it is the first call, we insert it to the locked_list. Otherwise we just leave it there. 
        if (first_call) {
            watcher_pair_t wp(i, j);
            locked.push_back(wp);
            i++;
        }
        success = false;
        return true;
    }
    // lock the clause
    c.cmtx(true);

    Lit      false_lit = ~p;
    if (c[0] == false_lit){        
        c[0] = c[1], c[1] = false_lit;
    }

    // assert(c[1] == false_lit);
    // TODO: no reason that a literal's watch list will be visited by more than a single thread. Keep a vector from literals to bool, whether it was already handled. 
    if (c[1] != false_lit) { // this means that this clause was visited by another thread
        c.cmtx(false); 
        success = true;
        return true;
        // need to think about two cases:
        // 1) there was another thread that looked at this clause from the same literal p. In this case p is no longer watching it, so we can skip. 
        // 2) there was another thread that looked at this clause from a different literal, e.g., q. 
        // 
        // What matters is that there are 2 false literals, so we do not need to visit it. 
       // lbool b1 = value(c[0]), b2 = value(c[1]);
        //assert(value(c[0]) != l_False && value(c[1]) != l_False); 

    }
    i++;

    // If 0th watch is true, then clause is already satisfied.
    Lit     first = c[0];
    Watcher w = Watcher(cr, first);
    if (first != blocker && value(first) == l_True) {
        *j++ = w;
        c.cmtx(false);
        return true;
    }

    // Look for new watch:
    for (int k = 2; k < c.size(); k++)
        if (value(c[k]) != l_False) {
            stat_prop_searchNewWatch++;
            c[1] = c[k]; c[k] = false_lit;
            watches[~c[1]].push(w);

            goto NextClause;
        }

    // Did not find watch -- clause is unit under assignment:
    *j++ = w;
    if (value(first) == l_False) { // os: found a conflict
        confl = cr;
        qhead = trail.size(); // This will end the loop.        // TODO THREADS              

        // Copy the remaining watches:
        while (i < end)
            *j++ = *i++;
        // todo: copy here i's from the locked list
        
    }
    else
    {
        if (currLevel == decisionLevel())
        {
            uncheckedEnqueue(first, currLevel, cr);
        }
        else // os: chrono
        {
            int nMaxLevel = currLevel;
            int nMaxInd = 1;
            // pass over all the literals in the clause and find the one with the biggest level
            for (int nInd = 2; nInd < c.size(); ++nInd)
            {
                int nLevel = level(var(c[nInd]));
                if (nLevel > nMaxLevel)
                {
                    nMaxLevel = nLevel;
                    nMaxInd = nInd;
                }
            }

            if (nMaxInd != 1)
            {
                std::swap(c[1], c[nMaxInd]);
                *j--; // undo last watch
                watches[~c[1]].push(w);
            }
            uncheckedEnqueue(first, nMaxLevel, cr);
        }
    }

NextClause:
    c.cmtx(false);
    success = true; // relevant in the 2nd loop, for popping the element from the locked list. 
    return true;
}

// propagation, given a literal order tr_trail. 
CRef Solver::propagate_thread(std::vector<Lit>& tr_trail)
{
    CRef    confl = CRef_Undef;
    int     num_props = 0;
    int last_qhead = -1;
    
    // TODO THREADS: we can't have that a thread cleans them while another thread uses them
    {
        std::lock_guard<std::mutex> lock(mtx);
        watches.cleanAll();
        watches_bin.cleanAll();
    }
        
    static int init_counter = 0;
    int edge_counter = 0;
    stat_prop_called++;

    std::vector<std::pair<Watcher*, Watcher*>> locked; // clauses that were locked when this thread tried to acess them. 
StartPropagate:
    for (Lit p: tr_trail) {  
        //std::cout << std::this_thread::get_id() << " literal " << p.x << std::endl;
        if (terminate_threads) {           
            //std::cout << "killed" << std::this_thread::get_id() << std::endl;
            return CRef_Undef;
        }
        int currLevel = level(var(p));
        vec<Watcher>& ws = watches[p];
        Watcher* i, * j, * end;
        num_props++;

        vec<Watcher>& ws_bin = watches_bin[p];  // Propagate binary clauses first.
        for (int k = 0; k < ws_bin.size(); k++) {
            if (terminate_threads) {
               // std::cout << "killed" << std::this_thread::get_id() << std::endl;
                return CRef_Undef;
            }
            stat_prop_binary++;
            Lit the_other = ws_bin[k].blocker;
            if (value(the_other) == l_False) {
                confl = ws_bin[k].cref;
                stat_binary_prop++;
                goto exit;
            }
            else if (value(the_other) == l_Undef)
            {
                uncheckedEnqueue(the_other, currLevel, ws_bin[k].cref);
                stat_binary_prop++;
            }
        }
       
        bool success = false;
        for (i = j = (Watcher*)ws, end = i + ws.size(); i != end;) {
            bool res = prop_body(true, i, j, end, p, currLevel, locked, confl, success);
            if (!res || confl != CRef_Undef) goto exit;
        }

        while (!locked.empty()) {
            /*std::cout << std::this_thread::get_id() << "." << p.x << ", (" << locked[0].first << "," << locked[0].second << ")" << std::endl;
           */ 
            success = false;
            watcher_pair_t wp = locked.back();
            bool res = prop_body(false, wp.first, wp.second, end, p, currLevel, locked, confl, success);
            if (!res || confl != CRef_Undef) goto exit;
            if (success) locked.pop_back();
        }
        ws.shrink(i - j);
    }

exit:
    if (!reorder_now) propagations += num_props;    
    return confl;
}

#endif

/*_________________________________________________________________________________________________
|
|  propagate : [void]  ->  [Clause*]
|  
|  Description:
|    Propagates all enqueued facts. If a conflict arises, the conflicting clause is returned,
|    otherwise CRef_Undef.
|  
|    Post-conditions:
|      * the propagation queue is empty, even if there was a conflict.
|________________________________________________________________________________________________@*/
CRef Solver::propagate(bool graph)
{
#if THREADS
    return spawnThreads(2);
#endif    
    CRef    confl     = CRef_Undef;
    int     num_props = 0;
    CRef ant_forbcpreduce; // used when BCPReduceWeight > 0
    int last_qhead = -1;
    watches.cleanAll();
    watches_bin.cleanAll();    
    

        
    std::map<int, std::pair<CRef, int> > ForReduce_implicationCount;
    std::vector<CRef> ForReduce_reasons;
    std::vector<float> ForReduce_weights;
    const float alpha = 0.95;
    static int init_counter = 0;
    int edge_counter = 0;
    stat_prop_called++;
    static bool dot = false;
    static int calls_counter = 0;
    if (graph && dot) calls_counter++;    

StartPropagate:
    // symmetry_sel
    vec<Lit> symmetrical;

    while (qhead < trail.size()){
        Lit            p   = trail[qhead++];     // 'p' is enqueued fact to propagate.
        if (value(p) != l_Undef) continue; // os, for threads
        int currLevel = level(var(p));    
        vec<Watcher>&  ws  = watches[p];
        Watcher        *i, *j, *end;
        num_props++;


        if (BCPReduceWeight > 0.0) // os:
        {            
            // calc implicationcount of previous literal: 
            if (ForReduce_implicationCount.count(last_qhead) == 1) {
                assert(last_qhead == qhead - 1);
				ant_forbcpreduce = ForReduce_implicationCount[last_qhead].first;
				unsigned int implied = trail.size() - ForReduce_implicationCount[last_qhead].second;
				//claBumpActivity(ca[ant_forbcpreduce], implied * BCPReduceWeight);                         
                // We store them and use them later if it turns out we reached a conflict. 
                ForReduce_reasons.push_back(ant_forbcpreduce);
                ForReduce_weights.push_back(implied * BCPReduceWeight);
                last_qhead = -1;
            }
			// start counting for current literal
			ant_forbcpreduce = reason(var(p));
            if (ant_forbcpreduce != CRef_Undef) {
                Clause& c = ca[ant_forbcpreduce];
                
                if (c.learnt() && c.mark() !=  CORE) {
                    std::pair<CRef, int> cls_counter(ant_forbcpreduce, trail.size());
                    ForReduce_implicationCount[qhead] = cls_counter;
                    last_qhead = qhead;
                }
            }
        }

        vec<Watcher>& ws_bin = watches_bin[p];  // Propagate binary clauses first.
        for (int k = 0; k < ws_bin.size(); k++){
			stat_prop_binary++;
            Lit the_other = ws_bin[k].blocker;
            if (value(the_other) == l_False) {
                confl = ws_bin[k].cref;
#ifdef LOOSE_PROP_STAT 
                stat_binary_prop++;
                if (graph) {
                    edge_counter++;                    
                }

                goto exit;
#else
                goto ExitProp;
#endif
            } else if(value(the_other) == l_Undef)
            {

                uncheckedEnqueue(the_other, currLevel, ws_bin[k].cref);
#if Extra              
                if (currLevel == 0) {
                    if (nonsym_unaries.count(var(p)) == 1 || !ca[ws_bin[k].cref].is_symmetric_clause()) nonsym_unaries.insert(var(the_other));
                    vec<Lit> v;
                    v.push(the_other);
                    if (extra_from_generators) 
                        generators_generate(v, ExtraClauses_gen);
#if Pyth
                    if (ca[ws_bin[k].cref].is_symmetric_clause() && extra_pyth) {
                        std::vector<Lit> v;
                        v.push_back(the_other);
                        pyth_generate(v, 1, var(the_other) + 1, ExtraClauses, GcdOfEClauses, MaxvarOfEClauses);
                    }
#endif
#if Waerden
                    if (extra_waerden && nonsym_unaries.count(var(p)) == 0) {
                        varbounds[var(the_other)] = {
                            std::min(varbounds[var(p)].dist_z, ca[ws_bin[k].cref].Distance_z()),
                            std::min(varbounds[var(p)].dist_n, ca[ws_bin[k].cref].Distance_n())
                        };
                    }
#endif
                }
#endif

                stat_binary_prop++;                
                if (graph) {
                    //edge(p, the_other, "blue"); 
                    edge_counter++;
                    dot = true; //!
                }
			}
        }
        /*if (stat_binary_prop > prev_stat_bin) {
            printf("stat_binary_prop = %d\n", stat_binary_prop);
            prev_stat_bin = stat_binary_prop;
        }*/

        for (i = j = (Watcher*)ws, end = i + ws.size();  i != end;){
			stat_prop_watched++;
            // Try to avoid inspecting the clause:
            Lit blocker = i->blocker;
            if (value(blocker) == l_True){
                *j++ = *i++; continue; }

            // Make sure the false literal is data[1]:
            CRef     cr        = i->cref;
            Clause&  c         = ca[cr];
            Lit      false_lit = ~p;
            if (c[0] == false_lit)
                c[0] = c[1], c[1] = false_lit;
            assert(c[1] == false_lit);
            i++;

            // If 0th watch is true, then clause is already satisfied.
            Lit     first = c[0];
            Watcher w     = Watcher(cr, first);
            if (first != blocker && value(first) == l_True){
                *j++ = w; continue; }

            // Look for new watch:
            for (int k = 2; k < c.size(); k++)
                if (value(c[k]) != l_False){
					stat_prop_searchNewWatch++;
                    c[1] = c[k]; c[k] = false_lit;
                    watches[~c[1]].push(w);
                    goto NextClause; }

            // Did not find watch -- clause is unit under assignment:
            *j++ = w;
            if (value(first) == l_False){ // os: found a conflict
                confl = cr;
                qhead = trail.size(); // This will end the loop. 

                //node(first);
                //edge(p, first);
                edge_counter++;
                
                // Copy the remaining watches:
                while (i < end)
                    *j++ = *i++;

            } else
            {
				if (currLevel == decisionLevel())
				{                    
					uncheckedEnqueue(first, currLevel, cr);
#if Extra              
                    if (currLevel == 0) {                        
                        if (nonsym_unaries.count(var(p)) == 1 || !ca[cr].is_symmetric_clause()) nonsym_unaries.insert(var(first));
                        vec<Lit> v;
                        v.push(first);
                        if (extra_from_generators)
                            generators_generate(v, ExtraClauses_gen);
#if Pyth
                        if (ca[cr].is_symmetric_clause() && extra_pyth) {
                            std::vector<Lit> v;
                            v.push_back(first);
                            pyth_generate(v, 1, var(first) + 1, ExtraClauses, GcdOfEClauses, MaxvarOfEClauses);
                        }
#endif
#if Waerden
                        if (extra_waerden && nonsym_unaries.count(var(p)) == 0) {
                            varbounds[var(first)] = {
                                std::min(varbounds[var(p)].dist_z, ca[cr].Distance_z()),
                                std::min(varbounds[var(p)].dist_n, ca[cr].Distance_n())
                            };
                        }
#endif
                    
                    }
#endif

                    if (graph) {
                        //edge(p, first); //!
                        edge_counter++;
                        dot = true; //!
                    }
				}
				else // os: chrono
				{
					int nMaxLevel = currLevel;
					int nMaxInd = 1;
					// pass over all the literals in the clause and find the one with the biggest level
					for (int nInd = 2; nInd < c.size(); ++nInd)
					{
						int nLevel = level(var(c[nInd]));
						if (nLevel > nMaxLevel)
						{
							nMaxLevel = nLevel;
							nMaxInd = nInd;
						}
					}

					if (nMaxInd != 1)
					{
						std::swap(c[1], c[nMaxInd]);
						*j--; // undo last watch
						watches[~c[1]].push(w);
					}
					
					uncheckedEnqueue(first, nMaxLevel, cr);
#if Extra              
                    if (currLevel == 0) {
                        if (nonsym_unaries.count(var(p)) == 1 || !ca[cr].is_symmetric_clause()) nonsym_unaries.insert(var(first));
                        vec<Lit> v;
                        v.push(first);
                        if (extra_from_generators)
                           generators_generate(v, ExtraClauses_gen);
#if Pyth
                        if (ca[cr].is_symmetric_clause() && extra_pyth) {
                            std::vector<Lit> v;
                            v.push_back(first);
                            pyth_generate(v, 1, var(first) + 1, ExtraClauses, GcdOfEClauses, MaxvarOfEClauses);
                        }
#endif
#if Waerden
                        if (extra_waerden && nonsym_unaries.count(var(p)) == 0) {
                            varbounds[var(first)] = {
                                std::min(varbounds[var(p)].dist_z, ca[cr].Distance_z()),
                                std::min(varbounds[var(p)].dist_n, ca[cr].Distance_n())
                            };
                        }
#endif
                    }
#endif

                    if (graph) {
                       // edge(p, first, "red");//!
                        edge_counter++;
                        dot = true;//!
                    }
				}
			}

NextClause:;
        }
        ws.shrink(i - j);
    }
     
#pragma region symmetry_sel
    // symmetry_sel
    if (useSymmetry) {
        /* check if some existing symmetrical clause in selClauses became unit or conflicting */
        bool had_unit = false;
        for (; confl == CRef_Undef && qhead_sel < trail.size(); ++qhead_sel) {
            Lit prop = trail[qhead_sel];  
            vec<int>& clWatches = *(selClausesOfWatcher[toInt(prop)]);

            int watchedclause_i = 0;
            while (watchedclause_i < clWatches.size()) {
                int currentclause = clWatches[watchedclause_i];

                // if the clause is no longer in selClauses - erase watch
                if (currentclause >= selClausesOrigProp.size()) {
                    clWatches.swapErase(watchedclause_i);
                    continue;
                }
                assert(selClausesIndices.size() > currentclause + 1);

                // find watched literal
                int c_start = selClausesIndices[currentclause];

                // if the clause is satisfied it is ignored for now
                if (value(selClauses[c_start]) == l_True || value(selClauses[c_start + 1]) == l_True) {
                    ++watchedclause_i;
                    continue;
                }

                // erase watch - a new watch is needed
                clWatches.swapErase(watchedclause_i);

                // check if the watch is indeed in the first two literals of the clause.
                // if not (e.g. clause already added) , continue.
                int watch = 0;
                while (selClauses[c_start + watch] != ~prop && watch < 2) {
                    ++watch;
                }
                if (watch >= 2) {
                    continue;
                }

                // find a new watch, otherwise unit or conflicting
                int c_end = selClausesIndices[currentclause + 1];
                watch += c_start; // now watch is the absolute index of the watch, instead of the index relative to c_start
                assert(value(selClauses[watch]) == l_False);
                for (int i = c_start + 2; i < c_end; ++i) {
                    if (value(selClauses[i]) != l_False) {
                        Lit tmp = selClauses[i];
                        selClauses[i] = selClauses[watch];
                        selClauses[watch] = tmp;
                        break;
                    }
                }

                // if a new watch found then add the clause's index to the new watcher's list of watched sel clauses, and continue
                if (value(selClauses[watch]) != l_False) {
                    selClausesOfWatcher[toInt(~selClauses[watch])]->push(currentclause);
                    continue;
                }

                // the clause is unit or conflicting

                // recover the original clause (not shortened), put it in 'symmetrical'.
                selClausesGen[currentclause]->getSymmetricalClause(ca[reason(selClausesOrigProp[currentclause])], symmetrical);

                // minimize the clause with a self-subsumption clause simplification step
                minimizeClause(symmetrical);

                // if we found a level 0 clause
                if (symmetrical.size() < 2) {
                    assert(symmetrical.size() == 1);
                    cancelUntil(0);

                    if (value(symmetrical[0]) == l_Undef) { // unit clause at level 0
                        ++symselprops;
                        uncheckedEnqueue(symmetrical[0]);
                        goto StartPropagate;
                    }
                    else if (value(symmetrical[0]) == l_False) { // conflict clause at level 0 - the formula is unsat
                        ++symselconfls;
                        confl = CRef_Unsat;
                        goto exit;
                    }
                }

                // the clause is not a level 0 clause
                prepareWatches(symmetrical);
                assert(value(symmetrical[1]) == l_False);
            
                confl = addClauseFromSymmetry(symmetrical);

                if (confl == CRef_Undef) { // the added clause is a unit clause
                    ++symselprops;
                //    goto StartPropagate; // os:
                    had_unit = true; // os:
                }
                else { // the added clause is a conflict clause
                    ++symselconfls;
                    if (had_unit) std::cout << "***" << std::endl;
                    goto exit;
                }
            }
        }
        if (had_unit) goto StartPropagate; // os:

        /* generate new symmetric clauses from propagated literals' explanation clauses */

        for (; confl == CRef_Undef && qhead_gen < trail.size(); ++qhead_gen, watchidx = 0) {
            Lit currentGenLit = trail[qhead_gen];

            // currently not supporting chrono backtracking. when chrono backtracking is supported this assertion should be removed
            assert(level(var(currentGenLit)) == decisionLevel());

            int genStartIndex = generatorsOfVarIndices[var(currentGenLit)];
            int genEndIndex = generatorsOfVarIndices[var(currentGenLit) + 1];

            // dealing with level 0 literals
            if (level(var(currentGenLit)) == 0) {
                for (int i = genStartIndex; i < genEndIndex; ++i) {
                    Lit symLit = generatorsOfVar[i]->getImage(currentGenLit);

                    if (value(symLit) == l_Undef) { // found symmetric unit clause at level 0
                        ++symgenprops;
                        uncheckedEnqueue(symLit);
                        goto StartPropagate;
                    }

                    else if (value(symLit) == l_False) { // found symmetric conflict clause at level 0
                        ++symgenconfls;
                        confl = CRef_Unsat;
                        goto exit;
                    }
                }
            }

            CRef reasonOfGenLit = reason(var(currentGenLit));
            if (reasonOfGenLit == CRef_Undef) { // choice literal
                continue;
            }

            // add the symmetric clauses of reasonOfGenLit to selClauses
            for (; watchidx < genEndIndex - genStartIndex; ++watchidx) {
                SymGenerator* g = generatorsOfVar[genStartIndex + watchidx];
                int result = addSelClause(g, currentGenLit);

                // if we encountered a symmetrical unit clause or a symmetrical conflict clause it should be added to the learned clause store
                if (result < 2) {
                    // recover the original clause (not shortened)
                    g->getSymmetricalClause(ca[reasonOfGenLit], symmetrical);

                    // minimize the clause with a self-subsumption clause simplification step
                    minimizeClause(symmetrical);

                    // if we found a level 0 clause
                    if (symmetrical.size() < 2) {
                        assert(symmetrical.size() == 1);
                        cancelUntil(0);
                        if (value(symmetrical[0]) == l_Undef) { // unit clause at level 0
                            ++symgenprops;
                            uncheckedEnqueue(symmetrical[0]);
                            goto StartPropagate;
                        }
                        else if (value(symmetrical[0]) == l_False) { /// conflict clause at level 0 - the formula is unsat
                            ++symgenconfls;
                            confl = CRef_Unsat;
                            goto exit;
                        }
                    }

                    // the clause is not a level 0 clause
                    prepareWatches(symmetrical);
                    assert(value(symmetrical[1]) == l_False); 
                    confl = addClauseFromSymmetry(symmetrical);

                    if (confl == CRef_Undef) { // the added clause is a unit clause
                        ++symgenprops;
                        goto StartPropagate;
                    }
                    else { // the added clause is a conflict clause
                        ++symgenconfls;
                        goto exit;
                    }

                }

            }

        }

    }

#pragma endregion symmetry_sel 

#ifndef LOOSE_PROP_STAT 
ExitProp:;
    if (reorder_now) reorder_propagations += num_props;
	//else propagations += num_props;
    simpDB_props -= num_props;
#endif
exit:
    if (!reorder_now) propagations += num_props;
    if (graph) {
        stat_propogations += num_props;
        if (confl == CRef_Undef) { // no conflict
            stat_size_sat += edge_counter;
          //if (conflicts %10 == 0) std::cout << edge_counter << "," << num_props << std::endl;
        }
        else { // conflict
            stat_size_unsat += edge_counter;
            stat_conflicts++;
            stat_smoothed_edges = alpha * stat_smoothed_edges + (1 - alpha) * edge_counter;
            // if (calls_counter % 20 == 0) printf("%d, %f\n", edge_counter, stat_smoothed_edges);
             // we record the value of stat_smoothed_edges after 30 times, to serve as the reference point
             // for the starting value. 
            if (++init_counter == 30) stat_smoothed_implications_begin = stat_smoothed_edges;
           // std::cout << edge_counter << std::endl;

            if (BCPReduceWeight > 0.0) {
                for (unsigned int i = 0; i < ForReduce_reasons.size(); ++i) {
                    claBumpActivity(ca[ForReduce_reasons[i]], ForReduce_weights[i]);
                }
            }

        }
    }
    return confl;
}


/*_________________________________________________________________________________________________
|
|  reduceDB : ()  ->  [void]
|  
|  Description:
|    Remove half of the learnt clauses, minus the clauses locked by the current assignment. Locked
|    clauses are clauses that are reason to some assignment. Binary clauses are never removed.
|________________________________________________________________________________________________@*/
struct reduceDB_lt { 
    ClauseAllocator& ca;
    reduceDB_lt(ClauseAllocator& ca_) : ca(ca_) {}
    bool operator () (CRef x, CRef y) const { 
        return ca[x].activity() < ca[y].activity();  
    }    
};


void Solver::reduceDB()
{
    int     i, j;
//    std::cout << "1000" << std::endl; // Just to identify the reduce points 
    stat_reduced++;
    int limit = learnts_local.size() / reduceratio;
    
    sort(learnts_local, reduceDB_lt(ca));
	
    //printf("activity:\n");
    //for (int i = 0; i < learnts_local.size(); ++i) {  
    //    float act = ca[learnts_local[i]].activity();
    //    printf("(%d: %f),", i, act);
    //}
    //printf("\n");
    if (act_reduce) {
        double sum = 0;
        for (int i = 0; i < learnts_local.size(); ++i) {
            float act = ca[learnts_local[i]].activity();
            sum += act;
        }
        double sum1 = 0;

        for (int i = learnts_local.size() - 1; i >= 0; --i) {
            float act = ca[learnts_local[i]].activity();
            sum1 += act;
            if (sum1 > sum / reduceratio) {
                limit = i;
                break;
            }
        }
        printf("limit = %d out of %d\n", limit, learnts_local.size());
    }
        
    for (i = j = 0; i < learnts_local.size(); i++){
        Clause& c = ca[learnts_local[i]];

// reducedb
        if (c.mark() == LOCAL) { // os: during simplify_learnt a local clause can 
            // become tier2 without being removed from the learnt_local list, hence it
            // is checked here. 
            if (c.removable() && !locked(c) && i < limit)
            {
                removeClause(learnts_local[i]);                
            }
            else {
                if (!c.removable()) {
                    limit++;
                    c.removable(true);
                }
                learnts_local[j++] = learnts_local[i];
            }
        }       
    }
    
    learnts_local.shrink(i - j);
    checkGarbage();
}
void Solver::reduceDB_Tier2()
{
    int i, j;
    for (i = j = 0; i < learnts_tier2.size(); i++){
        Clause& c = ca[learnts_tier2[i]];
        if (c.mark() == TIER2)
            if (!locked(c) && c.touched() + 30000 < conflicts){
                learnts_local.push(learnts_tier2[i]);
                c.mark(LOCAL);
                //c.removable(true);
                c.activity() = 0;
                claBumpActivity(c);
            }else
                learnts_tier2[j++] = learnts_tier2[i];
    }
    learnts_tier2.shrink(i - j);
}


void Solver::removeSatisfied(vec<CRef>& cs)
{
    int i, j;
    for (i = j = 0; i < cs.size(); i++){
        Clause& c = ca[cs[i]];
        if (satisfied(c))
            removeClause(cs[i]);
        else
            cs[j++] = cs[i];
    }
    cs.shrink(i - j);
}

void Solver::safeRemoveSatisfied(vec<CRef>& cs, unsigned valid_mark)
{
    int i, j;
    for (i = j = 0; i < cs.size(); i++){
        Clause& c = ca[cs[i]];
        if (c.mark() == valid_mark)
            if (satisfied(c))
                removeClause(cs[i]);
            else
                cs[j++] = cs[i];
    }
    cs.shrink(i - j);
}

void Solver::rebuildOrderHeap()
{
    vec<Var> vs;
    for (Var v = 0; v < nVars(); v++)
        if (decision[v] && value(v) == l_Undef)
            vs.push(v);

    order_heap_CHB  .build(vs);
    order_heap_VSIDS.build(vs);
    order_heap_distance.build(vs);
}


/*_________________________________________________________________________________________________
|
|  simplify : [void]  ->  [bool]
|  
|  Description:
|    Simplify the clause database according to the current top-level assigment. Currently, the only
|    thing done here is the removal of satisfied clauses, but more things can be put here.
|________________________________________________________________________________________________@*/
bool Solver::simplify()
{
    assert(decisionLevel() == 0);

    if (!ok || propagate() != CRef_Undef)
        return ok = false;

    if (nAssigns() == simpDB_assigns || (simpDB_props > 0))
        return true;

    // Remove satisfied clauses:
    removeSatisfied(learnts_core); // Should clean core first.
    safeRemoveSatisfied(learnts_tier2, TIER2);
    safeRemoveSatisfied(learnts_local, LOCAL);
    if (remove_satisfied)        // Can be turned off.
        removeSatisfied(clauses);
    checkGarbage();
    rebuildOrderHeap();

    simpDB_assigns = nAssigns();
    simpDB_props   = clauses_literals + learnts_literals;   // (shouldn't depend on stats really, but it will do for now)

    return true;
}

// pathCs[k] is the number of variables assigned at level k,
// it is initialized to 0 at the begining and reset to 0 after the function execution
bool Solver::collectFirstUIP(CRef confl){
    involved_lits.clear();
    int max_level=1;
    Clause& c=ca[confl]; int minLevel=decisionLevel();
    for(int i=0; i<c.size(); i++) {
        Var v=var(c[i]);
        //        assert(!seen[v]);
        if (level(v)>0) {
            seen[v]=1;
            var_iLevel_tmp[v]=1;
            pathCs[level(v)]++;
            if (minLevel>level(v)) {
                minLevel=level(v);
                assert(minLevel>0);
            }
            //    varBumpActivity(v);
        }
    }

    int limit=trail_lim[minLevel-1];
    for(int i=trail.size()-1; i>=limit; i--) {
        Lit p=trail[i]; Var v=var(p);
        if (seen[v]) {
            int currentDecLevel=level(v);
            //      if (currentDecLevel==decisionLevel())
            //      	varBumpActivity(v);
            seen[v]=0;
            if (--pathCs[currentDecLevel]!=0) {
                Clause& rc=ca[reason(v)]; 
                int reasonVarLevel=var_iLevel_tmp[v]+1;
                if(reasonVarLevel>max_level) max_level=reasonVarLevel;
                if (rc.size()==2 && value(rc[0])==l_False) {
                    // Special case for binary clauses
                    // The first one has to be SAT
                    assert(value(rc[1]) != l_False); 
                    Lit tmp = rc[0];
                    rc[0] =  rc[1], rc[1] = tmp;
                }
                for (int j = 1; j < rc.size(); j++){
                    Lit q = rc[j]; Var v1=var(q);
                    if (level(v1) > 0) {
                        if (minLevel>level(v1)) {
                            minLevel=level(v1); limit=trail_lim[minLevel-1]; 	assert(minLevel>0);
                        }
                        if (seen[v1]) {
                            if (var_iLevel_tmp[v1]<reasonVarLevel)
                                var_iLevel_tmp[v1]=reasonVarLevel;
                        }
                        else {
                            var_iLevel_tmp[v1]=reasonVarLevel;
                            //   varBumpActivity(v1);
                            seen[v1] = 1;
                            pathCs[level(v1)]++;
                        }
                    }
                }
            }
            involved_lits.push(p);
        }
    }
    double inc=var_iLevel_inc;
    vec<int> level_incs; level_incs.clear(); // os: vec<int> and not vec<double> is a bug by the authors of _dist that leads to overflows but improves for unknown reason the performance. 
    for(int i=0;i<max_level;i++){
        level_incs.push(inc);
        inc = inc/my_var_decay;
    }

    for(int i=0;i<involved_lits.size();i++){
        Var v =var(involved_lits[i]);
        //        double old_act=activity_distance[v];
        //        activity_distance[v] +=var_iLevel_inc * var_iLevel_tmp[v];
        activity_distance[v]+=var_iLevel_tmp[v]*level_incs[var_iLevel_tmp[v]-1];

        if(activity_distance[v]>1e100){
            for(int vv=0;vv<nVars();vv++)
                activity_distance[vv] *= 1e-100;
            var_iLevel_inc*=1e-100;
            for(int j=0; j<max_level; j++) level_incs[j]*=1e-100;
        }
        if (order_heap_distance.inHeap(v))
            order_heap_distance.decrease(v);

        //        var_iLevel_inc *= (1 / my_var_decay);
		if (ncb_opt) 
			MaxScoreInLevel[level(v)-1] = std::max(MaxScoreInLevel[level(v)], activity_distance[v]); // os: TODO. activity_distance also decreases above, so should update MaxScoreInLevel. 
    }
    var_iLevel_inc=level_incs[level_incs.size()-1];
    return true;
}

struct UIPOrderByILevel_Lt {
    Solver& solver;
    const vec<double>&  var_iLevel;
    bool operator () (Lit x, Lit y) const
    {
        return var_iLevel[var(x)] < var_iLevel[var(y)] ||
                (var_iLevel[var(x)]==var_iLevel[var(y)]&& solver.level(var(x))>solver.level(var(y)));
    }
    UIPOrderByILevel_Lt(const vec<double>&  iLevel, Solver& para_solver) : solver(para_solver), var_iLevel(iLevel) { }
};

struct {
    bool operator()(Lit a, Lit b) const {
        return toInt(a) < toInt(b);
    }
} litcompare;

struct {
    bool operator()(std::vector<Lit>& a, std::vector<Lit>& b) const {         
        return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end(), litcompare);
    }
} vecCompare;

/*_________________________________________________________________________________________________
|
|  search : (nof_conflicts : int) (params : const SearchParams&)  ->  [lbool]
|
|  Description:
|    Search for a model the specified number of conflicts.
|
|  Output:
|    'l_True' if a partial assigment that is consistent with respect to the clauseset is found. If
|    all variables are decision variables, this means that the clause set is satisfiable. 'l_False'
|    if the clause set is unsatisfiable. 'l_Undef' if the bound_extra_learned on number of conflicts is reached.
|________________________________________________________________________________________________@*/
lbool Solver::search(int& nof_conflicts)
{
	assert(ok);
	int         backtrack_level;
	int         lbd;
	vec<Lit>    learnt_clause;
	bool        cached = false;
	int			reorder_counter = 1; // os: every X decisions we reorder.

    starts++;
	
	// simplify
	//

	if (conflicts >= curSimplify * nbconfbeforesimplify) {  // a strange seq of thresholds: 1000, 4000, 15000, 24000, 35000, 48000, 63000,...
		// printf("c ### simplifyAll on conflict : %lld\n", conflicts);
		// printf("nbClauses: %d, nbLearnts_core: %d, nbLearnts_tier2: %d, nbLearnts_local: %d, nbLearnts: %d\n",
		// clauses.size(), learnts_core.size(), learnts_tier2.size(), learnts_local.size(),
		// learnts_core.size() + learnts_tier2.size() + learnts_local.size());
		nbSimplifyAll++;        
        //printf("watches_bin before simplifyall: %d\n", watches_bin.size());
		if (!simplifyAll()) {
			return l_False;
		}
        //printf("watches_bin after simplifyall: %d\n", watches_bin.size());
        //printf("--\n");
		curSimplify = (conflicts / nbconfbeforesimplify) + 1;
		nbconfbeforesimplify += incSimplify;
	}

	for (;;) {
		CRef confl = propagate(true);        
        int tmp = decisionLevel();
		if (confl != CRef_Undef) {
			// CONFLICT
			
			//assert(!reorder_now); // os: this assertion is supposed to hold only if we turn off simplification when reorder_now = true. 
			if (VSIDS) {
				if (--timer == 0 && var_decay < 0.95) timer = 5000, var_decay += 0.01;
			}
			else
				if (step_size > min_step_size) step_size -= step_size_dec;

			conflicts++; nof_conflicts--;
			if (conflicts == 100000 && learnts_core.size() < 100) core_lbd_cut = 5;

            // symmetry_sel
            if (confl == CRef_Unsat) return l_False;

			ConflictData data = FindConflictLevel(confl); // os: finds the highest dec. level in the conflicting clause.
			if (data.nHighestLevel == 0) return l_False;
			if (data.bOnlyOneLitFromHighest) // os: This is unique to chrono, because normally we would have at least two literals from the highest dec. level in the conflicting clause. 
			{
				cancelUntil(data.nHighestLevel - 1); // os: in slide 33 it says that it jumps to the 2nd highest. 
				continue;
			}

			learnt_clause.clear();
			if (conflicts == 50000) DISTANCE = 0;
			//else DISTANCE = 1;
			if (VSIDS && DISTANCE)
				collectFirstUIP(confl);
#if Waerden
            short min_Distance_z, min_Distance_n;
#endif
#if Pyth
            short gcd;
            short maxvar;
#endif
#if Extra
            bool is_symmetric_clause = true, is_e_derived = false;
#endif

/***********************************************************************/
            analyze(confl, learnt_clause, backtrack_level, lbd
#if Waerden
                , min_Distance_z, min_Distance_n
#endif
#if Pyth
                , gcd
                , maxvar
#endif
#if Extra
                , is_symmetric_clause
                , is_e_derived
#endif
            );		                       

            if (conflicts == 1) stat_exp_learnt_size = learnt_clause.size();
            else stat_exp_learnt_size = stat_exp_alpha * learnt_clause.size() + (1 - stat_exp_alpha) * stat_exp_learnt_size;            
            stat_learnt_size += learnt_clause.size();
            
			stat_backtrack_recommendad_size += decisionLevel() - backtrack_level;
			stat_backtrack_count++;
			// check chrono backtrack condition
			if ((confl_to_chrono <= conflicts) &&				
				(decisionLevel() - backtrack_level) >= chrono)
			{
				++chrono_backtrack;
				stat_backtrack_size += decisionLevel() - (data.nHighestLevel - 1);
				cancelUntil(data.nHighestLevel - 1);				
			}
			else // default behavior
			{
				int my_bl = backtrack_level;
				if (ncb_opt && (backtrack_level < decisionLevel() - 1)) { 

					// os: looking for the max index i in the trail (as of backtrak level), s.t. 
					// forall j > i score(lit at trail[i]) >= score(lit at trail[j]). 
					// e.g., scores as of backtrack_level = 5,5,3,2,3,0,4,-1. The answer is i = 1 (0-based, i.e. the 2nd '5')
					// because '3' is not the max in its suffix, which means that when we decide it '4' will bypass it. 				
					// Once we detect non-monotonicity we stop pushing elements to the stack and search 
					// the maximal element, which will tell us how much to pop.
					/* for (int i = 0; i < MaxScoreInLevel.size(); ++i) { // os:
						std::cout << "maxscore[" << i + 1 << "]= " << MaxScoreInLevel[i] << "; score[" << i + 1 << "]= " << activity_distance[var(trail[trail_lim[i]])] << std::endl;
					}*/

										
					int prefix_idx; // see definition of prefix below. It is the largest decision level before the score goes up.  
					for (prefix_idx = backtrack_level + 1; 
						prefix_idx < MaxScoreInLevel.size() && 
							MaxScoreInLevel[prefix_idx] <= MaxScoreInLevel[prefix_idx - 1]; 
						prefix_idx++); 

					// prefix = max{prefix_idx | score(backtrack_level)>=...>=score(prefix_idx)} + 1 
					double max = MaxScoreInLevel[prefix_idx]; // this one violates monotinicity
					// search in the suffix the largest number:
					int suffix_idx;
					for (suffix_idx = prefix_idx + 1; suffix_idx < MaxScoreInLevel.size(); suffix_idx++) { // need to go back
						if (MaxScoreInLevel[suffix_idx] > max) max = MaxScoreInLevel[suffix_idx];
						if (max > MaxScoreInLevel[backtrack_level]) break;
					}
					if (max > MaxScoreInLevel[backtrack_level]) suffix_idx = backtrack_level;
					// now find the largest index in the prefix that is not smaller than max;
					else
						for (suffix_idx = backtrack_level;
							suffix_idx < prefix_idx && MaxScoreInLevel[suffix_idx] >= max;
							++suffix_idx);

					my_bl = std::min(suffix_idx, data.nHighestLevel - 1);
#ifdef PRINT_OUT								
					if (backtrack_level != suffix_idx) {
						std::cout << "Normal backtrack = " << backtrack_level << std::endl;
						std::cout << "Can jump to " << suffix_idx << std::endl;
						std::cout << "Jumping to " << my_bl << std::endl;
					}
#endif
					stat_backtrack_size += decisionLevel() - my_bl;
					cancelUntil(my_bl); // os: 					
				}
				else {
					stat_backtrack_size += decisionLevel() - backtrack_level;
					cancelUntil(backtrack_level); // original 
				}
				++non_chrono_backtrack;
			}

            lbd--;
            stat_lbd_size += lbd;
            if (VSIDS){
                cached = false;
                conflicts_VSIDS++;
                lbd_queue.push(lbd);
                global_lbd_sum += (lbd > 50 ? 50 : lbd); }

            if (learnt_clause.size() == 1){
                uncheckedEnqueue(learnt_clause[0]);       
#if Extra
                if (!is_symmetric_clause) nonsym_unaries.insert(var(learnt_clause[0]));
                else {
                    if (extra_from_generators)
                        generators_generate(learnt_clause, ExtraClauses_gen);
#if Pyth
                    if (extra_pyth) {        
                        std::vector<Lit> v;
                        v.push_back(learnt_clause[0]);
                        pyth_generate(v,1, var(learnt_clause[0]) + 1, ExtraClauses, GcdOfEClauses, MaxvarOfEClauses);
                    }
#endif
#if Waerden 
                    if (extra_waerden) {
                        varbounds[var(learnt_clause[0])] = {min_Distance_z, min_Distance_n};
                        Waerden_generate(learnt_clause, ExtraClauses, min_Distance_z, min_Distance_n, WaerdenDistance);
                    }
#endif
                }
#endif

            }else{ // learnt_clause.size() > 1
                CRef cr = ca.alloc(learnt_clause, true);

                ca[cr].set_lbd(lbd);
                if (lbd <= core_lbd_cut){
                    learnts_core.push(cr);
                    ca[cr].mark(CORE);
                }else if (lbd <= 6){
                    learnts_tier2.push(cr);
                    ca[cr].mark(TIER2);
                    ca[cr].touched() = conflicts;
                }else{
                    learnts_local.push(cr); // os: LOCAL is the default for a new clause so no need to mark
                    claBumpActivity(ca[cr]); }
#if Extra
                ca[cr].is_e_clause(is_e_derived);
#endif
                attachClause(cr); // this is learnt_clause 
                
#if Extra
// accumulation
                stat_extra_active_total += stat_extra_active;
                stat_learned_active_total += stat_learned_active;
                ca[cr].is_symmetric_clause(is_symmetric_clause);        
                if (is_e_derived) stat_extra_derived++;
                // if !is_r_clause then it was derived from the non-symmetric parts of the formula
                // so we do not replicate the clause.                 
                stat_extra_time_begin = cpuTime();
                Clause& c = ca[cr];
#if Pyth
                c.gcd(gcd);
                c.maxvar(maxvar);
#endif
				if (learnt_clause.size() <= extra_maxsize) {

#if Extra
                    if (is_symmetric_clause && extra_from_generators)
                        generators_generate(c, ExtraClauses_gen);
#endif

#if Waerden
                    if (is_symmetric_clause && extra_waerden)
                    {
                        Waerden_generate(c, ExtraClauses, min_Distance_z, min_Distance_n, WaerdenDistance);
//                        eclauses_set.clear();
                    }
#endif
#if Pyth
					if (is_symmetric_clause && gcd > 1 && extra_pyth) pyth_generate(c, ExtraClauses, GcdOfEClauses, MaxvarOfEClauses);
#endif
#if Ramsey
                    if (is_symmetric_clause && extra_ramsey)
					    ramsey_generate(c, ExtraClauses);
#endif

				}

                stat_w_time += cpuTime() - stat_extra_time_begin;
#endif
                
                uncheckedEnqueue(learnt_clause[0], backtrack_level, cr);
            }
            if (drup_file){
#ifdef BIN_DRUP
                binDRUP('a', learnt_clause, drup_file);
#else
                for (int i = 0; i < learnt_clause.size(); i++)
                    fprintf(drup_file, "%i ", (var(learnt_clause[i]) + 1) * (-2 * sign(learnt_clause[i]) + 1));
                fprintf(drup_file, "0\n");
#endif
            }

            if (VSIDS) varDecayActivity();
            claDecayActivity();
            
            if (verbosity >= 1 && (conflicts % 5000 == 0)) {

#if Extra                

                printf("%7.1f: conf: %d,\textra: %d,\text-deriv: %d,\tcon-act: %d,\text-act: %d,\text-core: %d,\tlearnt-size: %5.1f\n",
                    cpuTime(), 
                    (int)conflicts,                    
                    stat_extra_learned,                    
                    stat_extra_derived,
                    stat_learned_active, 
                    stat_extra_active,
                    stat_extra_core,                    
                    stat_exp_learnt_size
               
#else
                printf("| %7.1f: conf: %9d active: %8d moving-exp-learnt-size: %6.2f |\n",
                    cpuTime(), 
                    (int)conflicts,
                    nLearnts(),
                    //    (double)learnts_literals / nLearnts()                  
                    stat_exp_learnt_size
#endif
                );             
                if (conflicts % 100000 == 0) {
                    printf("avg-learned-size %d\n", stat_learnt_size / conflicts);
                    printf("avg-lbd %d\n", stat_lbd_size / conflicts);
                    
                }              
            }
        }else{
            // NO CONFLICT
            bool restart = false;
            if (!VSIDS)
                restart = nof_conflicts <= 0;
            else if (!cached){
                restart = lbd_queue.full() && (lbd_queue.avg() * 0.8 > global_lbd_sum / conflicts_VSIDS);
                cached = true;
            }
            if (restart 
#if Extra
                || (extra_restartonconf && extra_restart_now)
#endif
                ){
#if Extra
                extra_restart_now = false;
#endif
                lbd_queue.clear();
                cached = false;                          
                if (conflicts >= 30000000) switch_mode = true; // os: replacing the time-driven switch. 
                cancelUntil(0);
#if Extra
// restart
                stat_extra_time_begin = cpuTime();
                vec<Lit> nClause;   
                int j = 0;        
                // EXTRA (0) = waerden/ramsey/pyth, EXTRA_GEN (1) = generators
                enum {EXTRA, EXTRA_GEN};
                bool has_gliding = extra_pyth || extra_waerden || extra_ramsey;
                for (int ex = 0; ex <= 1; ++ex) { // going over potentially two lists of eclauses. 
                    std::vector<std::vector<Lit>>& Extras = ex == EXTRA ? ExtraClauses : ExtraClauses_gen;
                    for (auto& lits : Extras) {
                        if (lits.size() == 1) {
                            // TODO: note that we do not include here the reason clause.
                            // it should be the reason of the original unit, under symmetry.
                            lbool val = value(var(lits[0]));
                            if ((val == l_True && sign(lits[0])) || (val == l_False && !sign(lits[0]))) {
                                printf("ha ha unsat!\n");
                                return l_False;
                            }                            
                            if (val == l_Undef) {
                                uncheckedEnqueue(lits[0]);
                                if (ex == EXTRA_GEN && has_gliding) nonsym_unaries.insert(var(lits[0]));
#if Waerden
                                else {
                                    varbounds[var(lits[0])].dist_z = WaerdenDistance[j++];
                                    varbounds[var(lits[0])].dist_n = WaerdenDistance[j++];
                                }
#endif
                                stat_extra_unit_learned++;
                            }
                        }
                        else {
                            nClause.clear();
                            nClause.growTo(lits.size());
                            for (unsigned int ii = 0; (ii < lits.size()); ++ii) {
                                nClause[ii] = lits[ii];
                            }

                            CRef cr = ca.alloc(nClause, true);
                            // choose the clause list: 
                            if (extra_reducelist == 0)
                                learnts_local.push(cr);
                            else {
                                learnts_tier2.push(cr);
                                ca[cr].mark(TIER2);
                            }
#if Waerden                             
                            if (ex == EXTRA) {
                                ca[cr].Distance_z(WaerdenDistance[j++]);
                                ca[cr].Distance_n(WaerdenDistance[j++]);
                            }
                          
#endif
#if Pyth
                            if (ex == EXTRA) {
                                ca[cr].gcd(GcdOfEClauses[j]);
                                ca[cr].maxvar(MaxvarOfEClauses[j++]);
                            }
                            else {
                                // TODO: we can do better by computing the real gcd. 
                                ca[cr].gcd(1);
                                ca[cr].maxvar(1);
                            }
#endif

                            ca[cr].is_symmetric_clause(!(ex == EXTRA_GEN && has_gliding));
                            ca[cr].is_e_clause(true);

                            attachClause(cr);
                            claBumpActivity(ca[cr], 0.8);

                            stat_extra_learned++;
                            if (ex == EXTRA_GEN) stat_extra_gen_learned++;
                            //!!conflicts++; // heuristic: for now we do not increase conflicts on waerden clauses
                            // because it affects various simplification heuristics.
                            // printf("attached %d\n", stat_waerden_learned);
                        }
                        //if (verbosity >=1) printf(" + %u\n", ExtraClauses.size());
                        //printf("finished adding\n");
                    }
                    Extras.clear();
                }
#if Waerden         
                WaerdenDistance.clear();
#endif
#if Pyth
                GcdOfEClauses.clear();
                MaxvarOfEClauses.clear();
#endif
                stat_w_time += cpuTime() - stat_extra_time_begin;
#endif // Extra
				
                return l_Undef; 
            }
			
			if (reorder && conflicts > 5000 /*!DISTANCE*/) {		// os: 
                // std::cout << "in reorder" << std::endl;
				if (!reorder_now) {					
					reorder_counter++;
					double sortedness_value = 0;
					if ((reorder_counter % reorder_freq) == 0 
						&& 
						(decisionLevel() >= reorder_filter)) { 		
						if (reorder_sort_filter < 1) {
							sortedness_value = sortedness(trail); // note that this is not counted in the reorder_time, although perhaps it should. It is time required for checking whether to activate reorder. 	
//							printf("sortedness = %f\n", sortedness_value);
						}
						if (reorder_sort_filter == 1 || sortedness_value < reorder_sort_filter) { // we reorder if the trail is not sufficiently sorted
							reorder_now = reorder_build = true;
							reorder_start = cpuTime();
							lbd_queue.clear();

							//cached = false;
							
							cancelUntil(0); // This one calls insertVarOrder, which updates reorder_sorted
							reorder_build = false;							
														
							// printf("O");
							// sort according to activity, although we will later visit this list in reverse order. 
							// printf("Before: ");
							// for (int i = 0; i < reorder_sorted.size(); ++i) printf("%d ", (int64_t)activity_[reorder_sorted[i]]);
							// static bool print = false;
							// if (print) printf("\nAfter (reverse order): ");
							
							sort(reorder_sorted, VarOrderLt(DISTANCE ? activity_distance : ((!VSIDS) ? activity_CHB : activity_VSIDS)));
							//	if (print) 
							//		for (int i = reorder_sorted.size() -1; i >=0 ; --i) 
							//			printf("( %I64d %d)", (int64_t)activity_distance[reorder_sorted[i]], reorder_sorted[i]);
							//	if (print) printf("\n");
							//	print = false;
							reorder_index = reorder_reverse ? reorder_sorted.size() -1 : 0;
							return l_Undef;
						}
					}
				}
			}

            // Simplify the set of problem clauses:
            if (decisionLevel() == 0 && !simplify())
                return l_False;

            if (conflicts >= next_T2_reduce){
                next_T2_reduce = conflicts + 10000;
                reduceDB_Tier2(); }
            if (conflicts >= next_L_reduce){
                next_L_reduce = conflicts + 15000;
                reduceDB(); 
            }

            Lit next = lit_Undef;
            /*while (decisionLevel() < assumptions.size()){
                // Perform user provided assumption:
                Lit p = assumptions[decisionLevel()];
                if (value(p) == l_True){
                    // Dummy decision level:
                    newDecisionLevel();
                }else if (value(p) == l_False){
                    analyzeFinal(~p, conflict);
                    return l_False;
                }else{
                    next = p;
                    break;
                }
            }

            if (next == lit_Undef)*/{
                // New variable decision:
				if (reorder_now) reorder_decisions++;
				else decisions++;
                next = pickBranchLit(&reorder_now);                                
                if (next == lit_Undef)
                    // Model found:
                    return l_True;
            }

            // Increase decision level and enqueue 'next'
            newDecisionLevel();
#ifdef PRINT_OUT			
//			std::cout << decisionLevel() << ". " << next << " (var = " << var(next) << ")" << std::endl;
#endif
			if (ncb_opt) {
				MaxScoreInLevel.push(activity_distance[var(next)]); // os:
				if (maxScore.size() < decisionLevel()) maxScore.push(0.0); // os:
				else maxScore[decisionLevel() - 1] = 0.0; // os:
			}
            uncheckedEnqueue(next, decisionLevel());
			//std::cout << "var at trail = " << var(trail[trail_lim[decisionLevel()-1]]) << std::endl;
			//std::cout << "var(next) = " << var(next) << std::endl;

        }
    }
}


/*
  Finite subsequences of the Luby-sequence:

  0: 1
  1: 1 1 2
  2: 1 1 2 1 1 2 4
  3: 1 1 2 1 1 2 4 1 1 2 1 1 2 4 8
  ...


 */

static double luby(double y, int x){

    // Find the finite subsequence that contains index 'x', and the
    // size of that subsequence:
    int size, seq;
    for (size = 1, seq = 0; size < x+1; seq++, size = 2*size+1);

    while (size-1 != x){
        size = (size-1)>>1;
        seq--;
        x = x % size;
    }

    return pow(y, seq);
}



// NOTE: assumptions passed in member-variable 'assumptions'.
lbool Solver::solve_()
{
    // os: removing undeterminism. 
    /*#ifndef _MSC_VER
	signal(SIGALRM, SIGALRM_switch);
    alarm(2500);
#endif*/

    model.clear();
    conflict.clear();
    if (!ok) return l_False;

    solves++;

    max_learnts               = nClauses() * learntsize_factor;
    learntsize_adjust_confl   = learntsize_adjust_start_confl;
    learntsize_adjust_cnt     = (int)learntsize_adjust_confl;
    lbool   status            = l_Undef;

    if (verbosity >= 1){
/*        printf("c ============================[ Search Statistics ]==============================\n");
        printf("c | Conflicts |          ORIGINAL         |          LEARNT          | Progress |\n");
        printf("c |           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |\n");
        printf("c ===============================================================================\n");
        */  
    }
    printf("time:,\t conf,\textra,\textra-derived,\tconf-act,\textra-act,\texp-core,\tlearnt-size\n");

    add_tmp.clear();
	if (chrono == -1) confl_to_chrono = INT32_MAX; // os: saves a check in search

	// first run vsids for 10000 conflicts without restarts: // os:
    if (init_no_restarts) {
		VSIDS = true;
		int init = 10000;
		while (status == l_Undef && init > 0 && withinTimelimit() /*&& withinBudget()*/)
			status = search(init);
	}
    VSIDS = false;

	// Then search with restarts
    // Search:
    int curr_restarts = 0;
    while (status == l_Undef && withinTimelimit()/*&& withinBudget()*/){
        if (VSIDS){
            int weighted = INT32_MAX;
            status = search(weighted);
        }else{
			int nof_conflicts = INT32_MAX;
			if (restarts) {
				nof_conflicts = luby(restart_inc, curr_restarts) * restart_first;
				curr_restarts++;
			}
			status = search(nof_conflicts);
        }
        if (!VSIDS && switch_mode){
            VSIDS = true;
            printf("c Switched to VSIDS.\n");
            fflush(stdout);
            picked.clear();
            conflicted.clear();
            almost_conflicted.clear();
#ifdef ANTI_EXPLORATION
            canceled.clear();
#endif
        }
    }

    if (verbosity >= 1)
        printf("c ===============================================================================\n");

#ifdef BIN_DRUP
    if (drup_file && status == l_False) binDRUP_flush(drup_file);
#endif

    if (status == l_True){
        // Extend & copy model:
        model.growTo(nVars());
        for (int i = 0; i < nVars(); i++) model[i] = value(i);
    }else if (status == l_False && conflict.size() == 0)
        ok = false;

    cancelUntil(0);
    return status;
}

//=================================================================================================
// Writing CNF to DIMACS:
// 
// FIXME: this needs to be rewritten completely.

static Var mapVar(Var x, vec<Var>& map, Var& max)
{
    if (map.size() <= x || map[x] == -1){
        map.growTo(x+1, -1);
        map[x] = max++;
    }
    return map[x];
}


void Solver::toDimacs(FILE* f, Clause& c, vec<Var>& map, Var& max)
{
    if (satisfied(c)) return;

    for (int i = 0; i < c.size(); i++)
        if (value(c[i]) != l_False)
            fprintf(f, "%s%d ", sign(c[i]) ? "-" : "", mapVar(var(c[i]), map, max)+1);
    fprintf(f, "0\n");
}


void Solver::toDimacs(const char *file, const vec<Lit>& assumps)
{
    FILE* f = fopen(file, "wr");
    if (f == NULL)
        fprintf(stderr, "could not open file %s\n", file), exit(1);
    toDimacs(f, assumps);
    fclose(f);
}


void Solver::toDimacs(FILE* f, const vec<Lit>& assumps)
{
    // Handle case when solver is in contradictory state:
    if (!ok){
        fprintf(f, "p cnf 1 2\n1 0\n-1 0\n");
        return; }

    vec<Var> map; Var max = 0;

    // Cannot use removeClauses here because it is not safe
    // to deallocate them at this point. Could be improved.
    int cnt = 0;
    for (int i = 0; i < clauses.size(); i++)
        if (!satisfied(ca[clauses[i]]))
            cnt++;

    for (int i = 0; i < clauses.size(); i++)
        if (!satisfied(ca[clauses[i]])){
            Clause& c = ca[clauses[i]];
            for (int j = 0; j < c.size(); j++)
                if (value(c[j]) != l_False)
                    mapVar(var(c[j]), map, max);
        }

    // Assumptions are added as unit clauses:
    cnt += assumptions.size();

    fprintf(f, "p cnf %d %d\n", max, cnt);

    for (int i = 0; i < assumptions.size(); i++){
        assert(value(assumptions[i]) != l_False);
        fprintf(f, "%s%d 0\n", sign(assumptions[i]) ? "-" : "", mapVar(var(assumptions[i]), map, max)+1);
    }

    for (int i = 0; i < clauses.size(); i++)
        toDimacs(f, ca[clauses[i]], map, max);

    if (verbosity > 0)
        printf("c Wrote %d clauses with %d variables.\n", cnt, max);
}


//=================================================================================================
// Garbage Collection methods:

void Solver::relocAll(ClauseAllocator& to)
{
    // All watchers:
    //
    // for (int i = 0; i < watches.size(); i++)

    

    watches.cleanAll();
    watches_bin.cleanAll();
    for (int v = 0; v < nVars(); v++)
        for (int s = 0; s < 2; s++){
            Lit p = mkLit(v, s);
            // printf(" >>> RELOCING: %s%d\n", sign(p)?"-":"", var(p)+1);
            vec<Watcher>& ws = watches[p];
            for (int j = 0; j < ws.size(); j++)
                ca.reloc(ws[j].cref, to);
            vec<Watcher>& ws_bin = watches_bin[p];
            for (int j = 0; j < ws_bin.size(); j++)
                ca.reloc(ws_bin[j].cref, to);
        }

    // All ForReduce_reasons:
    //
    for (int i = 0; i < trail.size(); i++){
        Var v = var(trail[i]);

        if (reason(v) != CRef_Undef && (ca[reason(v)].reloced() || locked(ca[reason(v)])))
            ca.reloc(vardata[v].reason, to);
    }

    // All learnt:
    //
    for (int i = 0; i < learnts_core.size(); i++)
        ca.reloc(learnts_core[i], to);
    for (int i = 0; i < learnts_tier2.size(); i++)
        ca.reloc(learnts_tier2[i], to);
    for (int i = 0; i < learnts_local.size(); i++)
        ca.reloc(learnts_local[i], to);

    // All original:
    //
    int i, j;
    for (i = j = 0; i < clauses.size(); i++)
        if (ca[clauses[i]].mark() != 1){
            ca.reloc(clauses[i], to);
            clauses[j++] = clauses[i]; }
    clauses.shrink(i - j);
}


void Solver::garbageCollect()
{
    // Initialize the next region to a size corresponding to the estimated utilization degree. This
    // is not precise but should avoid some unnecessary reallocations for the new region:
    ClauseAllocator to(ca.size() - ca.wasted());

    relocAll(to);
    /*if (verbosity >= 2)
        printf("c |  Garbage collection:   %12d bytes => %12d bytes             |\n",
               ca.size()*ClauseAllocator::Unit_Size, to.size()*ClauseAllocator::Unit_Size);*/
    to.moveTo(ca);
}

void printClause(std::vector<Lit> lits, std::string text, bool sort_it = false, std::ostream& f = std::cout) {
    f << text.c_str() << std::endl;
    std::vector<int> sorted;
    for (int i = 0; i < lits.size(); i++) {
        Lit l = lits[i];
        int litVal = var(l) + 1;
        litVal = sign(l) ? -litVal : litVal;
        sorted.push_back(litVal);
        //f << litVal << " ";
    }
    if (sort_it) sort(sorted.begin(), sorted.end());
    for (auto x : sorted) f << x << " ";

    f << std::endl;

}

#if Waerden

static void CartesianRecurse(std::vector<std::vector<int>>& res, std::vector<int> stack,
    std::vector<std::vector<int>> sequences, int index)
{
    std::vector<int> sequence = sequences[index];
    std::vector<int> seq_org = sequence;
    do {
        // plant permuted sequence in stack. e.g., sequence = 4 0 3, so we want stack[0]=4, 
        // stack[3] = 3, stack[4] = 3;
        for (int i = 0; i < sequence.size(); ++i) {
            stack[seq_org[i]] = sequence[i];
        }
		if (index == sequences.size() - 1)
			res.push_back(stack);
		else
			CartesianRecurse(res, stack, sequences, index + 1);        
    } while (std::next_permutation(sequence.begin(), sequence.end()));
}

// Suppose we are solving w_3_4_3_<n>.cnf. 
// This function detects that there are 2 colors (0,2) of the same size (3). So we have {0,2},{1} 
// what we want is a cartesian product of all internal orders, so (0,1,2), (2,1,0) in this case. 
// It generates and stores them in waerden_color_permutations.
void Solver::Waerden_createPallets () {
    std::vector<std::vector<int>> sequences;    
    bool found;
    for (int i = 0; i < waerden_col.size(); ++i) {
        found = false;
        for (int j = 0; j < sequences.size(); ++j) {
            if (waerden_col[sequences[j][0]] == waerden_col[i]) {
                sequences[j].push_back(i);                
                found = true;
                break;
            }            
        }
        if (!found) {
            std::vector<int> newvec;
            newvec.push_back(i);
            sequences.push_back(newvec);            
        }
    }    
    std::vector<int> stack (waerden_c);    
    CartesianRecurse(waerden_color_permutations, stack, sequences, 0);
    printf("Waerden: %d color permutations\n", waerden_color_permutations.size());
}

void Solver::Waerden_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses, int min_Distance_z, int min_Distance_n, std::vector<int>& WaerdenDistance) {
    c.Distance_z(min_Distance_z);
    c.Distance_n(min_Distance_n);
    vec<Lit> lits;
    for (int i = 0; i < c.size(); ++i) lits.push(c[i]);
    Waerden_generate(lits, ExtraClauses, min_Distance_z, min_Distance_n, WaerdenDistance);
}

void Solver::Waerden_generate(vec<Lit>& lits, std::vector<std::vector<Lit>>& ExtraClauses, int min_Distance_z, int min_Distance_n, std::vector<int>& WaerdenDistance) {

   // Here we accumulate the new clauses (to be added after restart)                
    static int counter = 0;
    ++counter;
    
    int count = 0;
    
    for (int permidx = 0; permidx < waerden_color_permutations.size(); ++permidx) {
		// for each color permutation
		/*printf("color order: ");
		for (int c : waerden_color_permutations[permidx]) printf("%d,", c);
		printf("\n");*/
		// towards zero: 
        for (int i = 1; i <= min_Distance_z; ++i) {
            std::vector<Lit> nClause;            
            unsigned int nonfalse = 0;
            for (int ii = 0; ii < lits.size(); ++ii) {
                Lit l = lits[ii];
                short color = var(l) % waerden_c;
                short recolor = waerden_color_permutations[permidx][color];
                int recolored_l = toInt(l) + 2 * (recolor - color);
                Lit l1 = toLit(recolored_l - 2 * i * waerden_c);
                if (toInt(l1) < 0) 
                    printf("BLA!!");
                nClause.push_back(l1);
                if (value(l1) != l_False) nonfalse++;
            }
            if (nonfalse == 0) {
                extra_restart_now = true;
                // std::cout << "unsat (gen) eclause" << std::endl;
            }
         
            //printClause(nClause, "eclause (z)");
            int plbd;
            if (extra_nonfalselit == -1) plbd = computePartialLBD(nClause);
            if ((extra_nonfalselit == -1 && plbd < extra_plbd) ||
                (extra_nonfalselit >= 0 && nonfalse <= extra_nonfalselit)) {
                ExtraClauses.push_back(nClause);       
                /*if (!clause_ok(nClause)) {
                    printf("clause not ok (waerden-z)! %d\n", counter);
                    printf("i=%d (min_Distance_z = %d)\n",i, min_Distance_z);
                    printf("conflicts = %d", conflicts);
                    exit(1);
                }*/
 
                ++count;                

                // This tells us that the same clause was already inferred by generators_generate: 
                // std::sort(nClause.begin(), nClause.end());
                // if (eclauses_set.count(nClause) == 1) printf("+");

                // see explanation of these values in waerden.txt
                WaerdenDistance.push_back(min_Distance_z - i);
                WaerdenDistance.push_back(min_Distance_n + i);
            }
        }
        // towards n:         
        for (int i = 1; i <= min_Distance_n; ++i) {
            std::vector<Lit> nClause;
            unsigned int nonfalse = 0;
            for (int ii = 0; ii < lits.size(); ++ii) {
                Lit l = lits[ii];
                short color = var(l) % waerden_c;
                short recolor = waerden_color_permutations[permidx][color];
                int recolored_l = toInt(l) + 2 * (recolor - color);              
                Lit l1 = toLit(recolored_l + 2 * i * waerden_c); //note the '+'
                nClause.push_back(l1); 
                if (value(l1) != l_False) nonfalse++;
            }
            if (nonfalse == 0) {
                extra_restart_now = true;
                // std::cout << "unsat (gen) eclause" << std::endl;
            }         
            // printClause(nClause, "eclause (n)");
            //printf("[%d %d]", min_Distance_z + i, min_Distance_n - i);
                        
            int plbd;
            if (extra_nonfalselit == -1) plbd = computePartialLBD(nClause);
            if ((extra_nonfalselit == -1 && plbd < extra_plbd) ||
                (extra_nonfalselit >= 0 && nonfalse <= extra_nonfalselit)) {          
                ExtraClauses.push_back(nClause);
                /*if (!clause_ok(nClause)) {
                    printf("clause not ok (waerden-n)! %d\n", counter);
                    printf("i=%d\n",i);
                    printf("conflicts = %d\n", conflicts);
                    exit(1);
                }*/
                
                ++count;                
                WaerdenDistance.push_back(min_Distance_z + i);
                WaerdenDistance.push_back(min_Distance_n - i);
            }
        }
        if (count > extra_learnbound) return;
    };
}
#endif

#if Pyth

void Solver::pyth_generate(std::vector<Lit>& base, int c_gcd, int c_maxvar, std::vector<std::vector<Lit>>& ExtraClauses, std::vector<short>& GcdOfEClauses, std::vector<short>& MaxvarOfEClauses) {
    int count = 0;
    if (base.size() == 1) printf("!1\n");
    ExtraClauses.push_back(base);
    GcdOfEClauses.push_back(1);
    MaxvarOfEClauses.push_back(c_maxvar / c_gcd);
    std::vector<Lit> nClause;
    
    int mul = 1;

    while (true) {
        ++mul;
        if (mul == c_gcd) continue; // because this is c itself. 
        // c.maxvar() is the maxvar accross all clauses 'S' that derived c. The clause we add here
        // is generated by dividing by gcd (see 'base' above), and multiplying by mul. 
        // This is correct based on the observation that each clause in S can be divided by gcd and
        // multiplied by mul and still be in the formula, as long as it is under pyth_n. So this is
        // what we check below. 
        int maxvar = (c_maxvar / c_gcd) * mul;
        if (maxvar > pyth_n) return;
        int nonfalse = 0;
        for (int i = 0; i < base.size(); ++i) {
            Var v = (var(base[i]) + 1) * mul;
            Lit l = mkLit(v - 1, sign(base[i]));
            if (value(l) != l_False) nonfalse++;
            nClause.push_back(l);
        }
        int plbd;
        if (extra_nonfalselit == -1) plbd = computePartialLBD(nClause);
        if ((extra_nonfalselit == -1 && plbd < extra_plbd) ||
            (extra_nonfalselit >= 0 && nonfalse <= extra_nonfalselit))
        {
            ExtraClauses.push_back(nClause);
            GcdOfEClauses.push_back(mul); // !! was:  std::max(gcd, mul));
            MaxvarOfEClauses.push_back(maxvar);
            ++count;
            //      printClause(nClause, "derived");            
            if (count > extra_learnbound) return;
        }
        nClause.clear();
    }
}

void Solver::pyth_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses, std::vector<short>& GcdOfEClauses, std::vector<short>& MaxvarOfEClauses) {
    // note that c holds the gcd and maxvar as computed from the set of clauses from which it was derived. 
    
    static int counter = 0;
    ++counter;    
    //c.printClause("source");    
    std::vector<Lit> base;
    for (int i = 0; i < c.size(); ++i) {
        Var v = (var(c[i]) + 1) / c.gcd();
        Lit l = mkLit(v - 1, sign(c[i]));
        base.push_back(l);
    }
    assert(c.maxvar() % c.gcd() == 0);
    
    pyth_generate(base, c.gcd(), c.maxvar(), ExtraClauses, GcdOfEClauses, MaxvarOfEClauses);
}
#endif

//#pragma optimize("",off)
#if Ramsey
// if there are constraints that break the symmetry, they should be given in a seperate .rcnf file 
// i.e., -include-rcnf.
void Solver::ramsey_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses) {

    std::unordered_set<int> nodes;
    // Taking c's literals (=edges), and breaking them into their nodes
    // c.printClause("learned");
    // printf("nodes: \n");
    for (int i = 0; i < c.size(); ++i) {
        // map back to edges: 
        // e.g. (2,3),-(4,7),...
        int edge = var(c[i]) + 1;
        //  if (edge > ramsey_lastvar) return; // in cases where we add other variables, e.g., for symmetrybreaking.
        // 'removing the color':
        edge = edge % (ramsey_n * (ramsey_n - 1));
        if (edge == 0) {            
            edge = ramsey_n * (ramsey_n - 1);
        }
        int n1 = ceil(((float)edge / ramsey_n));
        int res = edge % ramsey_n;
        int n2 = res ? res : ramsey_n;  // e.g. this way we get the row while being 1-based, so 10 % 5 = 0, but we need row 5.
        nodes.insert(n1);
        nodes.insert(n2);
    }
    //  printf("--------- \n");
    // copying nodes into a vector
    std::vector<int> nodes_v;
    nodes_v.insert(nodes_v.begin(), nodes.begin(), nodes.end());
    // building a mockup clause, made of the *indices* of nodes_v.
    // e.g., if nodes_v= {1,4,7,9,6}, and c = {(1,7,c1), (7,9,c2), ...} then 
    // mockup = {(1,3},(3,4),...} 
    std::vector<int> mockup; // two consecutive entries correspond to an edge.
    for (int i = 0; i < c.size(); ++i) {
        int edge = var(c[i]) + 1;
        
        int color = ((float) edge / (ramsey_n * (ramsey_n - 1)));
        // Suppose ramsey_n = 6. The indices in the model are such that the 1st 
        // color (0) will occupy variables 1.. 6 * 5 = 30. But if it is exactly 30
        // then 30 / 30 == 1 but the color is 0. 
        // Similarly, 30 % 30 == 0 but we need it to be 30. So we fix it here: 
        edge = edge % (ramsey_n * (ramsey_n - 1));
        if (edge == 0) {
            --color;
            edge = ramsey_n * (ramsey_n - 1);
        }

        int n1 = ceil(((float)edge / ramsey_n));
        int res = edge % ramsey_n;
        int n2 = res ? res : ramsey_n;
        int idx1 = std::find(nodes_v.begin(), nodes_v.end(), n1) - nodes_v.begin();
        int idx2 = std::find(nodes_v.begin(), nodes_v.end(), n2) - nodes_v.begin();

        mockup.push_back(idx1);
        mockup.push_back(idx2);
        mockup.push_back(color);
    }

    // suppose the clause involves k (<=n) nodes.
    // Then for each of the choose(n)(k), and each order permutation of those,                 
    // generate a new clause.
    // TODO: heuristic: choose permutations involving active edges. 
    // Note: this does not include color permutations. We have more than enough without it. 
    // Generally to apply color permutations we need, similarly to how it is done in waerden, to apply
    // it within subsets of colors with the same bound. 

    std::vector<int> node_permutation;
    std::vector<Lit> nClause;
    // enumerating every permutation of nchoosek(ramsey_n, nodes.size()) 
    // TODO: so far without taking all internal orders
    std::string bitmask(nodes.size(), 1); // K leading 1's
    bitmask.resize(ramsey_n, 0); // N-K trailing 0's
    int bound_extra_learned = extra_learnbound;    
    int bound = ramsey_searchbound;
    do {
        for (int i = 0; i < ramsey_n; ++i) // [0..N-1] integers. TODO: inefficient to rebuild this list each time. 
        {
            if (bitmask[i]) node_permutation.push_back(i + 1);        
        }
        unsigned int nonfalse = 0;
        for (unsigned int i = 0; i < mockup.size(); i += 3) {
            // nodes after permutation.             
            int n1 = node_permutation[mockup[i]];
            int n2 = node_permutation[mockup[i + 1]];            
        
            int color = mockup[i + 2];
            if (n1 > n2) { // the edges' index is built such that n1 is the smaller one. 
                int tmp = n1; n1 = n2; n2 = tmp;
            }
            // The respective edge:
            int edge = color * ramsey_n * (ramsey_n - 1) + (n1 - 1) * ramsey_n + n2;
      
            Lit l = mkLit(edge - 1 , sign(c[i / 3]));
            if (value(l) != l_False) nonfalse++;
            if (extra_nonfalselit >= 0 && nonfalse > extra_nonfalselit) goto next;
            nClause.push_back(l);  // because mockup has 2 entries for each edge, we take i/2                    
        }
      //  printf("=========\n");
        int plbd;
        if (extra_nonfalselit == -1) {
            plbd = computePartialLBD(nClause);
            if (plbd >= extra_plbd) goto next;
        }
        // if we get here it is either because extra_nonfalselit == -1 and we are under extra_plbd, 
        // or extra_nonfalselit >= 0 and nonfalse < extra_nonfalselit (because of the goto statement
        // at the end of the loop above).
        //std::sort(nClause.begin(), nClause.end(), litcompare);
        //if (!std::binary_search(ExtraClauses_gen.begin(), ExtraClauses_gen.end(), nClause)) {
            ExtraClauses.push_back(nClause);
            bound_extra_learned--;
        //}
        // else stat_redundant++;
        
 next: nClause.clear();
        node_permutation.clear();
    } while (bound_extra_learned && bound-- && std::prev_permutation(bitmask.begin(), bitmask.end()));
}
#endif
//#pragma optimize("",on)

#if Extra

void Solver::read_generators(std::string filename)
{
    const unsigned int TIMEBOUND = 10; // in sec. TODO: make parameter. 
    std::ifstream file(filename);
    if (!file.good()) {
        std::cout << "cannot read " << filename << std::endl;
        exit(1);
    }
    double start = 0.0;
    printf(" Started reading generators...");
    std::string str, str1, token;
    int i = 0;
    while (std::getline(file, str)) {
        int rows, col;
        int res = sscanf(str.c_str(), "rows %d columns %d", &rows, &col);
        if (res == 2) {            
            std::stringstream s, s1;
            std::getline(file, str);
            s << str;
            for (int r = 0; r < rows -1; ++r) {                
                std::getline(file, str1);
                s1 << str1;
                t_generator generator;
                std::map<int, t_location> lit2location;
                t_orbit orbit;
                for (int c = 0; c < col; ++c) {                    
                    std::string val, val1; 
                    s >> val;
                    s1 >> val1;
                    int l = atoi(val.c_str());
                    Var var = abs(l) - 1;
                    Lit lit = (l > 0) ? mkLit(var) : ~mkLit(var);
                    orbit.push_back(lit);
                    lit2location[toInt(lit)] = t_location(generator.size(), 0);
                    l = atoi(val1.c_str());
                    var = abs(l) - 1;
                    lit = (l > 0) ? mkLit(var) : ~mkLit(var);
                    orbit.push_back(lit);
                    lit2location[toInt(lit)] = t_location(generator.size(), 1);
                    generator.push_back(orbit);
                    orbit.clear();
                }
                generators.push_back(generator);
                generator_lit2location.push_back(lit2location);
                lit2location.clear();   
                s.swap(s1);
                s.seekg(0, s.beg);
                s1.str("");
            }
            continue;
        }
        std::stringstream st(str);
        t_generator generator;
        std::map<int, t_location> lit2location;
        while (!st.eof()) {
            std::getline(st, token, ' ');
            if (st.eof()) break;            
			assert(token == "(");
			t_orbit orbit; 
            i = 0;
			while (true) { // inside an orbit
				std::getline(st, token, ' ');
				if (token == ")") break;
				int l = atoi(token.c_str());
				Var var = abs(l) - 1;
                Lit lit = (l > 0) ? mkLit(var) : ~mkLit(var);
				orbit.push_back(lit);
                lit2location[toInt(lit)] = t_location(generator.size(), i);
                ++i;
			}
			generator.push_back(orbit);
        }
        generators.push_back(generator);
        generator_lit2location.push_back(lit2location);
        lit2location.clear();
        if (cpuTime() - start > TIMEBOUND) {
            std::cout << " Breaking from reading generators" << std::endl;
            break; // because of benchmarks in which there are too many generators. 
        }
    }
    std::cout << "reading generators took " << cpuTime() - start << " sec" << std::endl;
}

bool LitComparer(Lit i, Lit j) { return (toInt(i) < toInt(j)); }

void Solver::generators_generate(vec<Lit>& lits, std::vector<std::vector<Lit>>& ExtraClauses_gen) {
    
    std::vector<Lit> nClause; 
    
    int counter = 0;    
    
    bool changed;
    for (int g = 0; g < generators.size(); ++g) {
        changed = false;
        unsigned int nonfalse = 0;
        for (int ii = 0; ii < lits.size(); ++ii) {
            Lit lit = lits[ii];
            if (generator_lit2location[g].find(toInt(lit)) == generator_lit2location[g].end()) { // literal not in generator
                nClause.push_back(lit);
                continue;
            }
            // TODO: currently we only move one to the right, but we can continue to generate more clauses.
            std::pair<int, int> p = generator_lit2location[g][toInt(lit)];
            auto& orbit = generators[g][p.first];
            Lit newLit = orbit[(p.second + 1) % orbit.size()]; // next literal in the orbit
            nClause.push_back(newLit);
            if (value(newLit) != l_False) nonfalse++;
            changed = true;
        }
        if (nonfalse == 0) {
            extra_restart_now = true;
            // std::cout << "unsat (gen) eclause" << std::endl;
        }
        //else if (nonfalse == 1) std::cout << "unit (gen) eclause" << std::endl;

        if (changed) {
            int plbd;
            if (extra_nonfalselit == -1) plbd = computePartialLBD(nClause);
            if ((extra_nonfalselit == -1 && plbd < extra_plbd) || 
                (extra_nonfalselit >=0 && nonfalse <= extra_nonfalselit)) {
                // in case we want to prevent other generators, e.g., ramsey, adding the same clauses: 
                //std::sort(nClause.begin(), nClause.end(), litcompare); 
                ExtraClauses_gen.push_back(nClause); 
                /*if (!clause_ok(nClause)) {
                    printf("not ok w-lits\n");
                    exit(1);
                }*/
                // just for stats: 
                /*std::sort(nClause.begin(), nClause.end());
                eclauses_set.insert(nClause);*/

                ++counter;
                /*c.printClause("original");
                printClause(nClause, "new clause");
                std::cout << "generator " << g << std::endl;            */
            }
        }
        if (counter > extra_learnbound) return;
        nClause.clear();        
    }
    //if (counter > 1) std::cout << "counter = " << counter << std::endl;
}


void Solver::generators_generate(Clause& c, std::vector<std::vector<Lit>>& ExtraClauses_gen) {
    vec<Lit> lits;    
    for (int i = 0; i < c.size(); ++i)
        lits.push(c[i]);

    generators_generate(lits, ExtraClauses_gen);

    // in case we want to prevent other generators, e.g., ramsey, adding the same clauses: 
    //    std::sort(ExtraClauses_gen.begin(), ExtraClauses_gen.end(), vecCompare);
}


void Solver::read_assignment(const std::string& filename) // for testing
{
    std::string filenameWithExtension = filename + ".sol";
    std::cout << "Reading assignment from file: " << filenameWithExtension << std::endl;

    std::ifstream solutionFile(filenameWithExtension);
    std::string line;
    if (solutionFile.is_open())
    {
        while (std::getline(solutionFile, line))
        {
            //std::cout << "Read: " << line << std::endl;

            // Ignore lines that do not start with "v "
            if (line.rfind("v ", 0) != 0)
                continue;

            line = line.substr(2);

            std::istringstream iss(line);
            int num;
            while (iss >> num)
            {
                //std::cout << "  read num: " << num << std::endl;
                mAssignment.resize(abs(num) + 1, false);
                mAssignment[abs(num)] = (num > 0);
            }
        }
    }

    else std::cout << "Unable to open the solution file" << std::endl;
}

bool Solver::clause_ok(const std::vector<Lit>& eclause)
{
    bool ok = false;

   /* std::cout << "Checking clause: ";
    printClause(eclause, "checked");
    
    std::cout << "--> ";
    */
    for (unsigned i = 0; i < eclause.size(); i++)
    {
        Lit lit = eclause[i];
        unsigned v = var(lit) + 1;

        //assert(var < mAssignment.size());

        if (mAssignment[v] == (!sign(lit))) // found a satisfied literal
        {
            ok = true;
            break;
        }
    }

    //std::string retString = (ok) ? "OK" : "NOT OK";
    // std::cout << retString << std::endl;

    return ok;
}


bool Solver::clause_ok(const vec<Lit>& eclause) {
    std::vector<Lit> v;
    for (int i = 0; i < eclause.size(); ++i)
        v.push_back(eclause[i]);
    return clause_ok(v);
}



#endif

// symmetry_sel

/*
 puts 2 unknown literals in front,
 if none exist, puts 1 unknown literal in front and highest level false literal second
 otherwise, puts highest level false literal in front, and second highest false literal second
*/

void Solver::prepareWatches(vec<Lit>& c) {
    assert(c.size() > 0);
    if (value(c[0]) == l_True) { // should not happen
        return;
    }
    for (int i = 1; i < c.size(); ++i) {
        if (value(c[i]) == l_True) { // should not happen
            return;
        }
        else if (value(c[i]) == l_Undef) {
            if (value(c[0]) == l_Undef) {
                Lit tmp = c[1]; c[1] = c[i]; c[i] = tmp;
                return; // two unknown literals
            }
            else {
                Lit tmp = c[0]; c[0] = c[i]; c[i] = c[1]; c[1] = tmp;
            }
        }
        else { // value(c[i])==l_False
            if (value(c[0]) == l_False && level(var(c[0])) < level(var(c[i]))) {
                Lit tmp = c[0]; c[0] = c[i]; c[i] = c[1]; c[1] = tmp;
            }
            else if (level(var(c[1])) < level(var(c[i]))) {
                assert(value(c[1]) == l_False);
                Lit tmp = c[1]; c[1] = c[i]; c[i] = tmp;
            }
        }
    }
    // either one unknown literal or all false
}

// minimize clause through self-subsumption
void Solver::minimizeClause(vec<Lit>& cl) {
    vec<int> minimizeTmpVec;

    // copy cl into minimizeTmpVec
    minimizeTmpVec.growTo(cl.size());
    for (int i = 0; i < cl.size(); ++i) {
        int vcli = var(cl[i]);
        minimizeTmpVec[i] = vcli;
        seenSel[vcli] = 1; // mark as seen
    }

    // for every literal in cl check if reason(~p) is subsumed in cl.
    // if it is, p can be removed from cl.
    for (int i = 0; i < cl.size() && cl.size()>1; ++i) {
        if (value(cl[i]) != l_False) {
            continue;
        }
        if (level(var(cl[i])) == 0) {
            cl.swapErase(i);
            --i;
        }
        else if (reason(var(cl[i])) != CRef_Undef) { // the literal might not have a reason clause
            const Clause& expl = ca[reason(var(cl[i]))];
            bool allSeen = true;
            for (int j = 0; j < expl.size(); ++j) {
                int var_j = var(expl[j]);
                if (level(var_j) != 0 && !seenSel[var_j]) {
                    allSeen = false;
                    break;
                }
            }
            if (allSeen) { // remove cl[i] from cl
                cl.swapErase(i);
                --i;
            }
        }
    }

    // reset seenSel
    for (int i = 0; i < minimizeTmpVec.size(); ++i) { // reset seen
        seenSel[minimizeTmpVec[i]] = 0;
    }
}

CRef Solver::addClauseFromSymmetry(vec<Lit>& symmetrical) {
    assert(symmetrical.size() > 0);

    CRef cr = ca.alloc(symmetrical, true);
    learnts_core.push(cr); // add the clause to the learned clause store (Delta from the paper)
    attachClause(cr);
    claBumpActivity(ca[cr]);
    if (symmetrical.size() <= 1) {
        cancelUntil(0);
    }
    else {
        cancelUntil(level(var(symmetrical[1])));
        assert(value(symmetrical[1]) == l_False); // we add only unit or conflicting clauses
    }

    // unit clause
    if (value(symmetrical[0]) == l_Undef) {
        uncheckedEnqueue(symmetrical[0], decisionLevel(), cr);
        return CRef_Undef;
    }

    // conflict clause
    assert(value(symmetrical[0]) == l_False);
    return cr;
}

int Solver::addSelClause(SymGenerator* g, Lit l) {
    CRef reason_l = reason(var(l));
    assert(reason_l != CRef_Undef); 
    const Clause& c_l = ca[reason_l];
    if (!c_l.learnt()) return 2; // os:

    // if the symmetric clause is already satisfied we do not add it to selClauses
    for (int i = 0; i < c_l.size(); ++i) {
        if (value(g->getImage(c_l[i])) == l_True) {
            return 2;
        }
    }

    // add to selClauses the shrinked symmetric clause (only the unknown literals, no need to store the false literals because they will not change when the clause is in selClauses)
    for (int i = 0; i < c_l.size(); ++i) {
        Lit symLit = g->getImage(c_l[i]);
        if (value(symLit) == l_Undef) {
            selClauses.push(symLit);
        }
    }
    symseladded++;

    int addedLitsCount = selClauses.size() - selClausesIndices.last();

    // if we encountered a symmetrical unit clause or a symmetrical conflict clause
    if (addedLitsCount < 2) {
        selClauses.shrink_(addedLitsCount);
        return addedLitsCount;
    }

    int selClauseIndex = selClausesOrigProp.size(); // index of the new sel clause
    selClausesOfWatcher[toInt(~selClauses[selClausesIndices.last()])]->push(selClauseIndex); // negation of first literal is a watcher
    selClausesOfWatcher[toInt(~selClauses[selClausesIndices.last() + 1])]->push(selClauseIndex); // negation of second literal is a watcher
    selClausesIndices.push(selClauses.size());
    selClausesOrigProp.push(var(l));
    selClausesGen.push(g);

    return 3;
}

void Solver::addGenerator(SymGenerator* g) {
    generators_sel.push(g);
}

void Solver::initiateGenWatches() {
    generatorsOfVar.clear();
    generatorsOfVarIndices.clear();
    generatorsOfVarIndices.push(0);
    for (int v = 0; v < nVars(); ++v) {
        for (int g = 0; g < generators_sel.size(); ++g) {
            if (generators_sel[g]->permutes(mkLit(v))) {
                generatorsOfVar.push(generators_sel[g]);
            }
        }
        generatorsOfVarIndices.push(generatorsOfVar.size());
    }
}

