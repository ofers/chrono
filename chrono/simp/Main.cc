/*****************************************************************************************[Main.cc]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007,      Niklas Sorensson

 Chanseok Oh's MiniSat Patch Series -- Copyright (c) 2015, Chanseok Oh
 
Maple_LCM, Based on MapleCOMSPS_DRUP -- Copyright (c) 2017, Mao Luo, Chu-Min LI, Fan Xiao: implementing a learnt clause minimisation approach
Reference: M. Luo, C.-M. Li, F. Xiao, F. Manya, and Z. L. , “An effective learnt clause minimization approach for cdcl sat solvers,” in IJCAI-2017, 2017, pp. to–appear.
 
Maple_LCM_Dist, Based on Maple_LCM -- Copyright (c) 2017, Fan Xiao, Chu-Min LI, Mao Luo: using a new branching heuristic called Distance at the beginning of search
 
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#include <errno.h>
#include <string>
#include <signal.h>
#ifdef _MSC_VER
//#include <win/zlib.h>
#else
#include <zlib.h>
#include <sys/resource.h>
#endif

#include "utils/System.h"
#include "utils/ParseUtils.h"
#include "utils/Options.h"
#include "core/Dimacs.h"
#include "simp/SimpSolver.h"

using namespace Minisat;

//=================================================================================================


void printStats(Solver& solver)
{
    double cpu_time = cpuTime();
#if defined(__linux__)
    double mem_used = memUsedPeak();
#endif
    if (solver.conflicts > 0) {
        printf("avg-learned-size %d\n", solver.stat_learnt_size / solver.conflicts);
        printf("avg-lbd %d\n", solver.stat_lbd_size / solver.conflicts);
    }
    printf("### time %g\n", cpu_time);
    printf("### conflicts %lu \n", solver.conflicts);

    // symmetry_sel
    printf("### symgenconfls %-12" PRIu64"   (%.0f /sec)\n", solver.symgenconfls, solver.symgenconfls / cpu_time);
    printf("### symselconfls %-12" PRIu64"   (%.0f /sec)\n", solver.symselconfls, solver.symselconfls / cpu_time);
    printf("### symgenprops %-12" PRIu64"   (%.0f /sec)\n", solver.symgenprops, solver.symgenprops / cpu_time);
    printf("### symselprops %-12" PRIu64"   (%.0f /sec)\n", solver.symselprops, solver.symselprops / cpu_time);
    printf("### symseladded %-12" PRIu64"   (%.0f /sec)\n", solver.symseladded, solver.symseladded / cpu_time);
#if Extra
    printf("### stat_extra_learned %d\n", solver.stat_extra_learned);
    printf(" out of which, by generators: %d\n", solver.stat_extra_gen_learned);
    printf("### stat_extra_unit_learned %d\n", solver.stat_extra_unit_learned);    
    printf("### stat_extra_derived %d\n", solver.stat_extra_derived);

    printf("### waeren_ramsey_overhead %g\n", solver.stat_w_time);
    if (solver.conflicts > 0) {
        printf("### stat_extra_active_avg %g\n", (float)solver.stat_extra_active_total / solver.conflicts);
        printf("### stat_learned_active_avg %g\n", (float)solver.stat_learned_active_total / solver.conflicts);
    }
    else{
        printf("### stat_extra_active_avg -1\n");
        printf("### stat_learned_active_avg -1\n");
    }
#else 
    printf("### stat_extra_learned 0\n");
    printf("### stat_extra_unit_learned 0\n");
    printf("### waeren_ramsey_overhead 0\n");
    printf("### stat_extra_active_avg -1\n");
    printf("### stat_learned_active_avg -1\n");
    
#endif


    printf("### restarts %lu\n", solver.starts);	
	printf("### decisions %lu\n", solver.decisions);
	//printf("### reorder_decisions %llu\n", solver.reorder_decisions);	
	printf("### propagations %lu\n", solver.propagations);
    if (solver.stat_prop_called > 0) {
        printf("### watched_binary_per_propagate-call %f\n", (float)solver.stat_prop_binary / solver.stat_prop_called);
    }
    printf("### binary_propagations_or_conflicts %d\n", solver.stat_binary_prop);
    if (solver.stat_prop_called > 0) {
        printf("### watched_nonbinary_per_propagate-call %f\n", (float)solver.stat_prop_watched / solver.stat_prop_called);
    }
	printf("### stat_prop_searchNewWatch %lu\n", solver.stat_prop_searchNewWatch);
#if Extra
    printf("redundant: %d\n", solver.stat_redundant);
#endif
	//printf("### reorder_propagations %llu\n", solver.reorder_propagations);
#if defined(__linux__)
	//if (mem_used != 0) printf("c Memory used           : %.2f MB\n", mem_used);
#endif

/*	printf("### reorder_time %g \n", solver.reorder_cpu_time);

	if (solver.stat_backtrack_count > 0) {
	printf("### backtrack_size_avg %f \n", (float) solver.stat_backtrack_size / solver.stat_backtrack_count);
	printf("### backtrack_recommended_size_avg %f \n", (float) solver.stat_backtrack_recommendad_size / solver.stat_backtrack_count);
    printf("### stat_reduced %d\n", solver.stat_reduced);
    printf("### prop_ratio %f\n", (float)solver.propagations / solver.decisions);
    
    printf("### stat_binary_prop_ratio %f\n", (float)solver.stat_binary_prop / solver.stat_propogations );
    printf("### bumped_ratio %f\n", (float)solver.stat_bumped / (solver.stat_size_unsat));
	*/
	printf("### stat_prop_called %d\n", solver.stat_prop_called);
    printf("### avg_propagations_in_no_conflict_cases %f\n", (float)solver.stat_size_sat / (solver.stat_prop_called - solver.stat_conflicts));
    printf("### avg_propagations_in_conflict_cases %f\n", (float)solver.stat_size_unsat / (solver.stat_conflicts));
//    printf("### stat_smoothed_implications_delta %f\n", solver.stat_smoothed_edges - solver.stat_smoothed_implications_begin);
//}
    //printf("non-sym: %d\n", solver.stat_non_sym);
}



static Solver* solver;
// Terminate by notifying the solver and back out gracefully. This is mainly to have a test-case
// for this feature of the Solver as it may take longer than an immediate call to '_exit()'.
static void SIGINT_interrupt(int signum) { exit(1); /* solver->interrupt();*/  }// os:

// Note that '_exit()' rather than 'exit()' has to be used. The reason is that 'exit()' calls
// destructors and may cause deadlocks if a malloc/free function happens to be running (these
// functions are guarded by locks for multithreaded use).
static void SIGINT_exit(int signum) {
    printf("\n"); printf("c *** INTERRUPTED ***\n");
    if (solver->verbosity > 0){
        printStats(*solver);
        printf("\n"); printf("c *** INTERRUPTED ***\n"); }
    _exit(1); }

static void SIGTERM_exit(int signum) { // o.s.: does not seem to work.
    printStats(*solver);
    printf("\n### timedout 1\n");	
    printf("\n### sat -1\n");

	 std::cout.flush();
	_exit(1);
}

//=================================================================================================
// Main:

int main(int argc, char** argv)
{
    try {
        setUsageHelp("USAGE: %s [options] <input-file> <result-output-file>\n\n  where input may be either in plain or gzipped DIMACS.\n");

#if defined(__linux__)
        fpu_control_t oldcw, newcw;
        _FPU_GETCW(oldcw); newcw = (oldcw & ~_FPU_EXTENDED) | _FPU_DOUBLE; _FPU_SETCW(newcw);
        printf("c WARNING: for repeatability, setting FPU to use double precision\n");
#endif
        // Extra options:
        //
        IntOption    verb("MAIN", "verb", "Verbosity level (0=silent, 1=some, 2=more).", 0, IntRange(0, 2));
        BoolOption   pre("MAIN", "pre", "Completely turn on/off any preprocessing.", true);
        StringOption dimacs("MAIN", "dimacs", "If given, stop after preprocessing and write the result to this file.");
        IntOption    cpu_lim("MAIN", "cpu-lim", "Limit on CPU time allowed in seconds.\n", INT32_MAX, IntRange(0, INT32_MAX));
        IntOption    mem_lim("MAIN", "mem-lim", "Limit on memory usage in megabytes.\n", INT32_MAX, IntRange(0, INT32_MAX));
        BoolOption   drup("MAIN", "drup", "Generate DRUP UNSAT proof.", false);
        StringOption drup_file("MAIN", "drup-file", "DRUP UNSAT proof ouput file.", "");
        BoolOption   linear_sym_gens("MAIN", "linear-sym-gens", "Use a linear number of generators for row interchangeability.", false);   // symmetry_sel
        std::string args = "";
        for (int i = 1; i < argc; i++)
            args += std::string(argv[i]) + " ";


        // parseOptions removes all legal parameters from argv and reduces argc accordingly. 
        // So we are left with the input file name only: 
        parseOptions(argc, argv, true); 
        SimpSolver  S;
        assert(argc == 2); // if not, we probably gave it two input file names. 
        std::string st(argv[1]);


        if (S.extra_nonfalselit >= 0) std::cout << "Warning: plbd threshold ignored" << std::endl;
#if Waerden
        // expecting a file name of the form <path>\w_c1_c2_c3_n.cnf for some sequence of numbers
        // c1,c2,c3 representing bounds on arithmetic progression of that color. 
        printf("This is Chrono / Waerden\n");
		if (S.extra_waerden) {
			int pos = st.find("w_");
			if (pos == std::string::npos) {
				std::cout << "Wrong input file format for a waerden file\n";
				exit(1);
			}
			std::string filename = st.substr(pos);

			pos = filename.rfind("_");
			std::string suffix = filename.substr(pos + 1);
			int scanf_res = sscanf(suffix.c_str(), "%d.cnf", &S.waerden_n);

			// finding # of colors
			S.waerden_c = 0;
			int pos1;
			int num;
			for (;;) {
				pos = filename.find("_");
				pos1 = filename.find("_", pos + 1);
				if (pos == std::string::npos || pos1 == std::string::npos) break;

				num = stoi(filename.substr(pos + 1, pos1));
				filename = filename.substr(pos + 1);
				++S.waerden_c;
				S.waerden_col.push_back(num); // colors are 0-based
			};
			//S.waerden_col.pop_back();
			S.Waerden_createPallets();

			if (S.waerden_n <= 3 || S.waerden_n > 511) { // not set or > 9 bits
				printf("Please set the value of waerden_n in the range 4..511");
				exit(1);
			}
		}
        else
        {
            std::cout << "Error: Running with chrono/Waerden but -extra_waerden flag is not set." << std::endl;
            exit(1);
        }
#else
        //if (S.extra_waerden) {
        //    std::cout << "cannot activate extra_waerden with this executable (activate #define Waerden 1) " << std::endl;
        //    exit(1);
        //}
#endif

#if Pyth
        int pos = st.find("p_");
        if (pos == std::string::npos) {
            std::cout << "Wrong input file format for a pyth file\n";
            exit(1);
        }
        std::string filename = st.substr(pos);
        int scanf_res = sscanf(filename.c_str(), "p_%d.cnf", &S.pyth_n);
        if (scanf_res != 1) {
            std::cout << "Wrong input file format for a pyth file\n";
            exit(1);
        }
#endif
#if Ramsey
            printf("This is Chrono / Ramsey\n");
            if (S.extra_ramsey) {
                // expecting a file name *_<#>.cnf, where '#' is the # of nodes. 
                int pos = st.rfind("_");
                if (pos == std::string::npos) {
                    std::cout << "Wrong input file format for a ramsey file\n";
                    exit(1);
                }
                std::string filename = st.substr(pos + 1);

                int scanf_res = sscanf(filename.c_str(), "%d.cnf", &S.ramsey_n);

                if (scanf_res != 1) {
                    std::cout << "Wrong input file format for a ramsey file\n";
                    exit(1);
                }
            }
            else std::cout << "Warning: running with chrono/Ramsey but -extra_ramsey flag is not set." << std::endl;
#else
        if (S.extra_ramsey) {
            std::cout << "cannot activate extra_ramsey with this executable (activate #define Ramsey 1) " << std::endl;
            exit(1);
        }
#endif
#if Extra
            if (S.extra_from_generators) {
                if (S.useSymmetry) {
                    std::cout << "do not use -extra_generators and -gen_omer simultanously. These are competing methods.\n";
                    return 1;
                }
                S.stat_extra_time_begin = cpuTime();
                S.read_generators(st + ".sym");
                S.stat_w_time += cpuTime() - S.stat_extra_time_begin;
                std::cout << "Read " << S.generators.size() << " generators" << std::endl;
            }
#else
        if (S.extra_from_generators) {
            std::cout << "cannot activate extra_from_generators with this executable (activate #define Extra 1) " << std::endl;
            exit(1);
        }

#endif

        // For testing eclauses: 
        // S.read_assignment(st); //  must remove the '0' in the end of the assignment for this to work. 


        double  initial_time = cpuTime();

        S.set_cpu_lim(cpu_lim);

        if (!pre) S.eliminate();

        S.parsing = true;
        S.verbosity = verb;
		S.drup_file = NULL;
        
        if (S.verbosity > 1) {
            printf("c This is MapleLCMDistChronoBT.\n");
            for (int i = 0; i < argc; ++i) printf("%s ", argv[i]);
            printf("\n");
        }

        if (drup || strlen(drup_file)){
            S.drup_file = strlen(drup_file) ? fopen(drup_file, "wb") : stdout;
            if (S.drup_file == NULL){
                S.drup_file = stdout;
                printf("c Error opening %s for write.\n", (const char*) drup_file); }
            printf("c DRUP proof generation: %s\n", S.drup_file == stdout ? "stdout" : drup_file);
        }

        solver = &S;
        // Use signal handlers that forcibly quit until the solver will be able to respond to
        // interrupts:
        signal(SIGINT, SIGINT_exit);

#ifndef _MSC_VER
		signal(SIGTERM, SIGTERM_exit);
        // signal(SIGXCPU,SIGINT_exit);

        // Set limit on CPU-time:
        if (cpu_lim != INT32_MAX){
            rlimit rl;
            getrlimit(RLIMIT_CPU, &rl);
            if (rl.rlim_max == RLIM_INFINITY || (rlim_t)cpu_lim < rl.rlim_max){
                rl.rlim_cur = cpu_lim;
                if (setrlimit(RLIMIT_CPU, &rl) == -1)
                    printf("c WARNING! Could not set resource limit: CPU-time.\n");
            } }

        // Set limit on virtual memory:
        if (mem_lim != INT32_MAX){
            rlim_t new_mem_lim = (rlim_t)mem_lim * 1024*1024;
            rlimit rl;
            getrlimit(RLIMIT_AS, &rl);
            if (rl.rlim_max == RLIM_INFINITY || new_mem_lim < rl.rlim_max){
                rl.rlim_cur = new_mem_lim;
                if (setrlimit(RLIMIT_AS, &rl) == -1)
                    printf("c WARNING! Could not set resource limit: Virtual memory.\n");
            } }
#endif
        if (argc == 1)
            printf("c Reading from standard input... Use '--help' for help.\n");
#if defined(__linux__)
		printf("I am linux!\n");
		gzFile in = (argc == 1) ? gzdopen(0, "rb") : gzopen(argv[1], "rb");
#else
        FILE *in = (argc == 1) ? fopen(0, "rb") : fopen(argv[1], "rb");
#endif
		if (in == NULL)
            printf("c ERROR! Could not open file: %s\n", argc == 1 ? "<stdin>" : argv[1]), exit(1);
#if defined(__linux__)
		gzFile inR = NULL;
#else
		FILE *inR = NULL;
#endif
        if (S.verbosity > 0){
            printf("c ============================[ Problem Statistics ]=============================\n");
            printf("c |                                                                             |\n"); }
        // parse_DIMACS used to crash on stack-overflow. Solved by specifying properties/linker/stack-reserve-size 
        parse_DIMACS(in, S , true/*isCNF*/);
        
#if defined(__linux__)
		gzclose(in);
#else
        fclose(in);
#endif
		
        // symmetry_sel
        if (S.useSymmetry) {
            std::string cnfloc = argv[1];
            std::string symloc = cnfloc + ".sym";
#if defined(__linux__)
            gzFile in_sym = (argc == 1) ? gzdopen(0, "rb") : gzopen(symloc.c_str(), "rb");
#else
            FILE* in_sym = (argc == 1) ? fopen(0, "rb") : fopen(symloc.c_str(), "rb");
#endif
            if (in_sym != NULL) {
                parse_SYMMETRY(in_sym, S, linear_sym_gens);
                std::cout << "read symmetry file" << std::endl;
#if defined(__linux__)
                gzclose(in_sym);
#else
                fclose(in_sym);
#endif    
            }
            else {
                printf("c Did not find .sym symmetry file. Assuming no symmetry is provided.\n");
                S.useSymmetry = false;
            }
        }
        FILE* res = (argc >= 3) ? fopen(argv[2], "wb") : NULL;

        if (S.verbosity > 1){
            printf("c |  Number of variables:  %12d                                         |\n", S.nVars());
            printf("c |  Number of clauses:    %12d                                         |\n", S.nClauses()); }
        
        double parsed_time = cpuTime();
        if (S.verbosity > 1)
            printf("c |  Parse time:           %12.2f s                                       |\n", parsed_time - initial_time);

        // Change to signal-handlers that will only notify the solver and allow it to terminate
        // voluntarily:
         signal(SIGINT, SIGINT_interrupt);
#ifndef _MSC_VER
		signal(SIGXCPU,SIGINT_interrupt);
#endif
        S.parsing = false;
        S.eliminate(); 
        
        // This has to come after eliminate(): 
        if (S.add_rcnf) {
			char* file_name = argv[1];
			file_name[strlen(file_name) - 4] = 0;
#if defined(__linux__)
			inR = gzopen((std::string(file_name) + std::string(".rcnf")).c_str(), "rb");
#else
			inR = fopen((std::string(file_name) + std::string(".rcnf")).c_str(), "rb");
#endif
			if (inR == NULL)
				printf("c ERROR! Could not open file: %s\n", (std::string(file_name) + std::string(".rcnf")).c_str()), exit(1);
            else parse_DIMACS(inR, S, false/*isCNF*/);
            if (verb >= 2) printf("%d rcnf clauses\n", S.get_stat_rcnf_clauses());
		}

        double simplified_time = cpuTime();
        if (S.verbosity > 0){
            printf("c |  Simplification time:  %12.2f s                                       |\n", simplified_time - parsed_time);
            printf("c |                                                                             |\n"); }

        if (!S.okay()){
            if (res != NULL) fprintf(res, "UNSAT\n"), fclose(res);
            if (S.verbosity > 0){
                printf("c ===============================================================================\n");
                printf("c Solved by simplification\n");
                printStats(S);
                printf("\n### timedout 0\n");
                printf("\n"); }
            printf("### sat 0\n");
            printf("s UNSATISFIABLE\n");
            if (S.drup_file){
#ifdef BIN_DRUP
                fputc('a', S.drup_file); fputc(0, S.drup_file);
#else
                fprintf(S.drup_file, "0\n");
#endif
            }
            if (S.drup_file && S.drup_file != stdout) fclose(S.drup_file);
            exit(20);
        }

        if (dimacs){
            if (S.verbosity > 0)
                printf("c ==============================[ Writing DIMACS ]===============================\n");
            S.toDimacs((const char*)dimacs);
            if (S.verbosity > 0)
                printStats(S);
            exit(0);
        }

        vec<Lit> dummy;

        lbool ret = S.solveLimited(dummy);
        
        if (S.verbosity >= 0) {
            printf("%s\n", args.c_str());
            printStats(S);
            printf("\n### timedout 0\n");
            printf("\n");            
        }
        printf("### sat %d\n", ret == l_True ? 1 : ret == l_False ? 0 : -1);
        printf(ret == l_True ? "s SATISFIABLE\n" : ret == l_False ? "s UNSATISFIABLE\n" : "s UNKNOWN\n");
		// assignment: 
        /*if (ret == l_True){
            printf("v ");
            for (int i = 0; i < S.nVars(); i++)
                if (S.model[i] != l_Undef)
                    printf("%s%s%d", (i==0)?"":" ", (S.model[i]==l_True)?"":"-", i+1);
            printf(" 0\n");
        }*/

        
        if (S.drup_file && ret == l_False){
#ifdef BIN_DRUP
            fputc('a', S.drup_file); fputc(0, S.drup_file);
#else
            fprintf(S.drup_file, "0\n");
#endif
        }
        if (S.drup_file && S.drup_file != stdout) fclose(S.drup_file);

        if (res != NULL){
            if (ret == l_True){
                fprintf(res, "SAT\n");
                for (int i = 0; i < S.nVars(); i++)
                    if (S.model[i] != l_Undef)
                        fprintf(res, "%s%s%d", (i==0)?"":" ", (S.model[i]==l_True)?"":"-", i+1);
                fprintf(res, " 0\n");
            }else if (ret == l_False)
                fprintf(res, "UNSAT\n");
            else
                fprintf(res, "INDET\n");
            fclose(res);
        }

#ifdef NDEBUG
        exit(ret == l_True ? 10 : ret == l_False ? 20 : 0);     // (faster than "return", which will invoke the destructor for 'Solver')
#else
        return (ret == l_True ? 10 : ret == l_False ? 20 : 0);
#endif
    } catch (OutOfMemoryException&){
        printf("c ===============================================================================\n");
        printf("c Out of memory\n");
        printf("s UNKNOWN\n");
        exit(0);
    }
}
