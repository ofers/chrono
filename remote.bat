set root=\Users\Ofer\source\repos\chrono\chrono
set server=ofers@tamnun.technion.ac.il
set dest_root=~/chrono
scp %root%\core\Solver.cc %server%:%dest_root%/core/Solver.cc
scp %root%\core\Solver.h %server%:%dest_root%/core/Solver.h
scp %root%\core\SolverTypes.h %server%:%dest_root%/core/SolverTypes.h
scp %root%\core\Dimacs.h %server%:%dest_root%/core/Dimacs.h
scp %root%\simp\SimpSolver.cc %server%:%dest_root%/simp/SimpSolver.cc
scp %root%\simp\Main.cc %server%:%dest_root%/simp/Main.cc
scp %root%\simp\SimpSolver.h %server%:%dest_root%/simp/SimpSolver.h
ssh %server% "cd %dest_root%;./m"